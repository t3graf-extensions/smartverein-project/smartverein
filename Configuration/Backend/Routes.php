<?php

/*
 * This file is part of the "SmartVerein - TYPO3 Club Management" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2023 Development-Team <development@t3graf-media.de>, T3graf media-agentur UG
 */

return [
    'mowareupdate' => [
        'path' => '/mowarebookupdate',
        'target' => \T3graf\Smartverein\Controller\MembersController::class . '::update',
    ]
];
