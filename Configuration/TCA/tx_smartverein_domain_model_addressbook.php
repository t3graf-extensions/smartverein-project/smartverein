<?php

/*
 * This file is part of the "SmartVerein - TYPO3 Club Management" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2023 Development-Team <development@t3graf-media.de>, T3graf media-agentur UG
 */

return [
    'ctrl' => [
        'title' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_addressbook',
        'label' => 'company_name',
        'label_alt' => 'contact_person,city',
        'label_alt_force' => true,
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'cruser_id' => 'cruser_id',
        'sortby' => 'sorting',
        'versioningWS' => true,
        'languageField' => 'sys_language_uid',
        'transOrigPointerField' => 'l10n_parent',
        'transOrigDiffSourceField' => 'l10n_diffsource',
        'delete' => 'deleted',
        'enablecolumns' => [
            'disabled' => 'hidden',
            'starttime' => 'starttime',
            'endtime' => 'endtime',
        ],
        'searchFields' => 'company_name,contact_person,street,zip,city,state,country,salutation,department,position,note',
        'iconfile' => 'EXT:smartverein/Resources/Public/Icons/Database/addressbook.svg'
    ],
    'types' => [
        '1' => ['showitem' => '--div--;LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_addressbook.tab.address, type, image, --palette--;LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_addressbook.palette.company;company, --palette--;LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_addressbook.palette.contact_person;person, --palette--;LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_addressbook.palette.address;address, --palette--;LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_addressbook.palette.communication;communication, note, --div--;LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_addressbook.tab.membershipManager, hidden,'],
    ],
    'palettes'  => [
        '1'           => ['showitem' => ''],
        'company' => [
            'showitem' => '
                company_name, department,
            '
        ],
        'person' => [
            'showitem' => '
                contact_person, position, --linebreak--,
                salutation,
            '
        ],
        'address' => [
            'showitem' => '
                street, --linebreak--,
                zip, city, --linebreak--,
                state, country,
            '
        ],
        'communication' => [
            'showitem' => '
                contacts,
            '
        ],
    ],
    'columns' => [
        'hidden' => [
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.visible',
            'config' => [
                'type' => 'check',
                'renderType' => 'checkboxToggle',
                'items' => [
                    [
                        0 => '',
                        1 => '',
                        'invertStateDisplay' => true
                    ]
                ],
            ],
        ],
        'type' => [
            'exclude' => true,
            'label' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_addressbook.type',
            'description' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_addressbook.type.description',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'items' => [
                    ['LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_addressbook.addressbook.type.item.0', 0],
                    ['LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_addressbook.addressbook.type.item.1', 1],
                    ['LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_addressbook.addressbook.type.item.2', 2],
                    ['LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_addressbook.addressbook.type.item.3', 3],
                    ['LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_addressbook.addressbook.type.item.4', 4],
                    ['LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_addressbook.addressbook.type.item.5', 5],
                    ['LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_addressbook.addressbook.type.item.6', 6],
                    ['LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_addressbook.addressbook.type.item.7', 7],
                ],
                'size' => 1,
                'maxitems' => 1,
                'eval' => 'required'
            ],
        ],
        'company_name' => [
            'exclude' => true,
            'label' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_addressbook.company_name',
            'description' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_addressbook.company_name.description',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim',
                'default' => ''
            ],
        ],
        'contact_person' => [
            'exclude' => true,
            'label' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_addressbook.contact_person',
            'description' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_addressbook.contact_person.description',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim',
                'default' => ''
            ],
        ],
        'street' => [
            'exclude' => true,
            'label' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_addressbook.street',
            'description' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_addressbook.street.description',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim',
                'default' => ''
            ],
        ],
        'zip' => [
            'exclude' => true,
            'label' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_addressbook.zip',
            'description' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_addressbook.zip.description',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim',
                'default' => ''
            ],
        ],
        'city' => [
            'exclude' => true,
            'label' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_addressbook.city',
            'description' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_addressbook.city.description',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim',
                'default' => ''
            ],
        ],
        'state' => [
            'exclude' => true,
            'label' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_members.state',
            'description' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_members.state.description',
            'config'  => [
                'type'                => 'select',
                'renderType'          => 'selectSingle',
                'items' => [
                    ['LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:labels.tca.notSet', 0],
                ],
                'foreign_table'       => 'static_country_zones',
                'foreign_table_where' => 'static_country_zones.zn_country_uid=###REC_FIELD_country### ORDER BY static_country_zones.zn_name_local',
                'size'                => 1,
                'default'             => 0,
                'minitems'            => 0,
                'maxitems'            => 1,
            ],
        ],
        'country' => [
            'exclude' => true,
            'onChange' => 'reload',
            'label' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_members.country',
            'description' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_members.country.description',
            'config'  => [
                'type'                => 'select',
                'renderType'          => 'selectSingle',
                'items' => [
                    ['LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:labels.tca.notSet', 0],
                ],
                'foreign_table'       => 'static_countries',
                'foreign_table_where' => 'ORDER BY static_countries.cn_short_local',
                'size'                => 1,
                'default'             => 0,
                'minitems'            => 0,
                'maxitems'            => 1,
            ],
        ],
        'image' => [
            'exclude' => true,
            'label' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_addressbook.image',
            'description' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_addressbook.image.description',
            'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig(
                'image',
                [
                    'appearance' => [
                        'createNewRelationLinkTitle' => 'LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:images.addFileReference'
                    ],
                    'overrideChildTca' => [
                        'types' => [
                            '0' => [
                                'showitem' => '
                                --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                                --palette--;;filePalette'
                            ],
                            \TYPO3\CMS\Core\Resource\File::FILETYPE_TEXT => [
                                'showitem' => '
                                --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                                --palette--;;filePalette'
                            ],
                            \TYPO3\CMS\Core\Resource\File::FILETYPE_IMAGE => [
                                'showitem' => '
                                --palette--;;imageoverlayPalette,
                                --palette--;;filePalette',
                            ],
                            \TYPO3\CMS\Core\Resource\File::FILETYPE_AUDIO => [
                                'showitem' => '
                                --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                                --palette--;;filePalette'
                            ],
                            \TYPO3\CMS\Core\Resource\File::FILETYPE_VIDEO => [
                                'showitem' => '
                                --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                                --palette--;;filePalette'
                            ],
                            \TYPO3\CMS\Core\Resource\File::FILETYPE_APPLICATION => [
                                'showitem' => '
                                --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                                --palette--;;filePalette'
                            ]
                        ],
                    ],
                    'foreign_match_fields' => [
                        'fieldname' => 'image',
                        'tablenames' => 'tx_smartverein_domain_model_addressbook',
                        'table_local' => 'sys_file',
                    ],
                    'maxitems' => 1
                ],
                $GLOBALS['TYPO3_CONF_VARS']['GFX']['imagefile_ext']
            ),

        ],
        'salutation' => [
            'exclude' => true,
            'label' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_addressbook.salutation',
            'description' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_addressbook.salutation.description',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim',
                'default' => ''
            ],
        ],
        'department' => [
            'exclude' => true,
            'label' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_addressbook.department',
            'description' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_addressbook.department.description',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim',
                'default' => ''
            ],
        ],
        'position' => [
            'exclude' => true,
            'label' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_addressbook.position',
            'description' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_addressbook.position.description',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim',
                'default' => ''
            ],
        ],
        'note' => [
            'exclude' => true,
            'label' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_addressbook.note',
            'description' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_addressbook.note.description',
            'config' => [
                'type' => 'text',
                'enableRichtext' => true,
                'richtextConfiguration' => 'default',
                'fieldControl' => [
                    'fullScreenRichtext' => [
                        'disabled' => false,
                    ],
                ],
                'cols' => 40,
                'rows' => 15,
                'eval' => 'trim',
            ],

        ],
        'contacts' => [
            'exclude' => true,
            'label' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_addressbook.contacts',
            'description' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_addressbook.contacts.description',
            'config' => [
                'type' => 'inline',
                'foreign_table' => 'tx_smartverein_domain_model_contacts',
                'foreign_field' => 'addressbook',
                'foreign_sortby' => 'sorting',
                'maxitems' => 9999,
                'appearance' => [
                    'collapseAll' => 1,
                    'levelLinksPosition' => 'top',
                    'showSynchronizationLink' => 1,
                    'showPossibleLocalizationRecords' => 1,
                    'showAllLocalizationLink' => 1
                ],
            ],

        ],

    ],
];
