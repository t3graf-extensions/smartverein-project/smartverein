<?php

/*
 * This file is part of the "SmartVerein - TYPO3 Club Management" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2023 Development-Team <development@t3graf-media.de>, T3graf media-agentur UG
 */

use T3graf\Smartverein\Utility\SmartvereinUtility;

$isoCode = SmartvereinUtility::getExtConfProperty('smartverein', 'settings.siteLanguage');
switch ($isoCode) {
    case 'de':
        $defaultCountry = 54;
        break;
    default:
        $defaultCountry = 0;
}

return [
    'ctrl' => [
        'title' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_members',
        'label' => 'lastname',
        'label_alt' => 'firstname,member_id,organisation,city',
        'label_alt_force' => true,
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'cruser_id' => 'cruser_id',
        'sortby' => 'sorting',
        'versioningWS' => true,
        'languageField' => 'sys_language_uid',
        'transOrigPointerField' => 'l10n_parent',
        'transOrigDiffSourceField' => 'l10n_diffsource',
        'delete' => 'deleted',
        'enablecolumns' => [
            'disabled' => 'hidden',
        ],
        'searchFields' => 'member_id,firstname,lastname,title,street,zip,city,state,country,addition_to_address,account_owner,iban,bic,customfield1,customfield2,customfield3,customfield4,customfield5,customfield6,customfield7,customfield8,customfield9,customfield10,,mandate_reference',
        'iconfile' => 'EXT:smartverein/Resources/Public/Icons/Database/members.svg'
    ],
    'types' => [
        '1' => ['showitem' => '--div--;LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_members.tab.memberInformation,
        member_type, --linebreak--, --palette--;;member, --palette--;;organisation, --palette--;;address, --palette--;;communication, --palette--;;family, --palette--;;logindata, --div--;LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_members.tab.membership ,--palette--;;membership,  --div--;LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_members.tab.membershipFees , --palette--;;membership_fee, --palette--;;bank_account, --div--;LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_members.tab.customerFields, customfield1, customfield2, customfield3, customfield4, customfield5, customfield6, customfield7, customfield8, customfield9, customfield10,--div--;LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_members.tab.membershipCategories, categories, --div--;LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_members.tab.membershipCareer, vita, --div--;LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_members.tab.membershipManager, hidden, changelog'],
    ],
    'palettes'  => [
        '1'           => ['showitem' => ''],
        'membership' => [
            'showitem' => '
                profile_image, --linebreak--,
                status, member_id, --linebreak--,
                entry_date, leaving_date, --linebreak--,
                membership_application, leaving_statement, declaration_of_consent,  --linebreak--,
                sections, --linebreak--,
                functions, --linebreak--,
                positions, --linebreak--,
                role,
            '
        ],
        'member' => [
            'label' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_members.palette.member',
            'showitem' => '
                gender, title, --linebreak--,
                firstname, lastname, --linebreak--,
                dateofbirth, --linebreak--,
            '
        ],
        'organisation' => [
            'label' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_members.palette.organisation',
            'showitem' => '
                organisation, --linebreak--,
                contact_person, position, department, --linebreak--,

            '
        ],
        'logindata' => [
            'label' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_members.palette.logindata',
            'showitem' => '
                fe_user, --linebreak--,
            '
        ],
        'address' => [
            'label' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_members.palette.address',
            'showitem' => '
                street, addition_to_address, --linebreak--,
                zip, city, --linebreak--,
                state, country, --linebreak--,
            '
        ],
        'communication' => [
            'label' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_members.palette.communication',
            'showitem' => '
                salutation, preferred_communication, --linebreak--,
                email, phone, --linebreak--,
                mobile, fax, --linebreak--,
                website, --linebreak--,
            '
        ],
        'family' => [
            'label' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_members.palette.family',
            'showitem' => '
                belongsto,
            '
        ],
        'membership_fee' => [
            'showitem' => '
                method_of_payment, --linebreak--,
                member_fee, payment_interval, --linebreak--,
            '
        ],
        'bank_account' => [
            'label' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_members.palette.bank',
            'showitem' => '
                account_owner, --linebreak--,
                iban, bic, --linebreak--,
                mandate_reference, mandate_date,sepa_mandate
            '
        ],
    ],
    'columns' => [
        'hidden' => [
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.visible',
            'config' => [
                'type' => 'check',
                'renderType' => 'checkboxToggle',
                'items' => [
                    [
                        0 => '',
                        1 => '',
                        'invertStateDisplay' => true
                    ]
                ],
            ],
        ],
        'categories' => [
            'exclude' => true,
            'label'   => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_members.categories',
            'config'  => [
                'type' => 'category',
                'treeConfig' => [
                    'startingPoints' => 1,
                    'appearance' => [
                        'nonSelectableLevels' => '0,1',
                    ]
                ],
            ],
        ],
        'crdate' => [
            'exclude' => true,
            'label' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_members.crdate',
            'config' => [
                'type' => 'input',
                'eval' => 'datetime',
                'renderType' => 'inputDateTime',
                'readOnly' => true,
            ]
        ],
        'cruser_id' => [
            'exclude' => true,
            'label' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_members.cruser',
            'config' => [
                'type' => 'input',
                'readOnly' => true,
            ]
        ],
        'member_type' => [
            'exclude' => true,
            'label' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_members.member_type',
            'description' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_members.member_type.description',
            'onChange' => 'reload',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'items' => [
                    ['LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:memberType.item.0', 0],
                    ['LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:memberType.item.1', 1],
                ],
                'default' => SmartvereinUtility::getExtConfProperty('smartverein', 'settings/preferredMemberType'),
                'size' => 1,
                'maxitems' => 1,
                'eval' => ''
            ],
        ],
        'gender' => [
            'exclude' => true,
            'label' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_members.gender',
            'description' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_members.gender.description',
            'displayCond' => 'FIELD:member_type:=:0',
            'config' => [
                'type' => 'radio',
                'items' => [
                    ['LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_members.gender.item.0', 0],
                    ['LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_members.gender.item.1', 1],
                    ['LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_members.gender.item.2', 2],
                ],
                'size' => 1,
                'maxitems' => 1,
                'eval' => ''
            ],
        ],
        'member_id' => [
            'exclude' => true,
            'label' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_members.member_id',
            'description' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_members.member_id.description',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim',
                'default' => '',
            ],
        ],
        'firstname' => [
            'exclude' => true,
            'label' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_members.firstname',
            'description' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_members.firstname.description',
            'displayCond' => 'FIELD:member_type:=:0',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim,required',
                'default' => '',
            ],
        ],
        'lastname' => [
            'exclude' => true,
            'label' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_members.lastname',
            'description' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_members.lastname.description',
            'displayCond' => 'FIELD:member_type:=:0',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim,required',
                'default' => ''
            ],
        ],
        'status' => [
            'exclude' => true,
            'label' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_members.status',
            'description' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_members.status.description',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'items' => [
                    ['LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:labels.tca.notSet', 0],
                    ['LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:status.item.1', 1],
                    ['LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:status.item.2', 2],
                    ['LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:status.item.3', 3],
                    ['LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:status.item.4', 4],
                    ['LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:status.item.5', 5],
                ],
                'size' => 1,
                'maxitems' => 1,
                'eval' => ''
            ],
        ],
        'salutation' => [
            'exclude' => true,
            'label' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_members.salutation',
            'description' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_members.salutation.description',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'items' => [
                    ['LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:salutation.item.0', 0],
                    ['LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:salutation.item.1', 1],
                    ['LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:salutation.item.2', 2],
                    ['LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:salutation.item.3', 3],
                ],
                'size' => 1,
                'maxitems' => 1,
                'eval' => ''
            ],
        ],
        'title' => [
            'exclude' => true,
            'label' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_members.title',
            'description' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_members.title.description',
            'displayCond' => 'FIELD:member_type:=:0',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim',
                'default' => ''
            ],
        ],
        'organisation' => [
            'exclude' => true,
            'label' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_members.organisation',
            'description' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_members.organisation.description',
            'displayCond' => 'FIELD:member_type:=:1',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim',
                'default' => ''
            ],
        ],
        'contact_person' => [
            'exclude' => true,
            'label' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_members.contact_person',
            'description' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_members.contact_person.description',
            'displayCond' => 'FIELD:member_type:=:1',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim',
                'default' => ''
            ],
        ],
        'position' => [
            'exclude' => true,
            'label' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_members.position',
            'description' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_members.position.description',
            'displayCond' => 'FIELD:member_type:=:1',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim',
                'default' => ''
            ],
        ],
        'department' => [
            'exclude' => true,
            'label' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_members.department',
            'description' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_members.department.description',
            'displayCond' => 'FIELD:member_type:=:1',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim',
                'default' => ''
            ],
        ],
        'website' => [
            'exclude' => true,
            'label' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_members.website',
            'description' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_members.website.description',
            'displayCond' => 'FIELD:member_type:=:1',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim',
                'default' => ''
            ],
        ],
        'street' => [
            'exclude' => true,
            'label' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_members.street',
            'description' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_members.street.description',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim',
                'default' => ''
            ],
        ],
        'zip' => [
            'exclude' => true,
            'label' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_members.zip',
            'description' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_members.zip.description',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim',
                'default' => ''
            ],
        ],
        'city' => [
            'exclude' => true,
            'label' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_members.city',
            'description' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_members.city.description',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim',
                'default' => ''
            ],
        ],
        'state' => [
            'exclude' => true,
            'label' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_members.state',
            'description' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_members.state.description',
            'config'  => [
                'type'                => 'select',
                'renderType'          => 'selectSingle',
                'items' => [
                    ['LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:labels.tca.notSet', 0],
                ],
                'foreign_table'       => 'static_country_zones',
                'foreign_table_where' => 'static_country_zones.zn_country_uid=###REC_FIELD_country### ORDER BY static_country_zones.zn_name_local',
                'size'                => 1,
                'default'             => 0,
                'minitems'            => 0,
                'maxitems'            => 1,
            ],
        ],
        'country' => [
            'exclude' => true,
            'onChange' => 'reload',
            'label' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_members.country',
            'description' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_members.country.description',
            'config'  => [
                'type'                => 'select',
                'renderType'          => 'selectSingle',
                'items' => [
                    ['LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:labels.tca.notSet', 0],
                ],
                'foreign_table'       => 'static_countries',
                'foreign_table_where' => 'ORDER BY static_countries.cn_short_local',
                'size'                => 1,
                'default'             => $defaultCountry,
                'minitems'            => 0,
                'maxitems'            => 1,
            ],
        ],
        'addition_to_address' => [
            'exclude' => true,
            'label' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_members.addition_to_address',
            'description' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_members.addition_to_address.description',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim',
                'default' => ''
            ],
        ],
        'email' => [
            'exclude' => true,
            'label' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_members.email',
            'description' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_members.email.description',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'nospace,email',
                'default' => ''
            ]
        ],
        'phone' => [
            'exclude' => true,
            'label' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_members.phone',
            'description' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_members.phone.description',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim',
                'default' => ''
            ],
        ],
        'mobile' => [
            'exclude' => true,
            'label' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_members.mobile',
            'description' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_members.mobile.description',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim',
                'default' => ''
            ],
        ],
        'fax' => [
            'exclude' => true,
            'label' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_members.fax',
            'description' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_members.fax.description',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim',
                'default' => ''
            ],
        ],
        'dateofbirth' => [
            'exclude' => true,
            'label' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_members.dateofbirth',
            'description' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_members.dateofbirth.description',
            'displayCond' => 'FIELD:member_type:=:0',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'size' => 7,
                'eval' => 'date,int',
                'default' => ''
            ],
        ],
        'entry_date' => [
            'exclude' => true,
            'label' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_members.entry_date',
            'description' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_members.entry_date.description',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'size' => 7,
                'eval' => 'date,int',
                'default' => time()
            ],
        ],
        'leaving_date' => [
            'exclude' => true,
            'label' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_members.leaving_date',
            'description' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_members.leaving_date.description',
            'onChange' => 'reload',
            'displayCond' => 'REC:NEW:false',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'size' => 7,
                'eval' => 'date,int',
                'default' => ''
            ],
        ],
        'membership_application' => [
            'exclude' => true,
            'label' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_members.membership_application',
            'description' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_members.membership_application.description',
            'displayCond' => 'REC:NEW:false',
            'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig(
                'membership_application',
                [
                    'appearance' => [
                       'createNewRelationLinkTitle' => 'LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:media.addFileReference',
                        'fileByUrlAllowed' => false,
                        'collapseAll' => true,
                    ],
                    'overrideChildTca' => [
                        'types' => [
                            '0' => [
                                'showitem' => '

                                --palette--;;filePalette'
                            ],
                            \TYPO3\CMS\Core\Resource\File::FILETYPE_TEXT => [
                                'showitem' => '

                                --palette--;;filePalette'
                            ],
                            \TYPO3\CMS\Core\Resource\File::FILETYPE_IMAGE => [
                                'showitem' => '

                                --palette--;;filePalette',
                            ],
                            \TYPO3\CMS\Core\Resource\File::FILETYPE_APPLICATION => [
                                'showitem' => '

                                --palette--;;filePalette'
                            ]
                        ],
                    ],
                    'foreign_match_fields' => [
                        'fieldname' => 'membership_application',
                        'tablenames' => 'tx_smartverein_domain_model_members',
                        'table_local' => 'sys_file',
                    ],
                    'maxitems' => 1
                ],
                'jpg,jpeg,pdf,docx,doc,odt'
            ),

        ],
        'leaving_statement' => [
            'exclude' => true,
            'label' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_members.leaving_statement',
            'description' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_members.leaving_statement.description',
            'displayCond' => [
                'AND' => [
                    'FIELD:leaving_date:REQ:true',
                    'displayCond' => 'REC:NEW:false',
                ],
            ],
            'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig(
                'leaving_statement',
                [
                    'appearance' => [
                        'createNewRelationLinkTitle' => 'LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:media.addFileReference',
                        'fileByUrlAllowed' => false,
                        'collapseAll' => true,
                    ],
                    'overrideChildTca' => [
                        'types' => [
                            '0' => [
                                'showitem' => '

                                --palette--;;filePalette'
                            ],
                            \TYPO3\CMS\Core\Resource\File::FILETYPE_TEXT => [
                                'showitem' => '

                                --palette--;;filePalette'
                            ],
                            \TYPO3\CMS\Core\Resource\File::FILETYPE_IMAGE => [
                                'showitem' => '

                                --palette--;;filePalette',
                            ],
                            \TYPO3\CMS\Core\Resource\File::FILETYPE_APPLICATION => [
                                'showitem' => '

                                --palette--;;filePalette'
                            ]
                        ],
                    ],
                    'foreign_match_fields' => [
                        'fieldname' => 'leaving_statement',
                        'tablenames' => 'tx_smartverein_domain_model_members',
                        'table_local' => 'sys_file',
                    ],
                    'maxitems' => 1
                ],
                'jpg,jpeg,pdf,docx,doc,odt'
            ),

        ],
        'declaration_of_consent' => [
            'exclude' => true,
            'label' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_members.declaration_of_consent',
            'description' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_members.declaration_of_consent.description',
            'displayCond' => 'REC:NEW:false',
            'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig(
                'declaration_of_consent',
                [
                    'appearance' => [
                        'createNewRelationLinkTitle' => 'LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:media.addFileReference',
                        'fileByUrlAllowed' => false,
                        'collapseAll' => true,
                    ],
                    'overrideChildTca' => [
                        'types' => [
                            '0' => [
                                'showitem' => '

                                --palette--;;filePalette'
                            ],
                            \TYPO3\CMS\Core\Resource\File::FILETYPE_TEXT => [
                                'showitem' => '

                                --palette--;;filePalette'
                            ],
                            \TYPO3\CMS\Core\Resource\File::FILETYPE_IMAGE => [
                                'showitem' => '

                                --palette--;;filePalette',
                            ],
                            \TYPO3\CMS\Core\Resource\File::FILETYPE_APPLICATION => [
                                'showitem' => '

                                --palette--;;filePalette'
                            ]
                        ],
                    ],
                    'foreign_match_fields' => [
                        'fieldname' => 'declaration_of_consent',
                        'tablenames' => 'tx_smartverein_domain_model_members',
                        'table_local' => 'sys_file',
                    ],
                    'maxitems' => 1
                ],
                'jpg,jpeg,pdf,docx,doc,odt'
            ),

        ],
        'profile_image' => [
            'exclude' => true,
            'label' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_members.profile_image',
            'description' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_members.profile_image.description',
            'displayCond' => 'REC:NEW:false',
            'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig(
                'profile_image',
                [
                    'appearance' => [
                        'createNewRelationLinkTitle' => 'LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:images.addFileReference',
                        'elementBrowserAllowed' => 'jpeg,png',
                    ],
                    'overrideChildTca' => [
                        'types' => [
                            '0' => [
                                'showitem' => '
                                --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                                --palette--;;filePalette'
                            ],
                            \TYPO3\CMS\Core\Resource\File::FILETYPE_IMAGE => [
                                'showitem' => '
                                --palette--;;imageoverlayPalette,
                                --palette--;;filePalette',
                            ],
                        ],
                    ],
                    'foreign_match_fields' => [
                        'fieldname' => 'profile_image',
                        'tablenames' => 'tx_smartverein_domain_model_members',
                        'table_local' => 'sys_file',
                    ],
                    'maxitems' => 1
                ],
                'jpg,jpeg'
            ),

        ],
        'member_folder' => [
            'exclude' => true,
            'label' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_members.member_folder',
            'description' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_members.member_folder.description',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim',
                //'default' => SmartvereinUtility::createUUIDV4(),
            ],
        ],
        'preferred_communication' => [
            'exclude' => true,
            'label' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_members.preferred_communication',
            'description' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_members.preferred_communication.description',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'items' => [
                    ['LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:preferred_communication.item.0', 0],
                    ['LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:preferred_communication.item.1', 1],
                    ['LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:preferred_communication.item.2', 2],
                ],
                'size' => 1,
                'maxitems' => 1,
                'eval' => ''
            ],
        ],
        'method_of_payment' => [
            'exclude' => true,
            'label' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_members.method_of_payment',
            'description' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_members.method_of_payment.description',
            'onChange' => 'reload',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'items' => [
                    ['LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:labels.tca.notSet', 0],
                    ['LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:method_of_payment.item.1', 1],
                    ['LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:method_of_payment.item.2', 2],
                    ['LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:method_of_payment.item.3', 3],
                ],
                'size' => 1,
                'maxitems' => 1,
                'eval' => ''
            ],
        ],
        'payment_interval' => [
            'exclude' => true,
            'label' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_members.payment_interval',
            'description' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_members.payment_interval.description',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'items' => [
                    ['LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:labels.tca.notSet', 0],
                    ['LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:payment_interval.item.1', 1],
                    ['LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:payment_interval.item.2', 2],
                    ['LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:payment_interval.item.3', 3],
                    ['LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:payment_interval.item.4', 4],
                ],
                'size' => 1,
                'maxitems' => 1,
                'eval' => ''
            ],
        ],
        'account_owner' => [
            'exclude' => true,
            'label' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_members.account_owner',
            'description' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_members.account_owner.description',
            'displayCond' => 'FIELD:method_of_payment:=:1',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim',
                'default' => ''
            ],
        ],
        'iban' => [
            'exclude' => true,
            'label' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_members.iban',
            'description' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_members.iban.description',
            'displayCond' => 'FIELD:method_of_payment:=:1',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim',
                'default' => ''
            ],
        ],
        'bic' => [
            'exclude' => true,
            'label' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_members.bic',
            'description' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_members.bic.description',
            'displayCond' => 'FIELD:method_of_payment:=:1',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim',
                'default' => ''
            ],
        ],
        'mandate_reference' => [
            'exclude' => true,
            'label' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_members.mandate_reference',
            'description' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_members.mandate_reference.description',
            'displayCond' => 'FIELD:method_of_payment:=:1',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim',
                'default' => ''
            ],
        ],
        'mandate_date' => [
            'exclude' => true,
            'label' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_members.mandate_date',
            'description' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_members.mandate_date.description',
            'displayCond' => 'FIELD:method_of_payment:=:1',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'size' => 7,
                'eval' => 'date',
                'default' => time()
            ],
        ],
        'sepa_mandate' => [
            'exclude' => true,
            'label' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_members.sepa_mandate',
            'description' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_members.sepa_mandate.description',
            'displayCond' => [
                'AND' => [
                    'FIELD:method_of_payment:=:1',
                    'displayCond' => 'REC:NEW:false',
                    ],
                ],
            'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig(
                'sepa_mandate',
                [
                    'appearance' => [
                        'createNewRelationLinkTitle' => 'LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:media.addFileReference'
                    ],
                    'overrideChildTca' => [
                        'types' => [
                            '0' => [
                                'showitem' => '

                                --palette--;;filePalette'
                            ],
                            \TYPO3\CMS\Core\Resource\File::FILETYPE_TEXT => [
                                'showitem' => '

                                --palette--;;filePalette'
                            ],
                            \TYPO3\CMS\Core\Resource\File::FILETYPE_IMAGE => [
                                'showitem' => '
                                --palette--;;imageoverlayPalette,
                                --palette--;;filePalette',
                            ],
                            \TYPO3\CMS\Core\Resource\File::FILETYPE_AUDIO => [
                                'showitem' => '

                                --palette--;;filePalette'
                            ],
                            \TYPO3\CMS\Core\Resource\File::FILETYPE_VIDEO => [
                                'showitem' => '

                                --palette--;;filePalette'
                            ],
                            \TYPO3\CMS\Core\Resource\File::FILETYPE_APPLICATION => [
                                'showitem' => '

                                --palette--;;filePalette'
                            ]
                        ],
                    ],
                    'foreign_match_fields' => [
                        'fieldname' => 'sepa_mandate',
                        'tablenames' => 'tx_smartverein_domain_model_members',
                        'table_local' => 'sys_file',
                    ],
                    'maxitems' => 1
                ],
                'jpg,jpeg,pdf,docx,doc,odt'
            ),

        ],
        'customfield1' => [
            'exclude' => true,
            'label' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_members.customfield1',
            'description' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_members.customfield1.description',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim',
                'default' => ''
            ],
        ],
        'customfield2' => [
            'exclude' => true,
            'label' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_members.customfield2',
            'description' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_members.customfield2.description',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim',
                'default' => ''
            ],
        ],
        'customfield3' => [
            'exclude' => true,
            'label' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_members.customfield3',
            'description' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_members.customfield3.description',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim',
                'default' => ''
            ],
        ],
        'customfield4' => [
            'exclude' => true,
            'label' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_members.customfield4',
            'description' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_members.customfield4.description',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim',
                'default' => ''
            ],
        ],
        'customfield5' => [
            'exclude' => true,
            'label' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_members.customfield5',
            'description' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_members.customfield5.description',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim',
                'default' => ''
            ],
        ],
        'customfield6' => [
            'exclude' => true,
            'label' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_members.customfield6',
            'description' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_members.customfield6.description',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim',
                'default' => ''
            ],
        ],
        'customfield7' => [
            'exclude' => true,
            'label' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_members.customfield7',
            'description' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_members.customfield7.description',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim',
                'default' => ''
            ],
        ],
        'customfield8' => [
            'exclude' => true,
            'label' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_members.customfield8',
            'description' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_members.customfield8.description',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim',
                'default' => ''
            ],
        ],
        'customfield9' => [
            'exclude' => true,
            'label' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_members.customfield9',
            'description' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_members.customfield9.description',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim',
                'default' => ''
            ],
        ],
        'customfield10' => [
            'exclude' => true,
            'label' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_members.customfield10',
            'description' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_members.customfield10.description',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim',
                'default' => ''
            ],
        ],
        'fe_user' => [
            'exclude' => true,
            'label' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_members.fe_user',
            'description' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_members.fe_user.description',
            'config'  => [
                'type'          => 'inline',
                'foreign_table' => 'fe_users',
                'foreign_field' => 'smartverein_member',
                'maxitems'      => 1,
                'appearance'    => [
                    'collapseAll'                     => true,
                ],
                'overrideChildTca' => [
                    'types' => [
                        '0' => [
                            'showitem' => 'username,password,usergroup,lastlogin,lastreminderemailsent',
                        ],
                    ],
                    'columns' => [
                        'username' => [
                            'label' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_members.fe_user.username',
                            'description' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_members.fe_user..username.description',

                        ],
                        'password' => [
                            'label' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_members.fe_user.password',
                            'description' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_members.fe_user.password.description',
                            'config' => [
                                'eval' => 'trim,password,saltedPassword',
                            ]
                        ],
                        'usergroup' => [
                            'config' => [
                                'default' => 1,
                            ],
                        ]
                    ],
                ],
            ],
        ],
        'contacts' => [
            'exclude' => true,
            'label' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_members.contacts',
            'description' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_members.contacts.description',
            'config' => [
                'type' => 'inline',
                'foreign_table' => 'tx_smartverein_domain_model_contacts',
                'foreign_field' => 'members',
                'foreign_sortby' => 'sorting',
                'maxitems' => 9999,
                'appearance' => [
                    'collapseAll' => 1,
                    'levelLinksPosition' => 'top',
                    'showSynchronizationLink' => 1,
                    'showPossibleLocalizationRecords' => 1,
                    'useSortable' => 1,
                    'showAllLocalizationLink' => 1
                ],
            ],

        ],
        'belongsto' => [
            'exclude' => true,
            'label' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_members.belongsto',
            'description' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_members.belongsto.description',
            'displayCond' => 'FIELD:member_type:=:0',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectMultipleSideBySide',
                'foreign_table' => 'tx_smartverein_domain_model_members',
                'default' => 0,
                'size' => 10,
                'autoSizeMax' => 30,
                'maxitems' => 9999,
                'multiple' => 0,
                'fieldControl' => [
                    'editPopup' => [
                        'disabled' => true,
                    ],
                    'addRecord' => [
                        'disabled' => true,
                    ],
                    'listModule' => [
                        'disabled' => true,
                    ],
                ],
            ],

        ],
        'functions' => [
            'exclude' => true,
            'label' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_members.functions',
            'description' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_members.functions.description',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectMultipleSideBySide',
                'foreign_table' => 'tx_smartverein_domain_model_functions',
                'default' => 0,
                'size' => 10,
                'autoSizeMax' => 30,
                'maxitems' => 9999,
                'multiple' => 0,
                'fieldControl' => [
                    'editPopup' => [
                        'disabled' => true,
                    ],
                    'addRecord' => [
                        'disabled' => true,
                    ],
                    'listModule' => [
                        'disabled' => true,
                    ],
                ],
            ],

        ],
        'positions' => [
            'exclude' => true,
            'label' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_members.positions',
            'description' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_members.positions.description',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectMultipleSideBySide',
                'foreign_table' => 'tx_smartverein_domain_model_positions',
                'default' => 0,
                'size' => 10,
                'autoSizeMax' => 30,
                'maxitems' => 9999,
                'multiple' => 0,
                'fieldControl' => [
                    'editPopup' => [
                        'disabled' => true,
                    ],
                    'addRecord' => [
                        'disabled' => true,
                    ],
                    'listModule' => [
                        'disabled' => true,
                    ],
                ],
            ],

        ],
        'role' => [
            'exclude' => true,
            'label' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_members.role',
            'description' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_members.role.description',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'items' => [
                    ['LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:labels.tca.notSet', 0],
                ],
                'foreign_table' => 'tx_smartverein_domain_model_roles',
                'size'                => 1,
                'default'             => 0,
                'minitems'            => 0,
                'maxitems'            => 1,
            ],
        ],
        'sections' => [
            'exclude' => true,
            'label' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_members.sections',
            'description' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_members.sections.description',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectMultipleSideBySide',
                'foreign_table' => 'tx_smartverein_domain_model_sections',
                'default' => 0,
                'size' => 10,
                'autoSizeMax' => 30,
                'maxitems' => 9999,
                'multiple' => 0,
                'fieldControl' => [
                    'editPopup' => [
                        'disabled' => true,
                    ],
                    'addRecord' => [
                        'disabled' => true,
                    ],
                    'listModule' => [
                        'disabled' => true,
                    ],
                ],
            ],

        ],
        'vita' => [
            'exclude' => true,
            'label' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_members.vita',
            'description' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_members.vita.description',
            'displayCond' => 'FIELD:member_type:=:0',
            'config' => [
                'type' => 'inline',
                'foreign_table' => 'tx_smartverein_domain_model_vitaentries',
                'foreign_field' => 'members',
                'foreign_default_sortby' => 'ORDER BY date DESC',
                'maxitems' => 9999,
                'appearance' => [
                    'collapseAll' => 1,
                    'levelLinksPosition' => 'top',
                    'showSynchronizationLink' => 1,
                    'showPossibleLocalizationRecords' => 1,
                    'showAllLocalizationLink' => 1,
                ],
            ],
        ],
        'member_fee' => [
            'exclude' => true,
            'label' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_members.member_fee',
            'description' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_members.member_fee.description',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectMultipleSideBySide',
                'foreign_table' => 'tx_smartverein_domain_model_memberfees',
                'default' => 0,
                'size' => 10,
                'autoSizeMax' => 30,
                'maxitems' => 9999,
                'multiple' => 0,
                'fieldControl' => [
                    'editPopup' => [
                        'disabled' => true,
                    ],
                    'addRecord' => [
                        'disabled' => true,
                    ],
                    'listModule' => [
                        'disabled' => true,
                    ],
                ],
            ],

        ],
        'changelog' => [
            'exclude' => true,
            'label' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_members.changelog',
            'description' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_members.changelog.description',
            'config' => [
                'type' => 'inline',
                'readOnly' => true,
                'foreign_table' => 'tx_smartverein_domain_model_changes',
                'foreign_field' => 'members',
                'foreign_default_sortby' => 'ORDER BY date DESC',
                'maxitems' => 9999,
                'appearance' => [
                    'collapseAll' => 1,
                    'levelLinksPosition' => 'top',
                    'showSynchronizationLink' => 1,
                    'showPossibleLocalizationRecords' => 1,
                    'showAllLocalizationLink' => 1
                ],
            ],

        ],
    ],
];
