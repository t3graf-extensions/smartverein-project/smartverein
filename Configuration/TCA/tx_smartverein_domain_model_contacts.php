<?php

/*
 * This file is part of the "SmartVerein - TYPO3 Club Management" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2023 Development-Team <development@t3graf-media.de>, T3graf media-agentur UG
 */

return [
    'ctrl' => [
        'title' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_contacts',
        'label' => 'type',
        'label_alt' => 'contact',
        'label_alt_force' => true,
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'cruser_id' => 'cruser_id',
        'sortby' => 'sorting',
        'versioningWS' => true,
        'languageField' => 'sys_language_uid',
        'transOrigPointerField' => 'l10n_parent',
        'transOrigDiffSourceField' => 'l10n_diffsource',
        'delete' => 'deleted',
        //'hideTable' => 1,
        'enablecolumns' => [
            'disabled' => 'hidden',
            'starttime' => 'starttime',
            'endtime' => 'endtime',
        ],
        'searchFields' => 'contact',
        'iconfile' => 'EXT:smartverein/Resources/Public/Icons/Database/contacts.svg'
    ],
    'types' => [
        '1' => ['showitem' => 'type, contact, --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:access, hidden'],
    ],
    'columns' => [
        'hidden' => [
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.visible',
            'config' => [
                'type' => 'check',
                'renderType' => 'checkboxToggle',
                'items' => [
                    [
                        0 => '',
                        1 => '',
                        'invertStateDisplay' => true
                    ]
                ],
            ],
        ],
        'type' => [
            'exclude' => true,
            'label' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_contacts.type',
            'description' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_contacts.type.description',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'items' => [
                    ['LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_contact.type.item.label.privat', '--div--'],
                    ['LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_contact.type.item.1', 1],
                    ['LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_contact.type.item.2', 2],
                    ['LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_contact.type.item.3', 3],
                    ['LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_contact.type.item.label.business', '--div--'],
                    ['LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_contact.type.item.5', 5],
                    ['LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_contact.type.item.6', 6],
                    ['LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_contact.type.item.7', 7],
                ],
                'size' => 1,
                'maxitems' => 1,
                'eval' => '',
                'default' => 1,
            ],
        ],
        'contact' => [
            'exclude' => true,
            'label' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_contacts.contact',
            'description' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_contacts.contact.description',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim',
                'default' => ''
            ],
        ],

        'members' => [
            'config' => [
                'type' => 'passthrough',
            ],
        ],
        'addressbook' => [
            'config' => [
                'type' => 'passthrough',
            ],
        ],
    ],
];
