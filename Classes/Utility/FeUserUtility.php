<?php

/*
 * This file is part of the "SmartVerein - TYPO3 Club Management" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2023 Development-Team <development@t3graf-media.de>, T3graf media-agentur UG
 */

namespace T3graf\Smartverein\Utility;

use T3graf\Smartverein\Mail\MailService;
use TYPO3\CMS\Core\Configuration\ExtensionConfiguration;
use TYPO3\CMS\Core\Context\Context;
use TYPO3\CMS\Core\Crypto\PasswordHashing\PasswordHashFactory;
use TYPO3\CMS\Core\Crypto\Random;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Site\SiteFinder;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Security\Cryptography\HashService;

class FeUserUtility
{
    /**
     * @throws \Symfony\Component\Mailer\Exception\TransportExceptionInterface
     * @throws \TYPO3\CMS\Core\Crypto\PasswordHashing\InvalidPasswordHashException
     * @throws \Doctrine\DBAL\DBALException
     * @throws \Doctrine\DBAL\Driver\Exception
     * @throws \TYPO3\CMS\Core\Configuration\Exception\ExtensionConfigurationPathDoesNotExistException
     * @throws \TYPO3\CMS\Core\Configuration\Exception\ExtensionConfigurationExtensionNotConfiguredException
     * @throws \TYPO3\CMS\Core\Context\Exception\AspectNotFoundException
     * @throws \TYPO3\CMS\Core\Exception\SiteNotFoundException
     * @throws \TYPO3\CMS\Extbase\Security\Exception\InvalidArgumentForHashGenerationException
     */
    public static function createFeUser($uid)
    {
        $password = SmartvereinUtility::createRandomPassword();
        $hashInstance = GeneralUtility::makeInstance(PasswordHashFactory::class)->getDefaultHashInstance('FE');
        $hashedPassword = $hashInstance->getHashedPassword($password);
        $defaultMemberFeGroup = SmartvereinUtility::getExtConfProperty('smartverein', 'settings.defaultFeUserGroupUid');

        $connectionPool = GeneralUtility::makeInstance(ConnectionPool::class);
        $queryBuilder = $connectionPool->getQueryBuilderForTable('tx_smartverein_domain_model_members');
        $result = $queryBuilder
            ->select('*')
            ->from('tx_smartverein_domain_model_members')
            ->where(
                $queryBuilder->expr()->eq('uid', $uid)
            )
            ->executeQuery();
        $member = $result->fetchAssociative();
        $username = self::createUsername($member);

        if ($username) {
            $queryBuilder = $connectionPool->getQueryBuilderForTable('fe_users');
            $result = $queryBuilder
                ->insert('fe_users')
                ->values([
                    'username' => $username,
                    'password' => $hashedPassword,
                    'first_name' => $member['firstname'],
                    'last_name' => $member['lastname'],
                    'email' => $member['email'],
                    'company' => $member['organisation'],
                    'name' => $member['contact_person'],
                    'usergroup' => $defaultMemberFeGroup,
                    'smartverein_member' => $uid,
                    'pid' => SmartvereinUtility::getExtConfProperty('smartverein', 'settings.memberStoragePid'),
                ])
                ->executeStatement();

            $queryBuilder = $connectionPool->getQueryBuilderForTable('tx_smartverein_domain_model_members');
            $result = $queryBuilder
                ->update('tx_smartverein_domain_model_members')
                ->where(
                    $queryBuilder->expr()->eq('uid', $uid)
                )
                ->set('fe_user', $uid)
                ->executeStatement();
        }

        $loginPid = (int)GeneralUtility::makeInstance(ExtensionConfiguration::class)
            ->get('smartverein', 'settings/feUsersLoginPid');
        $passwordRecoveryLifeTime = (int)GeneralUtility::makeInstance(ExtensionConfiguration::class)
            ->get('smartverein', 'settings/passwordRecoveryLifeTime');
        $forgotHash = self::generateForgotHash($uid, $passwordRecoveryLifeTime);

        switch ($member['preferred_communication']) {
            case 0:
                // Sent Mail to member
                if ($member['email']) {
                    $variables = [
                        'member' => $member,
                        'username' => $username,
                        'beUser' => SmartvereinUtility::getBeUsername(),
                        'passwordRecoveryLifeTime' => self::getLifeTimeTimestamp($passwordRecoveryLifeTime),
                        'recoveryLink' => self::generateRecoveryLink($loginPid, $forgotHash),
                        'loginLink' => self::generateLoginLink($loginPid),
                    ];

                    $recipient = [
                        'address' => $member['email'],
                        'name' => $member['firstname'] . ' ' . $member['lastname'],
                    ];

                    MailService::sendSmartvereinMail($recipient, \TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate('email.subject.logindata', 'smartverein', arguments: [0=>GeneralUtility::makeInstance(ExtensionConfiguration::class)
                        ->get('smartverein', 'club/name')]), 'Member/SendAccessData', $variables);
                }

                break;
            case 1:
                //send letter to member
            default:
                // do nothing
        }
        return;
    }

    /**
     * @throws \TYPO3\CMS\Core\Context\Exception\AspectNotFoundException
     * @throws \Doctrine\DBAL\DBALException
     * @throws \Doctrine\DBAL\Driver\Exception
     * @throws \TYPO3\CMS\Core\Exception\SiteNotFoundException
     * @throws \TYPO3\CMS\Extbase\Security\Exception\InvalidArgumentForHashGenerationException
     * @throws \TYPO3\CMS\Core\Configuration\Exception\ExtensionConfigurationPathDoesNotExistException
     * @throws \TYPO3\CMS\Core\Configuration\Exception\ExtensionConfigurationExtensionNotConfiguredException
     */
    public static function updateFeUserData($uid)
    {
        $connectionPool = GeneralUtility::makeInstance(ConnectionPool::class);
        $queryBuilder = $connectionPool->getQueryBuilderForTable('tx_smartverein_domain_model_members');
        $result = $queryBuilder
            ->select('*')
            ->from('tx_smartverein_domain_model_members')
            ->where(
                $queryBuilder->expr()->eq('uid', $uid)
            )
            ->executeQuery();
        $member = $result->fetchAssociative();
        if ($member['fe_user']) {
            $queryBuilder = $connectionPool->getQueryBuilderForTable('fe_users');
            $result = $queryBuilder
                ->update('fe_users')
                ->where(
                    $queryBuilder->expr()->eq('uid', $uid)
                )
                ->set('email', $member['email'])
                ->set('company', $member['organisation'])
                ->set('name', $member['contact_person'])
                ->executeStatement();
        }
        $queryBuilder = $connectionPool->getQueryBuilderForTable('tx_smartverein_domain_model_members');
        $result = $queryBuilder
            ->update('tx_smartverein_domain_model_members')
            ->where(
                $queryBuilder->expr()->eq('uid', $uid)
            )
            ->set('fe_user', $uid)
            ->executeStatement();

        $loginPid = (int)GeneralUtility::makeInstance(ExtensionConfiguration::class)
            ->get('smartverein', 'settings/feUsersLoginPid');
        $passwordRecoveryLifeTime = (int)GeneralUtility::makeInstance(ExtensionConfiguration::class)
            ->get('smartverein', 'settings/passwordRecoveryLifeTime');
        $forgotHash = self::generateForgotHash($uid, $passwordRecoveryLifeTime);
        //Todo get username from fe_user
        switch ($member['preferred_communication']) {
            case 0:
                // Sent Mail to member
                if ($member['email']) {
                    $variables = [
                        'member' => $member,
                        'beUser' => SmartvereinUtility::getBeUsername(),
                        'passwordRecoveryLifeTime' => self::getLifeTimeTimestamp($passwordRecoveryLifeTime),
                        'recoveryLink' => self::generateRecoveryLink($loginPid, $forgotHash),
                        'loginLink' => self::generateLoginLink($loginPid),
                    ];

                    $recipient = [
                        'address' => $member['email'],
                        'name' => $member['firstname'] . ' ' . $member['lastname'],
                    ];

                    MailService::sendSmartvereinMail($recipient, \TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate('email.subject.logindata', 'smartverein', arguments: [0=>GeneralUtility::makeInstance(ExtensionConfiguration::class)
                        ->get('smartverein', 'club/name')]), 'Member/SendAccessData', $variables);
                }

                break;
            case 1:
                //send letter to member and email to admin
            default:
                // send email to admin
        }
        return;
    }

    private static function createUsername($member)
    {
        $username = '';
        if ($member['firstname'] === '' && $member['lastname'] === '' && $member['organisation'] === '' && $member['contact_person'] === '') {
            $username = $member['member_id'];
        }

        if (($member['firstname'] !== '' || $member['lastname'] !== '') && $username === '' && $member['member_type'] === 0) {
            $username = $member['firstname'] . $member['lastname'];
        }

        if (($member['organisation'] !== '' || $member['contact_person'] !== '') && $username === '' && $member['member_type'] === 1) {
            if ($member['organisation'] !== '') {
                $username = preg_replace('/[^a-zA-Z0-9]+/', '', $member['organisation']);
            }
            if ($member['contact_person'] !== '' && $username !== '') {
                $username = preg_replace('/[^a-zA-Z0-9]+/', '', $member['contact_person']);
            }
        }

        return $username;
    }

    /**
     * Returns timestamp of the forgot hash
     *
     * @param int $passwordRecoveryLifeTime
     * @return int
     * @throws \TYPO3\CMS\Core\Context\Exception\AspectNotFoundException
     */
    public static function getLifeTimeTimestamp(int $passwordRecoveryLifeTime): int
    {
        $context = GeneralUtility::makeInstance(Context::class);
        $currentTimestamp = $context->getPropertyFromAspect('date', 'timestamp');
        return $currentTimestamp + 3600 * $passwordRecoveryLifeTime;
    }

    /**
     * @throws \TYPO3\CMS\Core\Context\Exception\AspectNotFoundException
     * @throws \TYPO3\CMS\Extbase\Security\Exception\InvalidArgumentForHashGenerationException
     * @throws \Doctrine\DBAL\DBALException
     */
    protected static function generateForgotHash(int $feUserUid, int $passwordRecoveryLifeTime): string
    {
        $random = GeneralUtility::makeInstance(Random::class);
        $hashService = GeneralUtility::makeInstance(HashService::class);
        $randomString = $random->generateRandomHexString(16);
        $forgotHash = self::getLifeTimeTimestamp($passwordRecoveryLifeTime) . '|' . $hashService->generateHmac($randomString);

        $connectionPool = GeneralUtility::makeInstance(ConnectionPool::class);
        $queryBuilder = $connectionPool->getQueryBuilderForTable('fe_users');
        $result = $queryBuilder
            ->update('fe_users')
            ->where(
                $queryBuilder->expr()->eq('uid', $feUserUid)
            )
            ->set('felogin_forgotHash', GeneralUtility::hmac($forgotHash))
            ->executeStatement();
        return $forgotHash;
    }

    /**
     * @throws \TYPO3\CMS\Core\Exception\SiteNotFoundException
     */
    private static function generateLoginLink(int $loginPid): string
    {
        $parameters = [];
        $site = GeneralUtility::makeInstance(SiteFinder::class)->getSiteByPageId($loginPid);
        return (string) $site->getRouter()->generateUri($loginPid, $parameters);
    }

    /**
     * @throws \TYPO3\CMS\Core\Exception\SiteNotFoundException
     */
    private static function generateRecoveryLink(int $loginPid, string $forgotHash): string
    {
        //
        // snippet from https://various.at/news/typo3-uribuilder-im-backend-context
        $site = GeneralUtility::makeInstance(SiteFinder::class)->getSiteByPageId($loginPid);
        $parameters = [
            'tx_felogin_login' => [
                'action' => 'showChangePassword',
                'controller' => 'PasswordRecovery',
                'hash' => $forgotHash
            ]
        ];

        return (string) $site->getRouter()->generateUri(
            $loginPid,
            $parameters
        );
    }
}
