<?php

/*
 * This file is part of the "SmartVerein - TYPO3 Club Management" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2023 Development-Team <development@t3graf-media.de>, T3graf media-agentur UG
 */

namespace T3graf\Smartverein\Utility;

use TYPO3\CMS\Core\Configuration\ExtensionConfiguration;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\TypoScript\TypoScriptService;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Configuration\ConfigurationManager;
use TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface;

class SmartvereinUtility
{
    /**
     * @throws \TYPO3\CMS\Core\Configuration\Exception\ExtensionConfigurationPathDoesNotExistException
     * @throws \TYPO3\CMS\Core\Configuration\Exception\ExtensionConfigurationExtensionNotConfiguredException
     */
    public static function getExtConfProperty($extKey, $propName)
    {
        $extensionConfiguration = GeneralUtility::makeInstance(ExtensionConfiguration::class);
        $propName = str_replace('.', '/', $propName);
        return $extensionConfiguration->get($extKey, $propName);
    }

    public static function getTSSettings($extKey)
    {
        $configurationManager = GeneralUtility::makeInstance(ConfigurationManager::class);
        $typoscriptArray = GeneralUtility::makeInstance(TypoScriptService::class);
        $typoscript = $configurationManager->getConfiguration(
            ConfigurationManagerInterface::CONFIGURATION_TYPE_FULL_TYPOSCRIPT,
            $extKey
        );

        return $typoscriptArray->convertTypoScriptArrayToPlainArray($typoscript[$extKey . '.']['settings.']);
    }

    public static function getTSTemplatePaths($extKey, $template='mail')
    {
        $configurationManager = GeneralUtility::makeInstance(ConfigurationManager::class);
        $typoscriptArray = GeneralUtility::makeInstance(TypoScriptService::class);
        $typoscript = $configurationManager->getConfiguration(
            ConfigurationManagerInterface::CONFIGURATION_TYPE_FULL_TYPOSCRIPT,
            $extKey
        );
        debug($typoscriptArray->convertTypoScriptArrayToPlainArray($typoscript['module.']['tx_smartverein.']));
        die();
        return $typoscriptArray->convertTypoScriptArrayToPlainArray(['module.']['tx_smartverein.'][$template . '.']);
    }

    public static function getTsProperty($extKey, $propName)
    {
        $extensionSettings = $tsSettings = self::getTSSettings($extKey);
        // \TYPO3\CMS\Core\Utility\DebugUtility::debug($extensionSettings, 'Debug: ' . __FILE__ . ' in Line: ' . __LINE__);
        // \TYPO3\CMS\Core\Utility\DebugUtility::debug(array_key_exists($propName, $extensionSettings), 'Debug: ' . __FILE__ . ' in Line: ' . __LINE__);
        // die();
        if (!array_key_exists($propName, $extensionSettings)) {
            return;
        }

        // \TYPO3\CMS\Core\Utility\DebugUtility::debug($extensionSettings, 'Debug: ' . __FILE__ . ' in Line: ' . __LINE__);
        // die();
        return $extensionSettings->get($extKey, $propName);
    }

    /**
     * @throws \Doctrine\DBAL\Driver\Exception
     * @throws \Doctrine\DBAL\DBALException
     */
    public static function createMemberID($uid)
    {
        $connectionPool = GeneralUtility::makeInstance(ConnectionPool::class);
        $tsSettings = self::getTSSettings('smartverein');
        $memberID = '';
        if ($tsSettings['members']['autoMembershipNumber']) {
            $pattern = $tsSettings['members']['membershipNumberPattern'];
            if ($pattern !== '') {
                $dateFormat = $tsSettings['formats']['date'];
                $birthdayFormat = str_replace(
                    ['.', ',', ':', '-', 'D', 'j', 'l', 'F', 'M', 'n'],
                    ['', '', '', '', 'd', 'd', 'd', 'm', 'm', 'm'],
                    $dateFormat
                );
                $dateFormat = str_replace(
                    ['.', ',', ':', '-', 'D', 'j', 'l', 'F', 'M', 'n', 'Y'],
                    ['', '', '', '', 'd', 'd', 'd', 'm', 'm', 'm', 'y'],
                    $dateFormat
                );
                $memberID = $pattern;

                // todo #V und #N für die Anfangsbuchstaben von Vorname und Name
                $queryBuilder = $connectionPool->getQueryBuilderForTable('tx_smartverein_domain_model_members');
                $result = $queryBuilder
                    ->select('firstname', 'lastname', 'dateofbirth', 'entry_date')
                    ->from('tx_smartverein_domain_model_members')
                    ->where(
                        $queryBuilder->expr()->eq('uid', $uid)
                    )
                    ->executeQuery();
                $member = $result->fetchAssociative();
                //debug(substr($member['lastname'],0,1));die();
                $memberID = str_replace(['#YYYY', '#DDDDDD', '#BBBBBBBB', '#EEEEEE'], [date('Y'), date($dateFormat), date($birthdayFormat, $member['dateofbirth']), date($dateFormat, $member['entry_date'])], $memberID);
                $memberID = str_replace('#N', substr($member['lastname'], 0, 1), $memberID);
                $memberID = str_replace('#V', substr($member['firstname'], 0, 1), $memberID);

                if (str_contains($memberID, '#c')) {
                    $queryBuilder = $connectionPool->getQueryBuilderForTable('tx_smartverein_domain_model_members');
                    $count = $queryBuilder
                        ->count('uid')
                        ->from('tx_smartverein_domain_model_members')
                        ->executeQuery()
                        ->fetchOne();
                    //debug($count);die();
                    $tsSettings = self::getTSSettings('smartverein');
                    $count = $count + $tsSettings['members']['membershipNumberStartnumeration'];
                    $memberID = str_replace('#cn', str_pad($count, 4, 0, STR_PAD_LEFT), $memberID);
                }
                while (strpos($memberID, '#n')) {
                    $memberID = substr_replace($memberID, rand(0, 9), strpos($memberID, '#n'), 2);
                }
                while (strpos($memberID, '#C')) {
                    $memberID = substr_replace($memberID, substr(str_shuffle('ABCDEFGHIJKLMNOPQRSTUVWXYZ'), 0, 1), strpos($memberID, '#C'), 2);
                }
            }
        }
        krexxlog($memberID);
        return $memberID;
    }

    public static function createRandomPassword(): string
    {
        $tsSettings = self::getTSSettings('smartverein');
        $passwordlength = $tsSettings['security']['passwordLength'];
        $comb = 'abcdefjhijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890!$%&=?*-:;.,+~@_';
        $random = str_shuffle($comb);
        return substr($random, 0, $passwordlength);
    }

    public static function getBeUsername()
    {
        $user = '';
        if (isset($GLOBALS['BE_USER'])) {
            if ($GLOBALS['BE_USER']->user['realName']) {
                $user = $GLOBALS['BE_USER']->user['realName'];
            } else {
                $user = $GLOBALS['BE_USER']->user['username'];
            }
        }
        return $user;
    }

    public static function createUUIDV4($data = null)
    {
        // Generate 16 bytes (128 bits) of random data or use the data passed into the function.
        $data = $data ?? random_bytes(16);
        assert(strlen($data) == 16);

        // Set version to 0100
        $data[6] = chr(ord($data[6]) & 0x0f | 0x40);
        // Set bits 6-7 to 10
        $data[8] = chr(ord($data[8]) & 0x3f | 0x80);

        // Output the 36 character UUID.
        return vsprintf('%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex($data), 4));
    }

    /**
     * @throws \Doctrine\DBAL\Driver\Exception
     * @throws \Doctrine\DBAL\DBALException
     */
    public static function createMemberFolderName($uid)
    {
        $connectionPool = GeneralUtility::makeInstance(ConnectionPool::class);
        $queryBuilder = $connectionPool->getQueryBuilderForTable('tx_smartverein_domain_model_members');
        $result = $queryBuilder
            ->select('firstname', 'lastname', 'member_id')
            ->from('tx_smartverein_domain_model_members')
            ->where(
                $queryBuilder->expr()->eq('uid', $uid)
            )
            ->executeQuery();

        $member = $result->fetchAssociative();

        return str_replace(['Ä', 'Ö', 'Ü', 'ä', 'ö', 'ü', 'ß', ' '], ['Ae', 'Oe', 'Ue', 'ae', 'oe', 'ue', 'ss', ''], $member['lastname'] . '_' . $member['firstname'] . '_' . $member['member_id']);
    }
}
