<?php

/*
 * This file is part of the "SmartVerein - TYPO3 Club Management" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2023 Development-Team <development@t3graf-media.de>, T3graf media-agentur UG
 */

namespace T3graf\Smartverein\Utility;

use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class MemberUtility
{
    /**
     * @throws \Doctrine\DBAL\DBALException
     */
    public static function setVitaEntryAuthor($uid)
    {
        $connectionPool = GeneralUtility::makeInstance(ConnectionPool::class);

        $queryBuilder = $connectionPool->getQueryBuilderForTable('tx_smartverein_domain_model_vitaentries');
        $result = $queryBuilder
            ->select('*')
            ->from('tx_smartverein_domain_model_vitaentries')
            ->where(
                $queryBuilder->expr()->eq('uid', $uid)
            )
            ->executeQuery();
        $vita = $result->fetchAssociative();

        if (!$vita['entered_by']) {
            $queryBuilder = $connectionPool->getQueryBuilderForTable('tx_smartverein_domain_model_vitaentries');
            $result = $queryBuilder
                ->update('tx_smartverein_domain_model_vitaentries')
                ->where(
                    $queryBuilder->expr()->eq('uid', $uid)
                )
                ->set('entered_by', SmartvereinUtility::getBeUsername())
                ->executeStatement();
        }
    }

    /**
     * @throws \Doctrine\DBAL\DBALException
     * @throws \Doctrine\DBAL\Driver\Exception
     */
    public static function setMemberID($uid)
    {
        $connectionPool = GeneralUtility::makeInstance(ConnectionPool::class);
        $queryBuilder = $connectionPool->getQueryBuilderForTable('tx_smartverein_domain_model_members');
        $result = $queryBuilder
            ->update('tx_smartverein_domain_model_members')
            ->where(
                $queryBuilder->expr()->eq('uid', $uid)
            )
            ->set('member_id', SmartvereinUtility::createMemberID($uid))
            ->executeStatement();
    }

    /**
     * @throws \Doctrine\DBAL\DBALException
     * @throws \Doctrine\DBAL\Driver\Exception
     */
    public static function setMemberFolderName($uid)
    {
        $connectionPool = GeneralUtility::makeInstance(ConnectionPool::class);
        $queryBuilder = $connectionPool->getQueryBuilderForTable('tx_smartverein_domain_model_members');
        $result = $queryBuilder
            ->update('tx_smartverein_domain_model_members')
            ->where(
                $queryBuilder->expr()->eq('uid', $uid)
            )
            ->set('member_folder', SmartvereinUtility::createMemberFolderName($uid))
            ->executeStatement();
    }

    /**
     * @throws \Doctrine\DBAL\Driver\Exception
     * @throws \Doctrine\DBAL\DBALException
     */
    public static function getMemberFolder($uid)
    {
        $connectionPool = GeneralUtility::makeInstance(ConnectionPool::class);
        $queryBuilder = $connectionPool->getQueryBuilderForTable('tx_smartverein_domain_model_members');
        $result = $queryBuilder
            ->select('member_folder')
            ->from('tx_smartverein_domain_model_members')
            ->where(
                $queryBuilder->expr()->eq('uid', $uid)
            )
            ->executeQuery();
        $member = $result->fetchAssociative();
        return $member['member_folder'];
    }

    /**
     * @throws \Doctrine\DBAL\Driver\Exception
     * @throws \Doctrine\DBAL\DBALException
     */
    public static function getMemberEmail($uid)
    {
        $connectionPool = GeneralUtility::makeInstance(ConnectionPool::class);
        $queryBuilder = $connectionPool->getQueryBuilderForTable('tx_smartverein_domain_model_members');
        $result = $queryBuilder
            ->select('email')
            ->from('tx_smartverein_domain_model_members')
            ->where(
                $queryBuilder->expr()->eq('uid', $uid)
            )
            ->executeQuery();
        $member = $result->fetchAssociative();
        return $member['email'];
    }

    /**
     * @throws \Doctrine\DBAL\Driver\Exception
     * @throws \Doctrine\DBAL\DBALException
     */
    public static function getMemberCountry($uid)
    {
        $connectionPool = GeneralUtility::makeInstance(ConnectionPool::class);
        $queryBuilder = $connectionPool->getQueryBuilderForTable('static_countries');
        $result = $queryBuilder
            ->select('*')
            ->from('static_countries')
            ->where(
                $queryBuilder->expr()->eq('uid', $uid)
            )
            ->executeQuery();
        $row = $result->fetchAssociative();
        return $row['cn_short_' . SmartvereinUtility::getExtConfProperty('smartverein', 'settings.siteLanguage')];
    }

    /**
     * @throws \Doctrine\DBAL\Driver\Exception
     * @throws \Doctrine\DBAL\DBALException
     */
    public static function getMemberState($uid)
    {
        if ($uid === '0') {
            return '';
        }
        $connectionPool = GeneralUtility::makeInstance(ConnectionPool::class);
        $queryBuilder = $connectionPool->getQueryBuilderForTable('static_country_zones');
        $result = $queryBuilder
            ->select('*')
            ->from('static_country_zones')
            ->where(
                $queryBuilder->expr()->eq('uid', $uid)
            )
            ->executeQuery();
        $row = $result->fetchAssociative();
        return $row['zn_name_local'];
    }
}
