<?php

/*
 * This file is part of the "SmartVerein - TYPO3 Club Management" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2023 Development-Team <development@t3graf-media.de>, T3graf media-agentur UG
 */

namespace T3graf\Smartverein\Utility;

use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Fluid\View\StandaloneView;

class CsvUtility
{
    public function generateCsvFileContent($members, array $variables = []): string
    {
        $standaloneView = GeneralUtility::makeInstance(StandaloneView::class);
        $standaloneView->setLayoutRootPaths(
            [GeneralUtility::getFileAbsFileName('EXT:smartverein/Resources/Private/Backend/Layouts')]
        );
        $standaloneView->setPartialRootPaths(
            [GeneralUtility::getFileAbsFileName('EXT:smartverein/Resources/Private/Backend/Partials')]
        );
        $standaloneView->setTemplateRootPaths(
            [GeneralUtility::getFileAbsFileName('EXT:smartverein/Resources/Private/Backend/Templates')]
        );
        $standaloneView->setFormat('html');
        $standaloneView->setTemplate('Csv/CsvContent');
        $standaloneView->assignMultiple(['members' => $members], $variables);
        return $standaloneView->render();
    }
}
