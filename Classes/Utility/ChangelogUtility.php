<?php

/*
 * This file is part of the "SmartVerein - TYPO3 Club Management" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2023 Development-Team <development@t3graf-media.de>, T3graf media-agentur UG
 */

namespace T3graf\Smartverein\Utility;

use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Utility\LocalizationUtility;

class ChangelogUtility
{
    /**
     * @throws \Doctrine\DBAL\DBALException
     */
    public static function createChangelogEntries(string $member, array $entries, array $datamap): void
    {
        $tsSettings = SmartvereinUtility::getTSSettings('smartverein');
        $dateFormat = $tsSettings['formats']['date'];
        $timeFormat = $tsSettings['formats']['time'];
        $dateTimeFormat = $tsSettings['formats']['dateTime'];
        $history = '';
        $memberChanges = [];
        if (array_key_exists('tx_smartverein_domain_model_members:' . $member, $entries)) {
            $memberChanges = array_merge_recursive($entries['tx_smartverein_domain_model_members:' . $member]['oldRecord'], $entries['tx_smartverein_domain_model_members:' . $member]['newRecord']);
        }

        // NEW Values for Files
        foreach ($datamap as $memberField => $changes) {
            switch ($memberField) {
                case 'changelog':
                    break;
                case 'vita':
                    break;
                case 'profile_image':
                case 'leaving_statement':
                case 'declaration_of_consent':
                case 'sepa_mandate':
                case 'membership_application':
                    if (str_starts_with($changes, 'NEW')) {
                        $history .= self::translateMemberField($memberField) . ' ' . LocalizationUtility::translate('LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:labels.uploaded') . PHP_EOL;
                    }
                    break;
                default:
                    //$history .= 'DEFAULT NEW: '.self::translateMemberField($memberField) . ': ';
            }
        }

        // Update and delete of values
        if ($memberChanges) {
            foreach ($memberChanges as $memberField => $changes) {
                switch ($memberField) {
                    case 'l10n_diffsource':
                    case 'changelog':
                        break;
                    case 'salutation':
                        $memberField = self::translateMemberField($memberField);
                        $changes = self::getSalutationChanges($changes);
                        $history .= $memberField . ': ' . $changes[0] . ' ➜ ' . $changes[1] . PHP_EOL;
                        break;
                    case 'preferred_communication':
                        $memberField = self::translateMemberField($memberField);
                        $changes = self::getPreferedCommunicationChanges($changes);
                        $history .= $memberField . ': ' . $changes[0] . ' ➜ ' . $changes[1] . PHP_EOL;
                        break;
                    case 'dateofbirth':
                        $memberField = self::translateMemberField($memberField);
                        //$changes = self::getPreferedCommunicationChanges($changes);
                        if ($changes[0] === 0) {
                            $changes[0] = '';
                        } else {
                            $changes[0] = date($dateFormat, $changes[0]);
                        }
                        if ($changes[1] === 0) {
                            $changes[1] = '';
                        } else {
                            $changes[1] = date($dateFormat, $changes[1]);
                        }
                        $history .= $memberField . ': ' . $changes[0] . ' ➜ ' . $changes[1] . PHP_EOL;
                        break;
                    case 'status':
                        $memberField = self::translateMemberField($memberField);
                        $changes = self::getStatusChanges($changes);
                        $history .= $memberField . ': ' . $changes[0] . ' ➜ ' . $changes[1] . PHP_EOL;
                        break;
                    case 'state':
                        $memberField = self::translateMemberField($memberField);
                        $changes[0] = self::getStateTitleByUid($changes[0]);
                        $changes[1] = self::getStateTitleByUid($changes[1]);
                        $history .= $memberField . ': ' . $changes[0] . ' ➜ ' . $changes[1] . PHP_EOL;
                        break;
                    case 'country':
                        $memberField = self::translateMemberField($memberField);
                        $changes[0] = self::getCountryTitleByUid($changes[0]);
                        $changes[1] = self::getCountryTitleByUid($changes[1]);
                        $history .= $memberField . ': ' . $changes[0] . ' ➜ ' . $changes[1] . PHP_EOL;
                        break;
                    case 'method_of_payment':
                        $memberField = self::translateMemberField($memberField);
                        $changes = self::getMethodOfPaymentChanges($changes);
                        $history .= $memberField . ': ' . $changes[0] . ' ➜ ' . $changes[1] . PHP_EOL;
                        break;
                    case 'payment_interval':
                        $memberField = self::translateMemberField($memberField);
                        $changes = self::getPaymentIntervalChanges($changes);
                        $history .= $memberField . ': ' . $changes[0] . ' ➜ ' . $changes[1] . PHP_EOL;
                        break;
                    case 'belongsto':
                        $memberField = self::translateMemberField($memberField);
                        $old = explode(',', $changes[0]);
                        $new = explode(',', $changes[1]);
                        $remove = array_diff($old, $new); // bei remove passt
                        $add = array_diff($new, $old); // bei add passt
                        $removed='';
                        $added='';
                        foreach ($remove as $key=> $uid) {
                            if ($uid > 0) {
                                if ($key === array_key_last($remove)) {
                                    $removed .= self::getMemberFullNameByUid($uid) . ' ';
                                } else {
                                    $removed .= self::getMemberFullNameByUid($uid) . ', ';
                                }
                            }
                        }
                        if ($removed !== '') {
                            $removed = $memberField . ': ' . $removed . ' ➜ ' . LocalizationUtility::translate('LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:labels.removed') . PHP_EOL;
                        }

                        foreach ($add as $key=> $uid) {
                            if ($uid > 0) {
                                if ($key === array_key_last($remove)) {
                                    $added .= self::getMemberFullNameByUid($uid) . ' ';
                                } else {
                                    $added .= self::getMemberFullNameByUid($uid) . ', ';
                                }
                            }
                        }
                        if ($added !== '') {
                            $added = $memberField . ': ' . $added . ' ➜ ' . LocalizationUtility::translate('LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:labels.added') . PHP_EOL;
                        }
                        //$added .= ' ➜ '.LocalizationUtility::translate('LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:labels.added') . PHP_EOL;
                        $history .= $added . $removed . PHP_EOL;
                        break;
                    case 'sections':
                        $memberField = self::translateMemberField($memberField);
                        $old = explode(',', $changes[0]);
                        $new = explode(',', $changes[1]);
                        $remove = array_diff($old, $new); // bei remove passt
                        $add = array_diff($new, $old); // bei add passt
                        $removed='';
                        $added='';
                        foreach ($remove as $key=> $uid) {
                            if ($uid > 0) {
                                if ($key === array_key_last($remove)) {
                                    $removed .= self::getSectionTitleByUid($uid) . ' ';
                                } else {
                                    $removed .= self::getSectionTitleByUid($uid) . ', ';
                                }
                            }
                        }
                        if ($removed !== '') {
                            $removed = $memberField . ': ' . $removed . ' ➜ ' . LocalizationUtility::translate('LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:labels.removed') . PHP_EOL;
                        }
                        foreach ($add as $key=> $uid) {
                            if ($uid > 0) {
                                if ($key === array_key_last($remove)) {
                                    $added .= self::getSectionTitleByUid($uid) . ' ';
                                } else {
                                    $added .= self::getSectionTitleByUid($uid) . ', ';
                                }
                            }
                        }
                        if ($added !== '') {
                            $added = $memberField . ': ' . $added . ' ➜ ' . LocalizationUtility::translate('LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:labels.added') . PHP_EOL;
                        }
                        $history .= $added . $removed . PHP_EOL;
                        break;
                    case 'functions':
                        $memberField = self::translateMemberField($memberField);
                        $old = explode(',', $changes[0]);
                        $new = explode(',', $changes[1]);
                        $remove = array_diff($old, $new); // bei remove passt
                        $add = array_diff($new, $old); // bei add passt
                        $removed='';
                        $added='';
                        foreach ($remove as $key=> $uid) {
                            if ($uid > 0) {
                                if ($key === array_key_last($remove)) {
                                    $removed .= self::getFunctionTitleByUid($uid) . ' ';
                                } else {
                                    $removed .= self::getFunctionTitleByUid($uid) . ', ';
                                }
                            }
                        }
                        if ($removed !== '') {
                            $removed = $memberField . ': ' . $removed . ' ➜ ' . LocalizationUtility::translate('LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:labels.removed') . PHP_EOL;
                        }
                        foreach ($add as $key=> $uid) {
                            if ($uid > 0) {
                                if ($key === array_key_last($remove)) {
                                    $added .= self::getFunctionTitleByUid($uid) . ' ';
                                } else {
                                    $added .= self::getFunctionTitleByUid($uid) . ', ';
                                }
                            }
                        }
                        if ($added !== '') {
                            $added = $memberField . ': ' . $added . ' ➜ ' . LocalizationUtility::translate('LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:labels.added') . PHP_EOL;
                        }
                        $history .= $added . $removed . PHP_EOL;
                        break;
                    case 'positions':
                        $memberField = self::translateMemberField($memberField);
                        $old = explode(',', $changes[0]);
                        $new = explode(',', $changes[1]);
                        $remove = array_diff($old, $new); // bei remove passt
                        $add = array_diff($new, $old); // bei add passt
                        $removed='';
                        $added='';
                        foreach ($remove as $key=> $uid) {
                            if ($uid > 0) {
                                if ($key === array_key_last($remove)) {
                                    $removed .= self::getPositionTitleByUid($uid) . ' ';
                                } else {
                                    $removed .= self::getPositionTitleByUid($uid) . ', ';
                                }
                            }
                        }
                        if ($removed !== '') {
                            $removed = $memberField . ': ' . $removed . ' ➜ ' . LocalizationUtility::translate('LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:labels.removed') . PHP_EOL;
                        }
                        foreach ($add as $key=> $uid) {
                            if ($uid > 0) {
                                if ($key === array_key_last($remove)) {
                                    $added .= self::getPositionTitleByUid($uid) . ' ';
                                } else {
                                    $added .= self::getPositionTitleByUid($uid) . ', ';
                                }
                            }
                        }
                        if ($added !== '') {
                            $added = $memberField . ': ' . $added . ' ➜ ' . LocalizationUtility::translate('LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:labels.added') . PHP_EOL;
                        }
                        $history .= $added . $removed . PHP_EOL;
                        break;
                    case 'role':
                        $memberField = self::translateMemberField($memberField);
                        $old = explode(',', $changes[0]);
                        $new = explode(',', $changes[1]);
                        $remove = array_diff($old, $new); // bei remove passt
                        $add = array_diff($new, $old); // bei add passt
                        $removed='';
                        $added='';
                        foreach ($remove as $key=> $uid) {
                            if ($uid > 0) {
                                if ($key === array_key_last($remove)) {
                                    $removed .= self::getRoleTitleByUid($uid) . ' ';
                                } else {
                                    $removed .= self::getRoleTitleByUid($uid) . ', ';
                                }
                            }
                        }
                        if ($removed !== '') {
                            $removed = $memberField . ': ' . $removed . ' ➜ ' . LocalizationUtility::translate('LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:labels.removed') . PHP_EOL;
                        }
                        foreach ($add as $key=> $uid) {
                            if ($uid > 0) {
                                if ($key === array_key_last($remove)) {
                                    $added .= self::getRoleTitleByUid($uid) . ' ';
                                } else {
                                    $added .= self::getRoleTitleByUid($uid) . ', ';
                                }
                            }
                        }
                        if ($added !== '') {
                            $added = $memberField . ': ' . $added . ' ➜ ' . LocalizationUtility::translate('LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:labels.added') . PHP_EOL;
                        }
                        $history .= $added . $removed . PHP_EOL;
                        break;
                    case 'member_fee':
                        $memberField = self::translateMemberField($memberField);
                        $old = explode(',', $changes[0]);
                        $new = explode(',', $changes[1]);
                        $remove = array_diff($old, $new); // bei remove passt
                        $add = array_diff($new, $old); // bei add passt
                        $removed='';
                        $added='';
                        foreach ($remove as $key=> $uid) {
                            if ($uid > 0) {
                                if ($key === array_key_last($remove)) {
                                    $removed .= self::getMemberFeeTitleByUid($uid) . ' ';
                                } else {
                                    $removed .= self::getMemberFeeTitleByUid($uid) . ', ';
                                }
                            }
                        }
                        if ($removed !== '') {
                            $removed = $memberField . ': ' . $removed . ' ➜ ' . LocalizationUtility::translate('LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:labels.removed') . PHP_EOL;
                        }
                        foreach ($add as $key=> $uid) {
                            if ($uid > 0) {
                                if ($key === array_key_last($remove)) {
                                    $added .= self::getMemberFeeTitleByUid($uid) . ' ';
                                } else {
                                    $added .= self::getMemberFeeTitleByUid($uid) . ', ';
                                }
                            }
                        }
                        if ($added !== '') {
                            $added = $memberField . ': ' . $added . ' ➜ ' . LocalizationUtility::translate('LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:labels.added') . PHP_EOL;
                        }
                        $history .= $added . $removed . PHP_EOL;
                        break;
                    case 'categories':
                        $memberField = self::translateMemberField($memberField);
                        $old = explode(',', $changes[0]);
                        $new = explode(',', $changes[1]);
                        $remove = array_diff($old, $new); // bei remove passt
                        $add = array_diff($new, $old); // bei add passt
                        $removed='';
                        $added='';
                        foreach ($remove as $key=> $uid) {
                            if ($uid > 0) {
                                if ($key === array_key_last($remove)) {
                                    $removed .= self::getCategorieTitleByUid($uid) . ' ';
                                } else {
                                    $removed .= self::getCategorieTitleByUid($uid) . ', ';
                                }
                            }
                        }
                        if ($removed !== '') {
                            $removed = $memberField . ': ' . $removed . ' ➜ ' . LocalizationUtility::translate('LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:labels.removed') . PHP_EOL;
                        }
                        foreach ($add as $key=> $uid) {
                            if ($uid > 0) {
                                if ($key === array_key_last($remove)) {
                                    $added .= self::getCategorieTitleByUid($uid) . ' ';
                                } else {
                                    $added .= self::getCategorieTitleByUid($uid) . ', ';
                                }
                            }
                        }
                        if ($added !== '') {
                            $added = $memberField . ': ' . $added . ' ➜ ' . LocalizationUtility::translate('LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:labels.added') . PHP_EOL;
                        }
                        $history .= $added . $removed . PHP_EOL;
                        break;
                    case 'profile_image':
                    case 'sepa_mandate':
                    case 'declaration_of_consent':
                    case 'leaving_statement':
                    case 'membership_application':
                        $memberField = self::translateMemberField($memberField);
                        if ($changes[1] === 0) {
                            $history .= $memberField . ': ' . LocalizationUtility::translate('LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:labels.removed') . PHP_EOL;
                        } else {
                            $history .= $memberField . ': ' . LocalizationUtility::translate('LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:labels.modified') . PHP_EOL;
                        }
                        break;
                    case 'fe_user':
                        break;
                    default:
                        if ($changes[1] === '') {
                            $changes[1] = LocalizationUtility::translate('LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:labels.removed');
                        }
                        $history .= self::translateMemberField($memberField) . ': ' . $changes[0] . ' ➜ ' . $changes[1] . PHP_EOL;
                }
            }
        }
        if ($history) {
            $connectionPool = GeneralUtility::makeInstance(ConnectionPool::class);
            $queryBuilder = $connectionPool->getQueryBuilderForTable('tx_smartverein_domain_model_changes');

            $changes = $queryBuilder
                ->insert('tx_smartverein_domain_model_changes')
                ->values([
                    'changed_by' => SmartvereinUtility::getBeUsername(),
                    'date' => time(),
                    'members' => $member,
                    'note' => $history,
                    'pid' => SmartvereinUtility::getExtConfProperty('smartverein', 'settings.memberStoragePid'),
                ])
                ->execute();

            $lastChanges = $queryBuilder->getConnection()->lastInsertId('tx_smartverein_domain_model_changes');
            $queryBuilder = $connectionPool->getQueryBuilderForTable('tx_smartverein_domain_model_members');

            $memberChanges = $queryBuilder
                ->update('tx_smartverein_domain_model_members')
                ->where(
                    $queryBuilder->expr()->eq('uid', $member)
                )
                ->set('changelog', $lastChanges)
                ->execute();
        }
    }

    /**
     * @throws \TYPO3\CMS\Core\Configuration\Exception\ExtensionConfigurationPathDoesNotExistException
     * @throws \TYPO3\CMS\Core\Configuration\Exception\ExtensionConfigurationExtensionNotConfiguredException
     * @throws \Doctrine\DBAL\DBALException
     */
    public static function newMemberChangelogEntry(string $member, array $entries, array $datamap): void
    {
        $history = LocalizationUtility::translate('LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:labels.createMember');
        $connectionPool = GeneralUtility::makeInstance(ConnectionPool::class);
        $queryBuilder = $connectionPool->getQueryBuilderForTable('tx_smartverein_domain_model_changes');

        $changes = $queryBuilder
            ->insert('tx_smartverein_domain_model_changes')
            ->values([
                'changed_by' => SmartvereinUtility::getBeUsername(),
                'date' => time(),
                'members' => $member,
                'note' => $history,
                'pid' => SmartvereinUtility::getExtConfProperty('smartverein', 'settings.memberStoragePid'),
            ])
            ->execute();
    }

    private static function translateMemberField($memberField): ?string
    {
        $memberField = LocalizationUtility::translate('LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_members.' . $memberField, 'smartverein');
        return $memberField;
    }

    private static function getSalutationChanges($changes): ?array
    {
        $changes[0] = LocalizationUtility::translate('LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:salutation.item.' . $changes[0], 'smartverein');
        $changes[1] = LocalizationUtility::translate('LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:salutation.item.' . $changes[1], 'smartverein');
        return $changes;
    }

    private static function getPreferedCommunicationChanges($changes): ?array
    {
        $changes[0] = LocalizationUtility::translate('LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:preferred_communication.item.' . $changes[0], 'smartverein');
        $changes[1] = LocalizationUtility::translate('LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:preferred_communication.item.' . $changes[1], 'smartverein');
        return $changes;
    }

    private static function getStatusChanges($changes): ?array
    {
        $changes[0] = LocalizationUtility::translate('LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:status.item.' . $changes[0], 'smartverein');
        $changes[1] = LocalizationUtility::translate('LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:status.item.' . $changes[1], 'smartverein');
        return $changes;
    }

    private static function getMethodOfPaymentChanges($changes): ?array
    {
        $changes[0] = LocalizationUtility::translate('LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:method_of_payment.item.' . $changes[0], 'smartverein');
        $changes[1] = LocalizationUtility::translate('LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:method_of_payment.item.' . $changes[1], 'smartverein');
        return $changes;
    }

    private static function getPaymentIntervalChanges($changes): ?array
    {
        $changes[0] = LocalizationUtility::translate('LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:payment_interval.item.' . $changes[0], 'smartverein');
        $changes[1] = LocalizationUtility::translate('LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:payment_interval.item.' . $changes[1], 'smartverein');
        return $changes;
    }

    private static function getMemberFullNameByUid($uid): ?string
    {
        $connectionPool = GeneralUtility::makeInstance(ConnectionPool::class);
        $queryBuilder = $connectionPool->getQueryBuilderForTable('tx_smartverein_domain_model_members');
        $result = $queryBuilder
            ->select('firstname', 'lastname')
            ->from('tx_smartverein_domain_model_members')
            ->where(
                $queryBuilder->expr()->eq('uid', $uid)
            )
            ->executeQuery();
        $row = $result->fetchAssociative();

        return $row['firstname'] . ' ' . $row['lastname'];
    }

    private static function getSectionTitleByUid($uid): ?string
    {
        $connectionPool = GeneralUtility::makeInstance(ConnectionPool::class);
        $queryBuilder = $connectionPool->getQueryBuilderForTable('tx_smartverein_domain_model_sections');
        $result = $queryBuilder
            ->select('title')
            ->from('tx_smartverein_domain_model_sections')
            ->where(
                $queryBuilder->expr()->eq('uid', $uid)
            )
            ->executeQuery();
        $row = $result->fetchAssociative();

        //die();

        return $row['title'];
    }

    private static function getFunctionTitleByUid($uid): ?string
    {
        $connectionPool = GeneralUtility::makeInstance(ConnectionPool::class);
        $queryBuilder = $connectionPool->getQueryBuilderForTable('tx_smartverein_domain_model_functions');
        $result = $queryBuilder
            ->select('title')
            ->from('tx_smartverein_domain_model_functions')
            ->where(
                $queryBuilder->expr()->eq('uid', $uid)
            )
            ->executeQuery();
        $row = $result->fetchAssociative();

        return $row['title'];
    }

    private static function getPositionTitleByUid($uid): ?string
    {
        $connectionPool = GeneralUtility::makeInstance(ConnectionPool::class);
        $queryBuilder = $connectionPool->getQueryBuilderForTable('tx_smartverein_domain_model_positions');
        $result = $queryBuilder
            ->select('title')
            ->from('tx_smartverein_domain_model_positions')
            ->where(
                $queryBuilder->expr()->eq('uid', $uid)
            )
            ->executeQuery();
        $row = $result->fetchAssociative();

        return $row['title'];
    }

    private static function getRoleTitleByUid($uid): ?string
    {
        $connectionPool = GeneralUtility::makeInstance(ConnectionPool::class);
        $queryBuilder = $connectionPool->getQueryBuilderForTable('tx_smartverein_domain_model_roles');
        $result = $queryBuilder
            ->select('title')
            ->from('tx_smartverein_domain_model_roles')
            ->where(
                $queryBuilder->expr()->eq('uid', $uid)
            )
            ->executeQuery();
        $row = $result->fetchAssociative();

        return $row['title'];
    }

    private static function getMemberFeeTitleByUid($uid): ?string
    {
        $connectionPool = GeneralUtility::makeInstance(ConnectionPool::class);
        $queryBuilder = $connectionPool->getQueryBuilderForTable('tx_smartverein_domain_model_memberfees');
        $result = $queryBuilder
            ->select('title')
            ->from('tx_smartverein_domain_model_memberfees')
            ->where(
                $queryBuilder->expr()->eq('uid', $uid)
            )
            ->executeQuery();
        $row = $result->fetchAssociative();

        return $row['title'];
    }

    private static function getCategorieTitleByUid($uid): ?string
    {
        $connectionPool = GeneralUtility::makeInstance(ConnectionPool::class);
        $queryBuilder = $connectionPool->getQueryBuilderForTable('sys_category');
        $result = $queryBuilder
            ->select('title')
            ->from('sys_category')
            ->where(
                $queryBuilder->expr()->eq('uid', $uid)
            )
            ->executeQuery();
        $row = $result->fetchAssociative();

        return $row['title'];
    }

    /**
     * @throws \Doctrine\DBAL\DBALException
     * @throws \Doctrine\DBAL\Driver\Exception
     */
    private static function getCountryTitleByUid($uid): ?string
    {
        if ($uid === '0') {
            return '';
        }
        if ($uid === 0) {
            return LocalizationUtility::translate('LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:labels.removed');
        }
        $connectionPool = GeneralUtility::makeInstance(ConnectionPool::class);
        $queryBuilder = $connectionPool->getQueryBuilderForTable('static_countries');
        $result = $queryBuilder
            ->select('cn_short_local')
            ->from('static_countries')
            ->where(
                $queryBuilder->expr()->eq('uid', $uid)
            )
            ->executeQuery();
        $row = $result->fetchAssociative();
        return $row['cn_short_' . SmartvereinUtility::getExtConfProperty('smartverein', 'settings.siteLanguage')];
    }
    private static function getStateTitleByUid($uid): ?string
    {
        if ($uid === '0') {
            return '';
        }
        if ($uid === 0) {
            return LocalizationUtility::translate('LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:labels.removed');
        }
        $connectionPool = GeneralUtility::makeInstance(ConnectionPool::class);
        $queryBuilder = $connectionPool->getQueryBuilderForTable('static_country_zones');
        $result = $queryBuilder
            ->select('zn_name_local')
            ->from('static_country_zones')
            ->where(
                $queryBuilder->expr()->eq('uid', $uid)
            )
            ->executeQuery();
        $row = $result->fetchAssociative();

        return $row['zn_name_' . SmartvereinUtility::getExtConfProperty('smartverein', 'settings.siteLanguage')];
    }
}
