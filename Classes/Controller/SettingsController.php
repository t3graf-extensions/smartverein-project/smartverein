<?php

declare(strict_types=1);

/*
 * This file is part of the "SmartVerein - TYPO3 Club Management" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2023 Development-Team <development@t3graf-media.de>, T3graf media-agentur UG
 */

namespace T3graf\Smartverein\Controller;

use TYPO3\CMS\Core\Configuration\ExtensionConfiguration;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\PathUtility;

/**
 * This file is part of the "smartClub - club-administration" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2023 Development-Team <development@t3graf-media.de>, T3graf media-agentur UG
 */

/**
 * SettingsController
 */
class SettingsController extends AbstractController
{
    /**
     * action index
     *
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function indexAction(): \Psr\Http\Message\ResponseInterface
    {
        $this->view->assign('pageTitle', 'Club settings');
        $this->view->assign('club', $this->club);
        $this->view->assign('sections', $this->getSections());
        $this->view->assign('functions', $this->getFunctions());
        $this->view->assign('positions', $this->getPositions());
        $this->view->assign('roles', $this->getRoles());
        $this->view->assign('memberFees', $this->getMemberFees());
        $this->view->assign('storagePid', GeneralUtility::makeInstance(ExtensionConfiguration::class)->get('smartverein', 'settings/memberStoragePid'));

        $this->pageRenderer->addCssFile('EXT:smartverein/Resources/Public/Css/settings.css', 'stylesheet', 'all', '', true);
        $this->pageRenderer->addCssFile('EXT:smartverein/Resources/Public/Css/DataTables/Styles.css', 'stylesheet', 'all', '', true);
        $this->pageRenderer->addCssFile('EXT:smartverein/Resources/Public/Css/DataTables/ownStyle.css', 'stylesheet', 'all', '', true);
        $this->pageRenderer->addCssFile('EXT:smartverein/Resources/Public/Css/FontAwesome/all.min.css', 'stylesheet', 'all', '', true);
        $this->pageRenderer->loadRequireJsModule(
            'TYPO3/CMS/Smartverein/TabStorageModule',
            'function() { console.log("Loaded own module."); }'
        );
        $this->pageRenderer->addRequireJsConfiguration(
            [
                'paths' => [
                    'simpledatatables' => PathUtility::getPublicResourceWebPath(
                        'EXT:smartverein/Resources/Public/JavaScript/Libs/simple-datatables-min'
                    ),
                ],
                'shim' => [
                    'simpledatatables' => ['exports' => 'simpledatatables'],
                ],
            ]
        );
        return $this->htmlResponse();
    }
    /**
     * @throws \Doctrine\DBAL\Driver\Exception
     * @throws \Doctrine\DBAL\DBALException
     */
    private function getSections(): array
    {
        $queryBuilder = $this->connectionPool->getQueryBuilderForTable('tx_smartverein_domain_model_sections');

        return $queryBuilder->select('*')
            ->from('tx_smartverein_domain_model_sections')
            ->executeQuery()
            ->fetchAllAssociative();
    }

    /**
     * @throws \Doctrine\DBAL\Driver\Exception
     * @throws \Doctrine\DBAL\DBALException
     */
    private function getFunctions(): array
    {
        $queryBuilder = $this->connectionPool->getQueryBuilderForTable('tx_smartverein_domain_model_functions');

        return $queryBuilder->select('*')
            ->from('tx_smartverein_domain_model_functions')
            ->executeQuery()
            ->fetchAllAssociative();
    }

    /**
     * @throws \Doctrine\DBAL\Driver\Exception
     * @throws \Doctrine\DBAL\DBALException
     */
    private function getPositions(): array
    {
        $queryBuilder = $this->connectionPool->getQueryBuilderForTable('tx_smartverein_domain_model_positions');

        return $queryBuilder->select('*')
            ->from('tx_smartverein_domain_model_positions')
            ->executeQuery()
            ->fetchAllAssociative();
    }

    /**
     * @throws \Doctrine\DBAL\Driver\Exception
     * @throws \Doctrine\DBAL\DBALException
     */
    private function getRoles(): array
    {
        $queryBuilder = $this->connectionPool->getQueryBuilderForTable('tx_smartverein_domain_model_roles');

        return $queryBuilder->select('*')
            ->from('tx_smartverein_domain_model_roles')
            ->executeQuery()
            ->fetchAllAssociative();
    }

    /**
     * @throws \Doctrine\DBAL\Driver\Exception
     * @throws \Doctrine\DBAL\DBALException
     */
    private function getMemberFees(): array
    {
        $queryBuilder = $this->connectionPool->getQueryBuilderForTable('tx_smartverein_domain_model_memberfees');

        return $queryBuilder->select('*')
            ->from('tx_smartverein_domain_model_memberfees')
            ->executeQuery()
            ->fetchAllAssociative();
    }
}
