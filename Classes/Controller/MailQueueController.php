<?php

declare(strict_types=1);

/*
 * This file is part of the "SmartVerein - TYPO3 Club Management" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2023 Development-Team <development@t3graf-media.de>, T3graf media-agentur UG
 */

namespace T3graf\Smartverein\Controller;

/**
 * This file is part of the "smartClub - club-administration" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2023 Development-Team <development@t3graf-media.de>, T3graf media-agentur UG
 */

/**
 * MailQueueController
 */
class MailQueueController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{
    /**
     * mailQueueRepository
     *
     * @var \T3graf\Smartverein\Domain\Repository\MailQueueRepository
     */
    protected $mailQueueRepository = null;

    /**
     * @param \T3graf\Smartverein\Domain\Repository\MailQueueRepository $mailQueueRepository
     */
    public function injectMailQueueRepository(\T3graf\Smartverein\Domain\Repository\MailQueueRepository $mailQueueRepository)
    {
        $this->mailQueueRepository = $mailQueueRepository;
    }

    /**
     * action list
     *
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function listAction(): \Psr\Http\Message\ResponseInterface
    {
        $mailQueues = $this->mailQueueRepository->findAll();
        $this->view->assign('mailQueues', $mailQueues);
        return $this->htmlResponse();
    }
}
