<?php

declare(strict_types=1);

/*
 * This file is part of the "SmartVerein - TYPO3 Club Management" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2023 Development-Team <development@t3graf-media.de>, T3graf media-agentur UG
 */

namespace T3graf\Smartverein\Controller;

/**
 * This file is part of the "smartClub - club-administration" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2023 Development-Team <development@t3graf-media.de>, T3graf media-agentur UG
 */

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use T3graf\Smartverein\Domain\Model\Club;
use T3graf\Smartverein\Utility\MemberUtility;
use T3graf\Smartverein\Utility\SmartvereinUtility;
use TYPO3\CMS\Backend\Routing\UriBuilder;
use TYPO3\CMS\Core\Configuration\ExtensionConfiguration;
use TYPO3\CMS\Core\Http\PropagateResponseException;
use TYPO3\CMS\Core\Http\RedirectResponse;
use TYPO3\CMS\Core\Messaging\FlashMessage;
use TYPO3\CMS\Core\Messaging\FlashMessageService;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\PathUtility;
use TYPO3\CMS\Extbase\Utility\LocalizationUtility;

/**
 * MembersController
 */
class MembersController extends AbstractController
{
    /**
     * action beListMembers
     *
     * @return ResponseInterface
     * @throws \TYPO3\CMS\Core\Resource\Exception\InvalidFileException
     */
    public function beListMembersAction(): ResponseInterface
    {
        $members = $this->membersRepository->findAll();

        $this->view->assign('pageTitle', 'Member management');
        $this->view->assign('club', $this->club);
        $this->view->assign('members', $members);
        $this->view->assign('storagePid', GeneralUtility::makeInstance(ExtensionConfiguration::class)
            ->get('smartverein', 'settings/memberStoragePid'));

        $this->pageRenderer->addCssFile('EXT:smartverein/Resources/Public/Css/DataTables/Styles.css', 'stylesheet', 'all', '', true);
        $this->pageRenderer->addCssFile('EXT:smartverein/Resources/Public/Css/DataTables/ownStyle.css', 'stylesheet', 'all', '', true);
        $this->pageRenderer->addCssFile('EXT:smartverein/Resources/Public/Css/FontAwesome/all.min.css', 'stylesheet', 'all', '', true);
        $this->pageRenderer->addRequireJsConfiguration(
            [
                'paths' => [
                    'simpledatatables' => PathUtility::getPublicResourceWebPath(
                        'EXT:smartverein/Resources/Public/JavaScript/Libs/simple-datatables-min'
                    ),
                ],
                'shim' => [
                    'simpledatatables' => ['exports' => 'simpledatatables'],
                ],
            ]
        );
        return $this->htmlResponse();
    }

    /**
     * action beShowMemberDetails
     *
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function beShowMemberDetailsAction(): \Psr\Http\Message\ResponseInterface
    {
        $members = $this->membersRepository->findAll();
        $this->view->assign('pageTitle', 'Member management');
        $this->view->assign('club', $this->club);
        return $this->htmlResponse();
    }
    /**
     * action beEditMember
     *
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function beEditMemberAction(): \Psr\Http\Message\ResponseInterface
    {
        $members = $this->membersRepository->findAll();
        $this->view->assign('pageTitle', 'Member Edit');
        $this->view->assign('club', $this->club);
        return $this->htmlResponse();
    }

    /**
     * action beUpdateMember
     *
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function beUpdateMemberAction(): \Psr\Http\Message\ResponseInterface
    {
        $members = $this->membersRepository->findAll();
        $this->view->assign('pageTitle', 'Member Edit');
        $this->view->assign('club', $this->club);
        return $this->htmlResponse();
    }

    /**
     * action beDeleteMemberv
     *
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function beDeleteMemberAction(): \Psr\Http\Message\ResponseInterface
    {
        $members = $this->membersRepository->findAll();
        $this->view->assign('pageTitle', 'Member delete');
        $this->view->assign('club', $this->club);
        return $this->htmlResponse();
    }

    /**
     * action beCreateMissingMemberIds
     *
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function beCreateMissingMemberIdsAction(): \Psr\Http\Message\ResponseInterface
    {
        $members = $this->membersRepository->findAll();
        foreach ($members as $member) {
            if (empty($member->getMemberId)) {
                $number = $this->createUniqueMemberId();
            }
        }
        return $this->htmlResponse();
    }

    /**
     * action beDownloadVCardsAction
     *
     * @return ResponseInterface
     * @throws PropagateResponseException
     * @throws \Doctrine\DBAL\DBALException
     * @throws \Doctrine\DBAL\Driver\Exception
     */
    public function beDownloadVCardsAction(): \Psr\Http\Message\ResponseInterface
    {
        $filename = 'Members-list-vCards_' . date('Y-m-d') . '.vcf';
        $fileContent = '';

        $members = $this->membersRepository->findAll();
        foreach ($members as $member) {
            $birthday = '';
            if ($member->getDateofbirth() !== null) {
                $date = $member->getDateofbirth();
                $birthday = date('Ymd', $date->getTimestamp());
            }

            $fileContent .= '
BEGIN:VCARD
VERSION:4.0
FN:' . $member->getFirstname() . ' ' . $member->getLastname() . '
N:' . $member->getLastname() . ';' . $member->getFirstname() . ';;;' . $member->getTitle() . '
TEL;HOME;VOICE:' . $member->getPhone() . '
TEL;CELL:' . $member->getMobile() . '
TEL;HOME;FAX:' . $member->getFax() . '
ADR;DOM;PARCEL;HOME:;;' . $member->getStreet() . ';' . $member->getCity() . ';' . MemberUtility::getMemberState($member->getState()) . ';' . $member->getZip() . ';' . MemberUtility::getMemberCountry($member->getCountry()) . '
BDAY:' . $birthday . '
EMAIL;TYPE=HOME:' . $member->getEmail() . '
END:VCARD
';
        }

        $response = $this->responseFactory->createResponse()
            ->withHeader('Cache-Control', 'private')
            ->withHeader('Content-Disposition', sprintf('attachment; filename="%s"', $filename))
            //->withHeader('Content-Length', (string)filesize($filePath))
            ->withHeader('Content-Type', 'text/vcard')
            ->withBody($this->streamFactory->createStream($fileContent));

        throw new PropagateResponseException($response, 200);
    }
    /**
     * action beDownloadCsvAction
     *
     * @return ResponseInterface
     * @throws PropagateResponseException
     * @throws \Doctrine\DBAL\DBALException
     * @throws \Doctrine\DBAL\Driver\Exception
     */
    public function beDownloadCsvAction(): \Psr\Http\Message\ResponseInterface
    {
        $memberFieldsToExport = [
        'member_type',
        'gender',
        'title',
        'firstname',
        'lastname',
        'dateofbirth',
        'status',
        'member_id',
        'entry_date',
        'leaving_date',
        'sections',
        'functions',
        'positions',
        'role',
        'street',
        'addition_to_address',
        'zip',
        'city',
        'state',
        'country',
        'organisation',
        'contact_person',
        'position',
        'department',
        'salutation',
        'preferred_communication',
        'email',
        'phone',
        'mobile',
        'fax',
        'website',
        'belongsto',
        'method_of_payment',
        'member_fee',
        'payment_interval',
        'account_owner',
        'iban',
        'bic',
        'mandate_reference',
        'mandate_date'
    ];
        //debug($memberFieldsToExport);die();
        $filename = 'Members-list-CSV_' . date('Y-m-d') . '.csv';
        $fileContent = '';

        $members = $this->membersRepository->findAll();
        //debug($members);die();

        foreach ($memberFieldsToExport as $key => $memberField) {
            if ($key === array_key_last($memberFieldsToExport)) {
                $fileContent .= LocalizationUtility::translate('LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_members.' . $memberField, 'smartverein');
            } else {
                $fileContent .= LocalizationUtility::translate('LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_domain_model_members.' . $memberField, 'smartverein') . ';';
            }
        }
        //debug ($fileContent);die();
        //$fileContent = 'Mitgliedsnummer,Vorname,Nachname';
        $fileContent .= (new \T3graf\Smartverein\Utility\CsvUtility)->generateCsvFileContent($members);
        $fileContent = preg_replace("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", "\n", $fileContent);

        $response = $this->responseFactory->createResponse()
            ->withHeader('Cache-Control', 'private')
            ->withHeader('Content-Disposition', sprintf('attachment; filename="%s"', $filename))
            //->withHeader('Content-Length', (string)filesize($filePath))
            ->withHeader('Content-Type', 'text/csv')
            ->withBody($this->streamFactory->createStream($fileContent));

        throw new PropagateResponseException($response, 200);
    }

    /**
     * create memberID
     *
     * @return string
     */
    public function createUniqueMemberId(): string
    {
        do {
            $memberID = SmartvereinUtility::createMemberID();
            $query = $this->membersRepository->findByMemberId($memberID);
        } while (empty($query));

        return $memberID;
    }

    public function update(ServerRequestInterface $request): ResponseInterface
    {
        $filename = 'test.pdf';
        $file = 'sfTESTdj sfsjfsjfsfs fsjfskj';
        /*
        //$members = $this->membersRepository->findAll();
        foreach ($members as $member) {
            if (empty($member->getMemberId)) {
                $number = $this->createUniqueMemberId();
            }
        }
        */
        $response = $this->responseFactory->createResponse()
            ->withHeader('Cache-Control', 'private')
            ->withHeader('Content-Disposition', sprintf('attachment; filename="%s"', $filename))
            //->withHeader('Content-Length', (string)filesize($filePath))
            ->withHeader('Content-Type', 'application/pdf')
            ->withBody($this->streamFactory->createStream($file));

        throw new PropagateResponseException($response, 200);
        //get book uid
        $memberUid = intval($request->getQueryParams()['member']);

        //do whatever you want
        $updated = true;

        //create redirect url to edit view of book dataset
        $backendUriBuilder = GeneralUtility::makeInstance(UriBuilder::class);
        $uriParameters = ['edit' => ['tx_smartverein_domain_model_members' => [$memberUid => 'edit']]];
        $editMemberLink = $backendUriBuilder->buildUriFromRoute(
            'record_edit',
            $uriParameters
        );
        //add error message
        $message = GeneralUtility::makeInstance(
            FlashMessage::class,
            'Artikel konnte nicht artualisiert werden, überprüfen Sie die Angaben und versuchen Sie es erneut!',
            'Moware Update',
            FlashMessage::ERROR,
            true
        );
        //add success message
        if ($updated) {
            $message = GeneralUtility::makeInstance(
                FlashMessage::class,
                'Artikel artualisiert',
                'Moware Update',
                FlashMessage::OK,
                true
            );
        }
        //add message to flash message queue
        $flashMessageService = GeneralUtility::makeInstance(FlashMessageService::class);
        $messageQueue = $flashMessageService->getMessageQueueByIdentifier();
        $messageQueue->addMessage($message);
        //return redirect response
        return new RedirectResponse($editMemberLink);
    }
}
