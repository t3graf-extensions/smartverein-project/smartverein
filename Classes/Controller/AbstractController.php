<?php

declare(strict_types=1);

/*
 * This file is part of the "SmartVerein - TYPO3 Club Management" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2023 Development-Team <development@t3graf-media.de>, T3graf media-agentur UG
 */

namespace T3graf\Smartverein\Controller;

use T3graf\Smartverein\Domain\Model\Club;
use T3graf\Smartverein\Domain\Repository\AddressbookRepository;
use T3graf\Smartverein\Domain\Repository\MembersRepository;
use T3graf\Smartverein\Utility\SmartvereinUtility;
use TYPO3\CMS\Backend\Template\Components\ButtonBar;
use TYPO3\CMS\Backend\Template\ModuleTemplate;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Imaging\IconFactory;
use TYPO3\CMS\Core\Page\PageRenderer;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;

/*
 * A collection of various helper methods to keep
 * our Action Controllers small and clean
 */
class AbstractController extends ActionController
{
    /**
     * @var MembersRepository
     */
    protected $membersRepository = null;

    /**
     * @var AddressbookRepository
     */
    protected $addressbookRepository = null;

    /**
     * @var ModuleTemplate
     */
    protected $moduleTemplate;

    /**
     * @var IconFactory
     */
    protected $iconFactory;

    /**
     * @var ButtonBar
     */
    protected $buttonBar;

    /**
     * @var Club
     */
    public Club $club;

    /**
     * @var mixed|\Psr\Log\LoggerAwareInterface|PageRenderer|\TYPO3\CMS\Core\SingletonInterface
     */
    protected mixed $pageRenderer;

    /**
     * @var mixed|\Psr\Log\LoggerAwareInterface|ConnectionPool|\TYPO3\CMS\Core\SingletonInterface
     */
    protected mixed $queryBuilder;

    /**
     * @var mixed|\Psr\Log\LoggerAwareInterface|ConnectionPool|\TYPO3\CMS\Core\SingletonInterface
     */
    protected mixed $connectionPool;

    /**
     * @param MembersRepository $membersRepository
     */
    public function injectMembersRepository(MembersRepository $membersRepository)
    {
        $this->membersRepository = $membersRepository;
    }

    /**
     * @param AddressbookRepository $addressbookRepository
     */
    public function injectAddressbookRepository(AddressbookRepository $addressbookRepository)
    {
        $this->addressbookRepository = $addressbookRepository;
    }

    /**
     * @throws \TYPO3\CMS\Extbase\Configuration\Exception\InvalidConfigurationTypeException
     */
    public function initializeAction(): void
    {
        $this->club = new Club();
        $this->settings = SmartvereinUtility::getTSSettings('smartverein');
        $this->pageRenderer = GeneralUtility::makeInstance(PageRenderer::class);
        $this->connectionPool = GeneralUtility::makeInstance(ConnectionPool::class);
    }
}
