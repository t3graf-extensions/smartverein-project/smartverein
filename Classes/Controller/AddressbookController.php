<?php

declare(strict_types=1);

/*
 * This file is part of the "SmartVerein - TYPO3 Club Management" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2023 Development-Team <development@t3graf-media.de>, T3graf media-agentur UG
 */

namespace T3graf\Smartverein\Controller;

use Psr\Http\Message\ResponseInterface;
use TYPO3\CMS\Core\Configuration\ExtensionConfiguration;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\PathUtility;

/**
 * This file is part of the "smartClub - club-administration" Extension for TYPO3 CMS.
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 * (c) 2023 Development-Team <development@t3graf-media.de>, T3graf media-agentur UG
 */

/**
 * AddressbookController
 */
class AddressbookController extends AbstractController
{
    /**
     * action beListAddresses
     *
     * @return ResponseInterface
     * @throws \TYPO3\CMS\Core\Resource\Exception\InvalidFileException
     */
    public function beListAddressesAction(): ResponseInterface
    {
        $addresses = $this->addressbookRepository->findAll();

        $this->view->assign('pageTitle', 'Addressbook');
        $this->view->assign('club', $this->club);
        $this->view->assign('addresses', $addresses);
        $this->view->assign('storagePid', GeneralUtility::makeInstance(ExtensionConfiguration::class)
            ->get('smartverein', 'settings/addressbookStoragePid'));

        $this->pageRenderer->addCssFile('EXT:smartverein/Resources/Public/Css/DataTables/Styles.css', 'stylesheet', 'all', '', true);
        $this->pageRenderer->addCssFile('EXT:smartverein/Resources/Public/Css/DataTables/ownStyle.css', 'stylesheet', 'all', '', true);
        $this->pageRenderer->addCssFile('EXT:smartverein/Resources/Public/Css/FontAwesome/all.min.css', 'stylesheet', 'all', '', true);

        $this->pageRenderer->addRequireJsConfiguration(
            [
                'paths' => [
                    'simpledatatables' => PathUtility::getPublicResourceWebPath(
                        'EXT:smartverein/Resources/Public/JavaScript/Libs/simple-datatables-min'
                    ),
                ],
                'shim' => [
                    'simpledatatables' => ['exports' => 'simpledatatables'],
                ],
            ]
        );
        return $this->htmlResponse();
    }
}
