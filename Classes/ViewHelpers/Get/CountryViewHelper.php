<?php

/*
 * This file is part of the "SmartVerein - TYPO3 Club Management" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2023 Development-Team <development@t3graf-media.de>, T3graf media-agentur UG
 */

namespace T3graf\Smartverein\ViewHelpers\Get;

use SJBR\StaticInfoTables\Domain\Repository\CountryRepository;
use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;

class CountryViewHelper extends AbstractViewHelper
{
    /**
     * @var CountryRepository
     */
    protected $countryRepository;

    /**
     * Dependency injection of the Country Repository
     *
     * @param CountryRepository $countryRepository
     * @return void
     */
    public function injectCountryRepository(CountryRepository $countryRepository)
    {
        $this->countryRepository = $countryRepository;
    }

   public function initializeArguments()
   {
       // registerArgument($name, $type, $description, $required, $defaultValue, $escape)
       $this->registerArgument('uid', 'string', 'The uid of country to resolve the country name', true);
   }
       /**
        * Returns the country by uid
        *
        * @return string
        */
       public function render()
       {
           $uid = $this->arguments['uid'];
           if ($uid > 0) {
               $country = $this->countryRepository->findOneByUid($uid);

               return $country->getNameLocalized();
           }
       }
}
