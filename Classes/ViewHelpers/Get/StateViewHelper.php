<?php

/*
 * This file is part of the "SmartVerein - TYPO3 Club Management" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2023 Development-Team <development@t3graf-media.de>, T3graf media-agentur UG
 */

namespace T3graf\Smartverein\ViewHelpers\Get;

use SJBR\StaticInfoTables\Domain\Repository\CountryZoneRepository;
use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;

class StateViewHelper extends AbstractViewHelper
{
    /**
     * @var CountryZoneRepository
     */
    protected $countryZoneRepository;

    /**
     * Dependency injection of the Country Zone Repository
     *
     * @param CountryZoneRepository $countryZoneRepository
     * @return void
     */
    public function injectCountryZoneRepository(CountryZoneRepository $countryZoneRepository)
    {
        $this->countryZoneRepository = $countryZoneRepository;
    }

    public function initializeArguments()
    {
        // registerArgument($name, $type, $description, $required, $defaultValue, $escape)
        $this->registerArgument('uid', 'string', 'The uid of country state to resolve name', true);
    }
    /**
     * Returns the state by uid
     *
     * @return string
     */
    public function render()
    {
        $uid = $this->arguments['uid'];
        if ($uid > 0) {
            $state = $this->countryZoneRepository->findOneByUid($uid);

            return $state->getNameLocalized();
        }
    }
}
