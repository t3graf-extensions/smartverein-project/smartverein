<?php

/*
 * This file is part of the "SmartVerein - TYPO3 Club Management" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2023 Development-Team <development@t3graf-media.de>, T3graf media-agentur UG
 */

namespace T3graf\Smartverein\ViewHelpers\Get;

use TYPO3\CMS\Extbase\Utility\LocalizationUtility;
use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;

class StatusViewHelper extends AbstractViewHelper
{
    public function initializeArguments()
    {
        // registerArgument($name, $type, $description, $required, $defaultValue, $escape)
        $this->registerArgument('status', 'string', 'The uid of status to resolve name', true);
        $this->registerArgument('wrapper', 'string', 'The wrapper class of status', false);
    }
    /**
     * Returns the status by uid
     *
     * @return string
     */
    public function render()
    {
        $status = $this->arguments['status'];
        return LocalizationUtility::translate('LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:status.item.' . $status, 'smartverein');
    }
}
