<?php

/*
 * This file is part of the "SmartVerein - TYPO3 Club Management" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2023 Development-Team <development@t3graf-media.de>, T3graf media-agentur UG
 */

namespace T3graf\Smartverein\ViewHelpers\Date;

use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;

class MemberSinceYearsViewHelper extends AbstractViewHelper
{
    public function initializeArguments()
    {
        // registerArgument($name, $type, $description, $required, $defaultValue, $escape)
        $this->registerArgument('date', 'string', 'The birthday to resolve the age', true);
    }
    /**
     * Returns the age
     *
     * @return string
     */
    public function render()
    {
        $beforeYear = \TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate('beforeYear', 'smartverein');
        $since = date_diff(date_create(date('Y', strtotime($this->arguments['date']))), date_create('Y'))->y;
        $afterYear =  \TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate('afterYear', 'smartverein');
        if ($since > 1) {
            $afterYear =  \TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate('afterYears', 'smartverein');
        }
        if ($since === 0) {
            $since = \TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate('lessThanYear', 'smartverein');
        }

        return $beforeYear . ' ' . $since . ' ' . $afterYear;
    }
}
