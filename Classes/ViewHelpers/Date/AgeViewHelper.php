<?php

/*
 * This file is part of the "SmartVerein - TYPO3 Club Management" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2023 Development-Team <development@t3graf-media.de>, T3graf media-agentur UG
 */

namespace T3graf\Smartverein\ViewHelpers\Date;

use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;

class AgeViewHelper extends AbstractViewHelper
{
    public function initializeArguments()
    {
        // registerArgument($name, $type, $description, $required, $defaultValue, $escape)
        $this->registerArgument('birthday', 'string', 'The birthday to resolve the age', true);
    }
    /**
     * Returns the age
     *
     * @return string
     */
    public function render()
    {
        $afterYear =  \TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate('afterAgeYear', 'smartverein');
        $age = date_diff(date_create($this->arguments['birthday']), date_create('today'))->y;
        if ($age > 1) {
            $afterYear =  \TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate('afterAgeYears', 'smartverein');
        }
        if ($age === 0) {
            $age = \TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate('ageLessThanYear', 'smartverein');
        }
        return '(' . $age . ' ' . $afterYear;
    }
}
