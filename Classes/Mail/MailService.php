<?php

/*
 * This file is part of the "SmartVerein - TYPO3 Club Management" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2023 Development-Team <development@t3graf-media.de>, T3graf media-agentur UG
 */

namespace T3graf\Smartverein\Mail;

use Symfony\Component\Mime\Address;
use T3graf\Smartverein\Domain\Model\Club;
use TYPO3\CMS\Core\Configuration\ExtensionConfiguration;
use TYPO3\CMS\Core\Mail\MailMessage;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class MailService
{
    public static function sendSmartvereinMail(array $recipient, $subject, $templateName='', array $variables = []): void
    {
        $variables['club'] = new Club();
        $sender['address'] = GeneralUtility::makeInstance(ExtensionConfiguration::class)
            ->get('smartverein', 'email/from');
        $sender['name'] = GeneralUtility::makeInstance(ExtensionConfiguration::class)
            ->get('smartverein', 'email/fromName');
        $mailBody = (new MailGenerator)->generateHtmlMail($templateName, $variables);
        $textBody = (new MailGenerator)->generatePlainMail($templateName, $variables);

        $mail = GeneralUtility::makeInstance(MailMessage::class);
        $mail->from(new Address($sender['address'], $sender['name']));
        $mail->to(
            new Address($recipient['address'], $recipient['name']),
        );
        $mail->subject($subject);
        $mail->text($textBody);
        $mail->html($mailBody);
        //$mail->attachFromPath('/path/to/my-document.pdf');
        $mail->send();
    }
    public static function addSmartvereinMailToQueue(array $recipient, $subject, $templateName='', array $variables = []): void
    {
        $variables['club'] = new Club();
        $sender['address'] = GeneralUtility::makeInstance(ExtensionConfiguration::class)
            ->get('smartverein', 'email/from');
        $sender['name'] = GeneralUtility::makeInstance(ExtensionConfiguration::class)
            ->get('smartverein', 'email/fromName');
        $mailBody = (new MailGenerator)->generateHtmlMail($template = 'Member/Logindata', $variables);
        $textBody = (new MailGenerator)->generatePlainMail($template = 'Member/Logindata', $variables);

        $mail = GeneralUtility::makeInstance(MailMessage::class);
        $mail->from(new Address($sender['address'], $sender['name']));
        $mail->to(
            new Address($recipient['address'], $recipient['name']),
        );
        $mail->subject($subject);
        $mail->text($textBody);
        $mail->html($mailBody);
        //$mail->attachFromPath('/path/to/my-document.pdf');
        $mail->send();
    }
}
