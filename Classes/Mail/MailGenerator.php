<?php

/*
 * This file is part of the "SmartVerein - TYPO3 Club Management" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2023 Development-Team <development@t3graf-media.de>, T3graf media-agentur UG
 */

namespace T3graf\Smartverein\Mail;

use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Fluid\View\StandaloneView;

class MailGenerator
{
    public function generateHtmlMail($template = '', array $variables = []): string
    {
        $standaloneView = GeneralUtility::makeInstance(StandaloneView::class);
        $standaloneView->setLayoutRootPaths(
            [GeneralUtility::getFileAbsFileName('EXT:smartverein/Resources/Private/Mail/Layouts')]
        );
        $standaloneView->setPartialRootPaths(
            [GeneralUtility::getFileAbsFileName('EXT:smartverein/Resources/Private/Mail/Partials')]
        );
        $standaloneView->setTemplateRootPaths(
            [GeneralUtility::getFileAbsFileName('EXT:smartverein/Resources/Private/Mail/Templates')]
        );
        $standaloneView->setFormat('html');
        $standaloneView->setTemplate($template);
        $standaloneView->assignMultiple($variables);
        return $standaloneView->render();
    }

    public function generatePlainMail($template = '', array $variables = []): string
    {
        $standaloneView = GeneralUtility::makeInstance(StandaloneView::class);
        $standaloneView->setLayoutRootPaths(
            [GeneralUtility::getFileAbsFileName('EXT:smartverein/Resources/Private/Mail/Layouts')]
        );
        $standaloneView->setPartialRootPaths(
            [GeneralUtility::getFileAbsFileName('EXT:smartverein/Resources/Private/Mail/Partials')]
        );
        $standaloneView->setTemplateRootPaths(
            [GeneralUtility::getFileAbsFileName('EXT:smartverein/Resources/Private/Mail/Templates')]
        );
        $standaloneView->setFormat('txt');
        $standaloneView->setTemplate($template);
        $standaloneView->assignMultiple($variables);
        return $standaloneView->render();
    }
}
