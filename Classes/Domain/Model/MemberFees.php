<?php

declare(strict_types=1);

/*
 * This file is part of the "SmartVerein - TYPO3 Club Management" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2023 Development-Team <development@t3graf-media.de>, T3graf media-agentur UG
 */

namespace T3graf\Smartverein\Domain\Model;

/**
 * This file is part of the "smartClub - club-administration" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2023 Development-Team <development@t3graf-media.de>, T3graf media-agentur UG
 */

/**
 * MemberFees
 */
class MemberFees extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{
    /**
     * title
     *
     * @var string
     */
    protected $title = '';

    /**
     * fee
     *
     * @var string
     */
    protected $fee = '';

    /**
     * admissionFee
     *
     * @var string
     */
    protected $admissionFee = '';

    /**
     * membershipFeePeriod
     *
     * @var int
     */
    protected $membershipFeePeriod = 0;

    /**
     * Returns the title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Sets the title
     *
     * @param string $title
     * @return void
     */
    public function setTitle(string $title)
    {
        $this->title = $title;
    }

    /**
     * Returns the fee
     *
     * @return string
     */
    public function getFee()
    {
        return $this->fee;
    }

    /**
     * Sets the fee
     *
     * @param string $fee
     * @return void
     */
    public function setFee(string $fee)
    {
        $this->fee = $fee;
    }

    /**
     * Returns the admissionFee
     *
     * @return string
     */
    public function getAdmissionFee()
    {
        return $this->admissionFee;
    }

    /**
     * Sets the admissionFee
     *
     * @param string $admissionFee
     * @return void
     */
    public function setAdmissionFee(string $admissionFee)
    {
        $this->admissionFee = $admissionFee;
    }

    /**
     * Returns the membershipFeePeriod
     *
     * @return int
     */
    public function getMembershipFeePeriod()
    {
        return $this->membershipFeePeriod;
    }

    /**
     * Sets the membershipFeePeriod
     *
     * @param int $membershipFeePeriod
     * @return void
     */
    public function setMembershipFeePeriod(int $membershipFeePeriod)
    {
        $this->membershipFeePeriod = $membershipFeePeriod;
    }
}
