<?php

declare(strict_types=1);

/*
 * This file is part of the "SmartVerein - TYPO3 Club Management" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2023 Development-Team <development@t3graf-media.de>, T3graf media-agentur UG
 */

namespace T3graf\Smartverein\Domain\Model;

/**
 * This file is part of the "smartClub - club-administration" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2023 Development-Team <development@t3graf-media.de>, T3graf media-agentur UG
 */

/**
 * MailQueue
 */
class MailQueue extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{
    /**
     * subject
     *
     * @var string
     */
    protected $subject = '';

    /**
     * body
     *
     * @var string
     */
    protected $body = '';

    /**
     * bodyHtml
     *
     * @var string
     */
    protected $bodyHtml = '';

    /**
     * sender
     *
     * @var string
     */
    protected $sender = '';

    /**
     * recipient
     *
     * @var string
     */
    protected $recipient = '';

    /**
     * senderName
     *
     * @var string
     */
    protected $senderName = '';

    /**
     * recipientName
     *
     * @var string
     */
    protected $recipientName = '';

    /**
     * attachements
     *
     * @var string
     */
    protected $attachements = '';

    /**
     * dateSent
     *
     * @var \DateTime
     */
    protected $dateSent = null;

    /**
     * type
     *
     * @var string
     */
    protected $type = '';

    /**
     * status
     *
     * @var int
     */
    protected $status = 0;

    /**
     * priority
     *
     * @var int
     */
    protected $priority = 0;

    /**
     * retries
     *
     * @var int
     */
    protected $retries = 0;

    /**
     * processedTime
     *
     * @var \DateTime
     */
    protected $processedTime = null;

    /**
     * errorTime
     *
     * @var \DateTime
     */
    protected $errorTime = null;

    /**
     * errorMessage
     *
     * @var string
     */
    protected $errorMessage = '';

    /**
     * Returns the subject
     *
     * @return string
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * Sets the subject
     *
     * @param string $subject
     * @return void
     */
    public function setSubject(string $subject)
    {
        $this->subject = $subject;
    }

    /**
     * Returns the body
     *
     * @return string
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * Sets the body
     *
     * @param string $body
     * @return void
     */
    public function setBody(string $body)
    {
        $this->body = $body;
    }

    /**
     * Returns the bodyHtml
     *
     * @return string
     */
    public function getBodyHtml()
    {
        return $this->bodyHtml;
    }

    /**
     * Sets the bodyHtml
     *
     * @param string $bodyHtml
     * @return void
     */
    public function setBodyHtml(string $bodyHtml)
    {
        $this->bodyHtml = $bodyHtml;
    }

    /**
     * Returns the sender
     *
     * @return string
     */
    public function getSender()
    {
        return $this->sender;
    }

    /**
     * Sets the sender
     *
     * @param string $sender
     * @return void
     */
    public function setSender(string $sender)
    {
        $this->sender = $sender;
    }

    /**
     * Returns the recipient
     *
     * @return string
     */
    public function getRecipient()
    {
        return $this->recipient;
    }

    /**
     * Sets the recipient
     *
     * @param string $recipient
     * @return void
     */
    public function setRecipient(string $recipient)
    {
        $this->recipient = $recipient;
    }

    /**
     * Returns the senderName
     *
     * @return string
     */
    public function getSenderName()
    {
        return $this->senderName;
    }

    /**
     * Sets the senderName
     *
     * @param string $senderName
     * @return void
     */
    public function setSenderName(string $senderName)
    {
        $this->senderName = $senderName;
    }

    /**
     * Returns the recipientName
     *
     * @return string
     */
    public function getRecipientName()
    {
        return $this->recipientName;
    }

    /**
     * Sets the recipientName
     *
     * @param string $recipientName
     * @return void
     */
    public function setRecipientName(string $recipientName)
    {
        $this->recipientName = $recipientName;
    }

    /**
     * Returns the attachements
     *
     * @return string
     */
    public function getAttachements()
    {
        return $this->attachements;
    }

    /**
     * Sets the attachements
     *
     * @param string $attachements
     * @return void
     */
    public function setAttachements(string $attachements)
    {
        $this->attachements = $attachements;
    }

    /**
     * Returns the dateSent
     *
     * @return \DateTime
     */
    public function getDateSent()
    {
        return $this->dateSent;
    }

    /**
     * Sets the dateSent
     *
     * @param \DateTime $dateSent
     * @return void
     */
    public function setDateSent(\DateTime $dateSent)
    {
        $this->dateSent = $dateSent;
    }

    /**
     * Returns the type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Sets the type
     *
     * @param string $type
     * @return void
     */
    public function setType(string $type)
    {
        $this->type = $type;
    }

    /**
     * Returns the status
     *
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Sets the status
     *
     * @param int $status
     * @return void
     */
    public function setStatus(int $status)
    {
        $this->status = $status;
    }

    /**
     * Returns the priority
     *
     * @return int
     */
    public function getPriority()
    {
        return $this->priority;
    }

    /**
     * Sets the priority
     *
     * @param int $priority
     * @return void
     */
    public function setPriority(int $priority)
    {
        $this->priority = $priority;
    }

    /**
     * Returns the retries
     *
     * @return int
     */
    public function getRetries()
    {
        return $this->retries;
    }

    /**
     * Sets the retries
     *
     * @param int $retries
     * @return void
     */
    public function setRetries(int $retries)
    {
        $this->retries = $retries;
    }

    /**
     * Returns the processedTime
     *
     * @return \DateTime
     */
    public function getProcessedTime()
    {
        return $this->processedTime;
    }

    /**
     * Sets the processedTime
     *
     * @param \DateTime $processedTime
     * @return void
     */
    public function setProcessedTime(\DateTime $processedTime)
    {
        $this->processedTime = $processedTime;
    }

    /**
     * Returns the errorTime
     *
     * @return \DateTime
     */
    public function getErrorTime()
    {
        return $this->errorTime;
    }

    /**
     * Sets the errorTime
     *
     * @param \DateTime $errorTime
     * @return void
     */
    public function setErrorTime(\DateTime $errorTime)
    {
        $this->errorTime = $errorTime;
    }

    /**
     * Returns the errorMessage
     *
     * @return string
     */
    public function getErrorMessage()
    {
        return $this->errorMessage;
    }

    /**
     * Sets the errorMessage
     *
     * @param string $errorMessage
     * @return void
     */
    public function setErrorMessage(string $errorMessage)
    {
        $this->errorMessage = $errorMessage;
    }
}
