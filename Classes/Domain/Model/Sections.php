<?php

declare(strict_types=1);

/*
 * This file is part of the "SmartVerein - TYPO3 Club Management" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2023 Development-Team <development@t3graf-media.de>, T3graf media-agentur UG
 */

namespace T3graf\Smartverein\Domain\Model;

/**
 * This file is part of the "smartClub - club-administration" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2023 Development-Team <development@t3graf-media.de>, T3graf media-agentur UG
 */

/**
 * Sections
 */
class Sections extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{
    /**
     * title
     *
     * @var string
     */
    protected $title = '';

    /**
     * staff
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\T3graf\Smartverein\Domain\Model\SectionPositions>
     * @TYPO3\CMS\Extbase\Annotation\ORM\Cascade("remove")
     */
    protected $staff = null;

    /**
     * __construct
     */
    public function __construct()
    {
        // Do not remove the next line: It would break the functionality
        $this->initializeObject();
    }

    /**
     * Initializes all ObjectStorage properties when model is reconstructed from DB (where __construct is not called)
     * Do not modify this method!
     * It will be rewritten on each save in the extension builder
     * You may modify the constructor of this class instead
     *
     * @return void
     */
    public function initializeObject()
    {
        $this->staff = $this->staff ?: new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
    }

    /**
     * Returns the title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Sets the title
     *
     * @param string $title
     * @return void
     */
    public function setTitle(string $title)
    {
        $this->title = $title;
    }

    /**
     * Adds a SectionPositions
     *
     * @param \T3graf\Smartverein\Domain\Model\SectionPositions $staff
     * @return void
     */
    public function addStaff(\T3graf\Smartverein\Domain\Model\SectionPositions $staff)
    {
        $this->staff->attach($staff);
    }

    /**
     * Removes a SectionPositions
     *
     * @param \T3graf\Smartverein\Domain\Model\SectionPositions $staffToRemove The SectionPositions to be removed
     * @return void
     */
    public function removeStaff(\T3graf\Smartverein\Domain\Model\SectionPositions $staffToRemove)
    {
        $this->staff->detach($staffToRemove);
    }

    /**
     * Returns the staff
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\T3graf\Smartverein\Domain\Model\SectionPositions>
     */
    public function getStaff()
    {
        return $this->staff;
    }

    /**
     * Sets the staff
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\T3graf\Smartverein\Domain\Model\SectionPositions> $staff
     * @return void
     */
    public function setStaff(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $staff)
    {
        $this->staff = $staff;
    }
}
