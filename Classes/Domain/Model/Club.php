<?php

declare(strict_types=1);

/*
 * This file is part of the "SmartVerein - TYPO3 Club Management" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2023 Development-Team <development@t3graf-media.de>, T3graf media-agentur UG
 */

namespace T3graf\Smartverein\Domain\Model;

use TYPO3\CMS\Core\Configuration\ExtensionConfiguration;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Club
 */
class Club extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{
    /**
     * name
     *
     * @var string
     */
    protected $name = null;

    /**
     * shortname
     *
     * @var string
     */
    protected $shortname = null;

    /**
     * foundingyear
     *
     * @var string
     */
    protected $foundingyear = null;

    /**
     * street
     *
     * @var string
     */
    protected $street = null;

    /**
     * city
     *
     * @var string
     */
    protected $city = null;

    /**
     * logo
     *
     * @var string
     */
    protected $logo = null;

    /**
     * telephone
     *
     * @var string
     */
    protected $telephone = null;

    /**
     * email
     *
     * @var string
     */
    protected $email = null;

    /**
     * websiteurl
     *
     * @var string
     */
    protected $websiteurl = null;

    /**
     * taxnumber
     *
     * @var string
     */
    protected $taxnumber = null;

    /**
     * vatid
     *
     * @var string
     */
    protected $vatid = null;

    /**
     * districtcourt
     *
     * @var string
     */
    protected $districtcourt = null;

    /**
     * registernumber
     *
     * @var string
     */
    protected $registernumber = null;

    /**
     * iban
     *
     * @var string
     */
    protected $iban = null;

    /**
     * cin
     *
     * @var string
     */
    protected $cin = null;

    /**
     * Returns the name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Club constructor.
     */
    public function __construct()
    {
        $backendConfiguration = GeneralUtility::makeInstance(ExtensionConfiguration::class)
            ->get('smartverein');

        $this->setName($backendConfiguration['club']['name']);
        $this->setShortname($backendConfiguration['club']['shortname']);
        $this->setFoundingyear($backendConfiguration['club']['foundingYear']);
        $this->setStreet($backendConfiguration['club']['street']);
        $this->setCity($backendConfiguration['club']['city']);
        $this->setLogo($backendConfiguration['club']['logo']);
        $this->setTelephone($backendConfiguration['club']['phone']);
        $this->setEmail($backendConfiguration['club']['email']);
        $this->setWebsiteUrl($backendConfiguration['club']['website']);
        $this->setTaxnumber($backendConfiguration['club']['taxnumber']);
        $this->setVatid($backendConfiguration['club']['VATID']);
        $this->setDistrictcourt($backendConfiguration['club']['districtCourt']);
        $this->setRegisternumber($backendConfiguration['club']['registernumber']);
        $this->setIban($backendConfiguration['club']['IBAN']);
        $this->setCin($backendConfiguration['club']['CIN']);
    }

    /**
     * Sets the name
     *
     * @param string $name
     * @return void
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * Returns the shortname
     *
     * @return string
     */
    public function getShortname()
    {
        return $this->shortname;
    }

    /**
     * Sets the shortname
     *
     * @param string $shortname
     * @return void
     */
    public function setShortname(string $shortname)
    {
        $this->shortname = $shortname;
    }

    /**
     * Returns the foundingyear
     *
     * @return string
     */
    public function getFoundingyear()
    {
        return $this->foundingyear;
    }

    /**
     * Sets the foundingyear
     *
     * @param string $foundingyear
     * @return void
     */
    public function setFoundingyear(string $foundingyear)
    {
        $this->foundingyear = $foundingyear;
    }

    /**
     * Returns the street
     *
     * @return string
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * Sets the street
     *
     * @param string $street
     * @return void
     */
    public function setStreet(string $street)
    {
        $this->street = $street;
    }

    /**
     * Returns the city
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Sets the city
     *
     * @param string $city
     * @return void
     */
    public function setCity(string $city)
    {
        $this->city = $city;
    }

    /**
     * Returns the logo
     *
     * @return string
     */
    public function getLogo()
    {
        return $this->logo;
    }

    /**
     * Sets the logo
     *
     * @param string $logo
     * @return void
     */
    public function setLogo(string $logo)
    {
        $this->logo = $logo;
    }

    /**
     * Returns the telephone
     *
     * @return string
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * Sets the telephone
     *
     * @param string $telephone
     * @return void
     */
    public function setTelephone(string $telephone)
    {
        $this->telephone = $telephone;
    }

    /**
     * Returns the email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Sets the email
     *
     * @param string $email
     * @return void
     */
    public function setEmail(string $email)
    {
        $this->email = $email;
    }

    /**
     * Returns the websiteurl
     *
     * @return string
     */
    public function getWebsiteUrl()
    {
        return $this->websiteurl;
    }

    /**
     * Sets the websiteurl
     *
     * @param string $websiteurl
     * @return void
     */
    public function setWebsiteUrl(string $websiteurl)
    {
        $this->websiteurl = $websiteurl;
    }

    /**
     * Returns the taxnumber
     *
     * @return string
     */
    public function getTaxnumber()
    {
        return $this->taxnumber;
    }

    /**
     * Sets the taxnumber
     *
     * @param string $taxnumber
     * @return void
     */
    public function setTaxnumber(string $taxnumber)
    {
        $this->taxnumber = $taxnumber;
    }

    /**
     * Returns the vatid
     *
     * @return string
     */
    public function getVatid()
    {
        return $this->vatid;
    }

    /**
     * Sets the vatid
     *
     * @param string $vatid
     * @return void
     */
    public function setVatid(string $vatid)
    {
        $this->vatid = $vatid;
    }

    /**
     * Returns the districtcourt
     *
     * @return string
     */
    public function getDistrictcourt()
    {
        return $this->districtcourt;
    }

    /**
     * Sets the districtcourt
     *
     * @param string $districtcourt
     * @return void
     */
    public function setDistrictcourt(string $districtcourt)
    {
        $this->districtcourt = $districtcourt;
    }

    /**
     * Returns the registernumber
     *
     * @return string
     */
    public function getRegisternumber()
    {
        return $this->registernumber;
    }

    /**
     * Sets the registernumber
     *
     * @param string $registernumber
     * @return void
     */
    public function setRegisternumber(string $registernumber)
    {
        $this->registernumber = $registernumber;
    }

    /**
     * Returns the iban
     *
     * @return string
     */
    public function getIban()
    {
        return $this->iban;
    }

    /**
     * Sets the iban
     *
     * @param string $iban
     * @return void
     */
    public function setIban(string $iban)
    {
        $this->iban = $iban;
    }

    /**
     * Returns the cin
     *
     * @return string
     */
    public function getCin()
    {
        return $this->cin;
    }

    /**
     * Sets the cin
     *
     * @param string $cin
     * @return void
     */
    public function setCin(string $cin)
    {
        $this->cin = $cin;
    }
}
