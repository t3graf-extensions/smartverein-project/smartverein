<?php

declare(strict_types=1);

/*
 * This file is part of the "SmartVerein - TYPO3 Club Management" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2023 Development-Team <development@t3graf-media.de>, T3graf media-agentur UG
 */

namespace T3graf\Smartverein\Domain\Model;

/**
 * This file is part of the "smartClub - club-administration" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2023 Development-Team <development@t3graf-media.de>, T3graf media-agentur UG
 */

/**
 * Members
 */
class Members extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{
    /**
     * Member ID
     *
     * @var string|null
     */
    protected $memberId = null;

    /**
     * firstname
     *
     * @var string|null
     */
    protected $firstname = null;

    /**
     * midname
     *
     * @var string
     */
    protected $midname = '';

    /**
     * lastname
     *
     * @var string
     */
    protected $lastname = '';

    /**
     * Member status
     *
     * @var int
     */
    protected $status = 0;

    /**
     * salutation
     *
     * @var int
     */
    protected $salutation = 0;

    /**
     * title
     *
     * @var string
     */
    protected $title = '';

    /**
     * street
     *
     * @var string
     */
    protected $street = '';

    /**
     * zip
     *
     * @var string
     */
    protected $zip = '';

    /**
     * city
     *
     * @var string
     */
    protected $city = '';

    /**
     * state
     *
     * @var string
     */
    protected $state = '';

    /**
     * country
     *
     * @var string
     */
    protected $country = '';

    /**
     * additionToAddress
     *
     * @var string
     */
    protected $additionToAddress = '';

    /**
     * leavingWish
     *
     * @var bool
     */
    protected $leavingWish = false;

    /**
     * dateofbirth
     *
     * @var \DateTime
     */
    protected $dateofbirth = null;

    /**
     * profileImage
     *
     * @var \TYPO3\CMS\Extbase\Domain\Model\FileReference
     * @TYPO3\CMS\Extbase\Annotation\ORM\Cascade("remove")
     */
    protected $profileImage = null;

    /**
     * memberFolder
     *
     * @var string
     */
    protected $memberFolder = '';

    /**
     * gender
     *
     * @var int
     */
    protected $gender = 0;

    /**
     * preferredCommunication
     *
     * @var int
     */
    protected $preferredCommunication = 0;

    /**
     * methodOfPayment
     *
     * @var int
     */
    protected $methodOfPayment = 0;

    /**
     * accountOwner
     *
     * @var string
     */
    protected $accountOwner = '';

    /**
     * iban
     *
     * @var string
     */
    protected $iban = '';

    /**
     * bic
     *
     * @var string
     */
    protected $bic = '';

    /**
     * customfield1
     *
     * @var string
     */
    protected $customfield1 = '';

    /**
     * customfield2
     *
     * @var string
     */
    protected $customfield2 = '';

    /**
     * customfield3
     *
     * @var string
     */
    protected $customfield3 = '';

    /**
     * customfield4
     *
     * @var string
     */
    protected $customfield4 = '';

    /**
     * customfield5
     *
     * @var string
     */
    protected $customfield5 = '';

    /**
     * customfield6
     *
     * @var string
     */
    protected $customfield6 = '';

    /**
     * customfield7
     *
     * @var string
     */
    protected $customfield7 = '';

    /**
     * customfield8
     *
     * @var string
     */
    protected $customfield8 = '';

    /**
     * customfield9
     *
     * @var string
     */
    protected $customfield9 = '';

    /**
     * customfield10
     *
     * @var string
     */
    protected $customfield10 = '';

    /**
     * entryDate
     *
     * @var \DateTime
     */
    protected $entryDate = null;

    /**
     * leavingDate
     *
     * @var \DateTime
     */
    protected $leavingDate = null;

    /**
     * mandateReference
     *
     * @var string
     */
    protected $mandateReference = '';

    /**
     * mandateDate
     *
     * @var \DateTime
     */
    protected $mandateDate = null;

    /**
     * sepaMandate
     *
     * @var \TYPO3\CMS\Extbase\Domain\Model\FileReference
     * @TYPO3\CMS\Extbase\Annotation\ORM\Cascade("remove")
     */
    protected $sepaMandate = null;

    /**
     * membershipApplication
     *
     * @var \TYPO3\CMS\Extbase\Domain\Model\FileReference
     * @TYPO3\CMS\Extbase\Annotation\ORM\Cascade("remove")
     */
    protected $membershipApplication = null;

    /**
     * leavingStatement
     *
     * @var \TYPO3\CMS\Extbase\Domain\Model\FileReference
     * @TYPO3\CMS\Extbase\Annotation\ORM\Cascade("remove")
     */
    protected $leavingStatement = null;

    /**
     * declarationOfConsent
     *
     * @var \TYPO3\CMS\Extbase\Domain\Model\FileReference
     * @TYPO3\CMS\Extbase\Annotation\ORM\Cascade("remove")
     */
    protected $declarationOfConsent = null;

    /**
     * paymentInterval
     *
     * @var int
     */
    protected $paymentInterval = 0;

    /**
     * email
     *
     * @var string
     */
    protected $email = '';

    /**
     * phone
     *
     * @var string
     */
    protected $phone = '';

    /**
     * mobile
     *
     * @var string
     */
    protected $mobile = '';

    /**
     * fax
     *
     * @var string
     */
    protected $fax = '';

    /**
     * memberType
     *
     * @var int
     */
    protected $memberType = 0;

    /**
     * organisation
     *
     * @var string
     */
    protected $organisation = '';

    /**
     * contactPerson
     *
     * @var string
     */
    protected $contactPerson = '';

    /**
     * position
     *
     * @var string
     */
    protected $position = '';

    /**
     * department
     *
     * @var string
     */
    protected $department = '';

    /**
     * website
     *
     * @var string
     */
    protected $website = '';

    /**
     * contactPersonSalutation
     *
     * @var int
     */
    protected $contactPersonSalutation = 0;

    /**
     * FE User
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\CMS\Extbase\Domain\Model\FrontendUser>
     * @TYPO3\CMS\Extbase\Annotation\ORM\Cascade("remove")
     */
    protected $feUser = null;

    /**
     * FE User
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\T3graf\Smartverein\Domain\Model\Contacts>
     * @TYPO3\CMS\Extbase\Annotation\ORM\Cascade("remove")
     */
    protected $contacts = null;

    /**
     * Belongs to family
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\T3graf\Smartverein\Domain\Model\Members>
     * @TYPO3\CMS\Extbase\Annotation\ORM\Cascade("remove")
     */
    protected $belongsto = null;

    /**
     * Functions in club
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\T3graf\Smartverein\Domain\Model\Functions>
     * @TYPO3\CMS\Extbase\Annotation\ORM\Cascade("remove")
     */
    protected $functions = null;

    /**
     * Positions in club
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\T3graf\Smartverein\Domain\Model\Positions>
     * @TYPO3\CMS\Extbase\Annotation\ORM\Cascade("remove")
     */
    protected $positions = null;

    /**
     * Role/Rank in club
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\T3graf\Smartverein\Domain\Model\Roles>
     * @TYPO3\CMS\Extbase\Annotation\ORM\Cascade("remove")
     */
    protected $role = null;

    /**
     * Section member belongs
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\T3graf\Smartverein\Domain\Model\Sections>
     * @TYPO3\CMS\Extbase\Annotation\ORM\Cascade("remove")
     */
    protected $sections = null;

    /**
     * Vita entries of member
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\T3graf\Smartverein\Domain\Model\VitaEntries>
     * @TYPO3\CMS\Extbase\Annotation\ORM\Cascade("remove")
     */
    protected $vita = null;

    /**
     * changes to member profil
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\T3graf\Smartverein\Domain\Model\Changes>
     * @TYPO3\CMS\Extbase\Annotation\ORM\Cascade("remove")
     */
    protected $changelog = null;

    /**
     * changes to member profil
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\T3graf\Smartverein\Domain\Model\MemberFees>
     * @TYPO3\CMS\Extbase\Annotation\ORM\Cascade("remove")
     */
    protected $memberFee = null;

    /**
     * __construct
     */
    public function __construct()
    {
        // Do not remove the next line: It would break the functionality
        $this->initializeObject();
    }

    /**
     * Initializes all ObjectStorage properties when model is reconstructed from DB (where __construct is not called)
     * Do not modify this method!
     * It will be rewritten on each save in the extension builder
     * You may modify the constructor of this class instead
     *
     * @return void
     */
    public function initializeObject()
    {
        $this->feUser = $this->feUser ?: new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $this->contacts = $this->contacts ?: new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $this->belongsto = $this->belongsto ?: new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $this->functions = $this->functions ?: new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $this->positions = $this->positions ?: new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $this->role = $this->role ?: new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $this->sections = $this->sections ?: new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $this->vita = $this->vita ?: new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $this->changelog = $this->changelog ?: new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $this->memberFee = $this->memberFee ?: new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
    }

    /**
     * Returns the memberId
     *
     * @return string|null
     */
    public function getMemberId()
    {
        return $this->memberId;
    }

    /**
     * Sets the memberId
     *
     * @param string|null $memberId
     * @return void
     */
    public function setMemberId(?string $memberId)
    {
        $this->memberId = $memberId;
    }

    /**
     * Returns the firstname
     *
     * @return string|null
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * Sets the firstname
     *
     * @param string|null $firstname
     * @return void
     */
    public function setFirstname(?string $firstname)
    {
        $this->firstname = $firstname;
    }

    /**
     * Returns the midname
     *
     * @return string
     */
    public function getMidname()
    {
        return $this->midname;
    }

    /**
     * Sets the midname
     *
     * @param string $midname
     * @return void
     */
    public function setMidname(string $midname)
    {
        $this->midname = $midname;
    }

    /**
     * Returns the lastname
     *
     * @return string
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * Sets the lastname
     *
     * @param string $lastname
     * @return void
     */
    public function setLastname(string $lastname)
    {
        $this->lastname = $lastname;
    }

    /**
     * Returns the status
     *
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Sets the status
     *
     * @param int $status
     * @return void
     */
    public function setStatus(int $status)
    {
        $this->status = $status;
    }

    /**
     * Returns the salutation
     *
     * @return int
     */
    public function getSalutation()
    {
        return $this->salutation;
    }

    /**
     * Sets the salutation
     *
     * @param int $salutation
     * @return void
     */
    public function setSalutation(int $salutation)
    {
        $this->salutation = $salutation;
    }

    /**
     * Returns the title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Sets the title
     *
     * @param string $title
     * @return void
     */
    public function setTitle(string $title)
    {
        $this->title = $title;
    }

    /**
     * Returns the street
     *
     * @return string
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * Sets the street
     *
     * @param string $street
     * @return void
     */
    public function setStreet(string $street)
    {
        $this->street = $street;
    }

    /**
     * Returns the zip
     *
     * @return string
     */
    public function getZip()
    {
        return $this->zip;
    }

    /**
     * Sets the zip
     *
     * @param string $zip
     * @return void
     */
    public function setZip(string $zip)
    {
        $this->zip = $zip;
    }

    /**
     * Returns the city
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Sets the city
     *
     * @param string $city
     * @return void
     */
    public function setCity(string $city)
    {
        $this->city = $city;
    }

    /**
     * Returns the state
     *
     * @return string
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * Sets the state
     *
     * @param string $state
     * @return void
     */
    public function setState(string $state)
    {
        $this->state = $state;
    }

    /**
     * Returns the country
     *
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Sets the country
     *
     * @param string $country
     * @return void
     */
    public function setCountry(string $country)
    {
        $this->country = $country;
    }

    /**
     * Returns the additionToAddress
     *
     * @return string
     */
    public function getAdditionToAddress()
    {
        return $this->additionToAddress;
    }

    /**
     * Sets the additionToAddress
     *
     * @param string $additionToAddress
     * @return void
     */
    public function setAdditionToAddress(string $additionToAddress)
    {
        $this->additionToAddress = $additionToAddress;
    }

    /**
     * Returns the leavingWish
     *
     * @return bool
     */
    public function getLeavingWish()
    {
        return $this->leavingWish;
    }

    /**
     * Sets the leavingWish
     *
     * @param bool $leavingWish
     * @return void
     */
    public function setLeavingWish(bool $leavingWish)
    {
        $this->leavingWish = $leavingWish;
    }

    /**
     * Returns the boolean state of leavingWish
     *
     * @return bool
     */
    public function isLeavingWish()
    {
        return $this->leavingWish;
    }

    /**
     * Returns the dateofbirth
     *
     * @return \DateTime
     */
    public function getDateofbirth()
    {
        return $this->dateofbirth;
    }

    /**
     * Sets the dateofbirth
     *
     * @param \DateTime $dateofbirth
     * @return void
     */
    public function setDateofbirth(\DateTime $dateofbirth)
    {
        $this->dateofbirth = $dateofbirth;
    }

    /**
     * Returns the profileImage
     *
     * @return \TYPO3\CMS\Extbase\Domain\Model\FileReference
     */
    public function getProfileImage()
    {
        return $this->profileImage;
    }

    /**
     * Sets the profileImage
     *
     * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $profileImage
     * @return void
     */
    public function setProfileImage(\TYPO3\CMS\Extbase\Domain\Model\FileReference $profileImage)
    {
        $this->profileImage = $profileImage;
    }

    /**
     * Returns the memberFolder
     *
     * @return string
     */
    public function getMemberFolder()
    {
        return $this->memberFolder;
    }

    /**
     * Sets the memberFolder
     *
     * @param string $memberFolder
     * @return void
     */
    public function setMemberFolder(string $memberFolder)
    {
        $this->memberFolder = $memberFolder;
    }

    /**
     * Returns the gender
     *
     * @return int
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * Sets the gender
     *
     * @param int $gender
     * @return void
     */
    public function setGender(int $gender)
    {
        $this->gender = $gender;
    }

    /**
     * Returns the preferredCommunication
     *
     * @return int
     */
    public function getPreferredCommunication()
    {
        return $this->preferredCommunication;
    }

    /**
     * Sets the preferredCommunication
     *
     * @param int $preferredCommunication
     * @return void
     */
    public function setPreferredCommunication(int $preferredCommunication)
    {
        $this->preferredCommunication = $preferredCommunication;
    }

    /**
     * Returns the methodOfPayment
     *
     * @return int
     */
    public function getMethodOfPayment()
    {
        return $this->methodOfPayment;
    }

    /**
     * Sets the methodOfPayment
     *
     * @param int $methodOfPayment
     * @return void
     */
    public function setMethodOfPayment(int $methodOfPayment)
    {
        $this->methodOfPayment = $methodOfPayment;
    }

    /**
     * Returns the accountOwner
     *
     * @return string
     */
    public function getAccountOwner()
    {
        return $this->accountOwner;
    }

    /**
     * Sets the accountOwner
     *
     * @param string $accountOwner
     * @return void
     */
    public function setAccountOwner(string $accountOwner)
    {
        $this->accountOwner = $accountOwner;
    }

    /**
     * Returns the iban
     *
     * @return string
     */
    public function getIban()
    {
        return $this->iban;
    }

    /**
     * Sets the iban
     *
     * @param string $iban
     * @return void
     */
    public function setIban(string $iban)
    {
        $this->iban = $iban;
    }

    /**
     * Returns the bic
     *
     * @return string
     */
    public function getBic()
    {
        return $this->bic;
    }

    /**
     * Sets the bic
     *
     * @param string $bic
     * @return void
     */
    public function setBic(string $bic)
    {
        $this->bic = $bic;
    }

    /**
     * Returns the customfield1
     *
     * @return string
     */
    public function getCustomfield1()
    {
        return $this->customfield1;
    }

    /**
     * Sets the customfield1
     *
     * @param string $customfield1
     * @return void
     */
    public function setCustomfield1(string $customfield1)
    {
        $this->customfield1 = $customfield1;
    }

    /**
     * Returns the customfield2
     *
     * @return string
     */
    public function getCustomfield2()
    {
        return $this->customfield2;
    }

    /**
     * Sets the customfield2
     *
     * @param string $customfield2
     * @return void
     */
    public function setCustomfield2(string $customfield2)
    {
        $this->customfield2 = $customfield2;
    }

    /**
     * Returns the customfield3
     *
     * @return string
     */
    public function getCustomfield3()
    {
        return $this->customfield3;
    }

    /**
     * Sets the customfield3
     *
     * @param string $customfield3
     * @return void
     */
    public function setCustomfield3(string $customfield3)
    {
        $this->customfield3 = $customfield3;
    }

    /**
     * Returns the customfield4
     *
     * @return string
     */
    public function getCustomfield4()
    {
        return $this->customfield4;
    }

    /**
     * Sets the customfield4
     *
     * @param string $customfield4
     * @return void
     */
    public function setCustomfield4(string $customfield4)
    {
        $this->customfield4 = $customfield4;
    }

    /**
     * Returns the customfield5
     *
     * @return string
     */
    public function getCustomfield5()
    {
        return $this->customfield5;
    }

    /**
     * Sets the customfield5
     *
     * @param string $customfield5
     * @return void
     */
    public function setCustomfield5(string $customfield5)
    {
        $this->customfield5 = $customfield5;
    }

    /**
     * Returns the customfield6
     *
     * @return string
     */
    public function getCustomfield6()
    {
        return $this->customfield6;
    }

    /**
     * Sets the customfield6
     *
     * @param string $customfield6
     * @return void
     */
    public function setCustomfield6(string $customfield6)
    {
        $this->customfield6 = $customfield6;
    }

    /**
     * Returns the customfield7
     *
     * @return string
     */
    public function getCustomfield7()
    {
        return $this->customfield7;
    }

    /**
     * Sets the customfield7
     *
     * @param string $customfield7
     * @return void
     */
    public function setCustomfield7(string $customfield7)
    {
        $this->customfield7 = $customfield7;
    }

    /**
     * Returns the customfield8
     *
     * @return string
     */
    public function getCustomfield8()
    {
        return $this->customfield8;
    }

    /**
     * Sets the customfield8
     *
     * @param string $customfield8
     * @return void
     */
    public function setCustomfield8(string $customfield8)
    {
        $this->customfield8 = $customfield8;
    }

    /**
     * Returns the customfield9
     *
     * @return string
     */
    public function getCustomfield9()
    {
        return $this->customfield9;
    }

    /**
     * Sets the customfield9
     *
     * @param string $customfield9
     * @return void
     */
    public function setCustomfield9(string $customfield9)
    {
        $this->customfield9 = $customfield9;
    }

    /**
     * Returns the customfield10
     *
     * @return string
     */
    public function getCustomfield10()
    {
        return $this->customfield10;
    }

    /**
     * Sets the customfield10
     *
     * @param string $customfield10
     * @return void
     */
    public function setCustomfield10(string $customfield10)
    {
        $this->customfield10 = $customfield10;
    }

    /**
     * Returns the entryDate
     *
     * @return \DateTime
     */
    public function getEntryDate()
    {
        return $this->entryDate;
    }

    /**
     * Sets the entryDate
     *
     * @param \DateTime $entryDate
     * @return void
     */
    public function setEntryDate(\DateTime $entryDate)
    {
        $this->entryDate = $entryDate;
    }

    /**
     * Returns the leavingDate
     *
     * @return \DateTime
     */
    public function getLeavingDate()
    {
        return $this->leavingDate;
    }

    /**
     * Sets the leavingDate
     *
     * @param \DateTime $leavingDate
     * @return void
     */
    public function setLeavingDate(\DateTime $leavingDate)
    {
        $this->leavingDate = $leavingDate;
    }

    /**
     * Returns the mandateReference
     *
     * @return string
     */
    public function getMandateReference()
    {
        return $this->mandateReference;
    }

    /**
     * Sets the mandateReference
     *
     * @param string $mandateReference
     * @return void
     */
    public function setMandateReference(string $mandateReference)
    {
        $this->mandateReference = $mandateReference;
    }

    /**
     * Returns the mandateDate
     *
     * @return \DateTime
     */
    public function getMandateDate()
    {
        return $this->mandateDate;
    }

    /**
     * Sets the mandateDate
     *
     * @param \DateTime $mandateDate
     * @return void
     */
    public function setMandateDate(\DateTime $mandateDate)
    {
        $this->mandateDate = $mandateDate;
    }

    /**
     * Returns the sepaMandate
     *
     * @return \TYPO3\CMS\Extbase\Domain\Model\FileReference
     */
    public function getSepaMandate()
    {
        return $this->sepaMandate;
    }

    /**
     * Sets the sepaMandate
     *
     * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $sepaMandate
     * @return void
     */
    public function setSepaMandate(\TYPO3\CMS\Extbase\Domain\Model\FileReference $sepaMandate)
    {
        $this->sepaMandate = $sepaMandate;
    }

    /**
     * Returns the membershipApplication
     *
     * @return \TYPO3\CMS\Extbase\Domain\Model\FileReference
     */
    public function getMembershipApplication()
    {
        return $this->membershipApplication;
    }

    /**
     * Sets the membershipApplication
     *
     * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $membershipApplication
     * @return void
     */
    public function setMembershipApplication(\TYPO3\CMS\Extbase\Domain\Model\FileReference $membershipApplication)
    {
        $this->membershipApplication = $membershipApplication;
    }

    /**
     * Returns the leavingStatement
     *
     * @return \TYPO3\CMS\Extbase\Domain\Model\FileReference
     */
    public function getLeavingStatement()
    {
        return $this->leavingStatement;
    }

    /**
     * Sets the leavingStatement
     *
     * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $leavingStatement
     * @return void
     */
    public function setLeavingStatement(\TYPO3\CMS\Extbase\Domain\Model\FileReference $leavingStatement)
    {
        $this->leavingStatement = $leavingStatement;
    }

    /**
     * Returns the declarationOfConsent
     *
     * @return \TYPO3\CMS\Extbase\Domain\Model\FileReference
     */
    public function getDeclarationOfConsent()
    {
        return $this->declarationOfConsent;
    }

    /**
     * Sets the declarationOfConsent
     *
     * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $declarationOfConsent
     * @return void
     */
    public function setDeclarationOfConsent(\TYPO3\CMS\Extbase\Domain\Model\FileReference $declarationOfConsent)
    {
        $this->declarationOfConsent = $declarationOfConsent;
    }

    /**
     * Returns the paymentInterval
     *
     * @return int
     */
    public function getPaymentInterval()
    {
        return $this->paymentInterval;
    }

    /**
     * Sets the paymentInterval
     *
     * @param int $paymentInterval
     * @return void
     */
    public function setPaymentInterval(int $paymentInterval)
    {
        $this->paymentInterval = $paymentInterval;
    }

    /**
     * Returns the email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Sets the email
     *
     * @param string $email
     * @return void
     */
    public function setEmail(string $email)
    {
        $this->email = $email;
    }

    /**
     * Returns the phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Sets the phone
     *
     * @param string $phone
     * @return void
     */
    public function setPhone(string $phone)
    {
        $this->phone = $phone;
    }

    /**
     * Returns the mobile
     *
     * @return string
     */
    public function getMobile()
    {
        return $this->mobile;
    }

    /**
     * Sets the mobile
     *
     * @param string $mobile
     * @return void
     */
    public function setMobile(string $mobile)
    {
        $this->mobile = $mobile;
    }

    /**
     * Returns the fax
     *
     * @return string
     */
    public function getFax()
    {
        return $this->fax;
    }

    /**
     * Sets the fax
     *
     * @param string $fax
     * @return void
     */
    public function setFax(string $fax)
    {
        $this->fax = $fax;
    }

    /**
     * Returns the memberType
     *
     * @return int
     */
    public function getMemberType()
    {
        return $this->memberType;
    }

    /**
     * Sets the memberType
     *
     * @param int $memberType
     * @return void
     */
    public function setMemberType(int $memberType)
    {
        $this->memberType = $memberType;
    }

    /**
     * Returns the organisation
     *
     * @return string
     */
    public function getOrganisation()
    {
        return $this->organisation;
    }

    /**
     * Sets the organisation
     *
     * @param string $organisation
     * @return void
     */
    public function setOrganisation(string $organisation)
    {
        $this->organisation = $organisation;
    }

    /**
     * Returns the contactPerson
     *
     * @return string
     */
    public function getContactPerson()
    {
        return $this->contactPerson;
    }

    /**
     * Sets the contactPerson
     *
     * @param string $contactPerson
     * @return void
     */
    public function setContactPerson(string $contactPerson)
    {
        $this->contactPerson = $contactPerson;
    }

    /**
     * Returns the position
     *
     * @return string
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Sets the position
     *
     * @param string $position
     * @return void
     */
    public function setPosition(string $position)
    {
        $this->position = $position;
    }

    /**
     * Returns the department
     *
     * @return string
     */
    public function getDepartment()
    {
        return $this->department;
    }

    /**
     * Sets the department
     *
     * @param string $department
     * @return void
     */
    public function setDepartment(string $department)
    {
        $this->department = $department;
    }

    /**
     * Returns the website
     *
     * @return string
     */
    public function getWebsite()
    {
        return $this->website;
    }

    /**
     * Sets the website
     *
     * @param string $website
     * @return void
     */
    public function setWebsite(string $website)
    {
        $this->website = $website;
    }

    /**
     * Returns the contactPersonSalutation
     *
     * @return int
     */
    public function getContactPersonSalutation()
    {
        return $this->contactPersonSalutation;
    }

    /**
     * Sets the contactPersonSalutation
     *
     * @param int $contactPersonSalutation
     * @return void
     */
    public function setContactPersonSalutation(int $contactPersonSalutation)
    {
        $this->contactPersonSalutation = $contactPersonSalutation;
    }

    /**
     * Adds a FrontendUser
     *
     * @param \TYPO3\CMS\Extbase\Domain\Model\FrontendUser $feUser
     * @return void
     */
    public function addFeUser(\TYPO3\CMS\Extbase\Domain\Model\FrontendUser $feUser)
    {
        $this->feUser->attach($feUser);
    }

    /**
     * Removes a FrontendUser
     *
     * @param \TYPO3\CMS\Extbase\Domain\Model\FrontendUser $feUserToRemove The FrontendUser to be removed
     * @return void
     */
    public function removeFeUser(\TYPO3\CMS\Extbase\Domain\Model\FrontendUser $feUserToRemove)
    {
        $this->feUser->detach($feUserToRemove);
    }

    /**
     * Returns the feUser
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\CMS\Extbase\Domain\Model\FrontendUser>
     */
    public function getFeUser()
    {
        return $this->feUser;
    }

    /**
     * Sets the feUser
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\CMS\Extbase\Domain\Model\FrontendUser> $feUser
     * @return void
     */
    public function setFeUser(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $feUser)
    {
        $this->feUser = $feUser;
    }

    /**
     * Adds a Contacts
     *
     * @param \T3graf\Smartverein\Domain\Model\Contacts $contact
     * @return void
     */
    public function addContact(\T3graf\Smartverein\Domain\Model\Contacts $contact)
    {
        $this->contacts->attach($contact);
    }

    /**
     * Removes a Contacts
     *
     * @param \T3graf\Smartverein\Domain\Model\Contacts $contactToRemove The Contacts to be removed
     * @return void
     */
    public function removeContact(\T3graf\Smartverein\Domain\Model\Contacts $contactToRemove)
    {
        $this->contacts->detach($contactToRemove);
    }

    /**
     * Returns the contacts
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\T3graf\Smartverein\Domain\Model\Contacts>
     */
    public function getContacts()
    {
        return $this->contacts;
    }

    /**
     * Sets the contacts
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\T3graf\Smartverein\Domain\Model\Contacts> $contacts
     * @return void
     */
    public function setContacts(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $contacts)
    {
        $this->contacts = $contacts;
    }

    /**
     * Adds a Members
     *
     * @param \T3graf\Smartverein\Domain\Model\Members $belongsto
     * @return void
     */
    public function addBelongsto(self $belongsto)
    {
        $this->belongsto->attach($belongsto);
    }

    /**
     * Removes a Members
     *
     * @param \T3graf\Smartverein\Domain\Model\Members $belongstoToRemove The Members to be removed
     * @return void
     */
    public function removeBelongsto(self $belongstoToRemove)
    {
        $this->belongsto->detach($belongstoToRemove);
    }

    /**
     * Returns the belongsto
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\T3graf\Smartverein\Domain\Model\Members>
     */
    public function getBelongsto()
    {
        return $this->belongsto;
    }

    /**
     * Sets the belongsto
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\T3graf\Smartverein\Domain\Model\Members> $belongsto
     * @return void
     */
    public function setBelongsto(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $belongsto)
    {
        $this->belongsto = $belongsto;
    }

    /**
     * Adds a Functions
     *
     * @param \T3graf\Smartverein\Domain\Model\Functions $function
     * @return void
     */
    public function addFunction(\T3graf\Smartverein\Domain\Model\Functions $function)
    {
        $this->functions->attach($function);
    }

    /**
     * Removes a Functions
     *
     * @param \T3graf\Smartverein\Domain\Model\Functions $functionToRemove The Functions to be removed
     * @return void
     */
    public function removeFunction(\T3graf\Smartverein\Domain\Model\Functions $functionToRemove)
    {
        $this->functions->detach($functionToRemove);
    }

    /**
     * Returns the functions
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\T3graf\Smartverein\Domain\Model\Functions>
     */
    public function getFunctions()
    {
        return $this->functions;
    }

    /**
     * Sets the functions
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\T3graf\Smartverein\Domain\Model\Functions> $functions
     * @return void
     */
    public function setFunctions(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $functions)
    {
        $this->functions = $functions;
    }

    /**
     * Adds a Positions
     *
     * @param \T3graf\Smartverein\Domain\Model\Positions $position
     * @return void
     */
    public function addPosition(\T3graf\Smartverein\Domain\Model\Positions $position)
    {
        $this->positions->attach($position);
    }

    /**
     * Removes a Positions
     *
     * @param \T3graf\Smartverein\Domain\Model\Positions $positionToRemove The Positions to be removed
     * @return void
     */
    public function removePosition(\T3graf\Smartverein\Domain\Model\Positions $positionToRemove)
    {
        $this->positions->detach($positionToRemove);
    }

    /**
     * Returns the positions
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\T3graf\Smartverein\Domain\Model\Positions>
     */
    public function getPositions()
    {
        return $this->positions;
    }

    /**
     * Sets the positions
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\T3graf\Smartverein\Domain\Model\Positions> $positions
     * @return void
     */
    public function setPositions(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $positions)
    {
        $this->positions = $positions;
    }

    /**
     * Adds a Roles
     *
     * @param \T3graf\Smartverein\Domain\Model\Roles $role
     * @return void
     */
    public function addRole(\T3graf\Smartverein\Domain\Model\Roles $role)
    {
        $this->role->attach($role);
    }

    /**
     * Removes a Roles
     *
     * @param \T3graf\Smartverein\Domain\Model\Roles $roleToRemove The Roles to be removed
     * @return void
     */
    public function removeRole(\T3graf\Smartverein\Domain\Model\Roles $roleToRemove)
    {
        $this->role->detach($roleToRemove);
    }

    /**
     * Returns the role
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\T3graf\Smartverein\Domain\Model\Roles>
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * Sets the role
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\T3graf\Smartverein\Domain\Model\Roles> $role
     * @return void
     */
    public function setRole(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $role)
    {
        $this->role = $role;
    }

    /**
     * Adds a Sections
     *
     * @param \T3graf\Smartverein\Domain\Model\Sections $section
     * @return void
     */
    public function addSection(\T3graf\Smartverein\Domain\Model\Sections $section)
    {
        $this->sections->attach($section);
    }

    /**
     * Removes a Sections
     *
     * @param \T3graf\Smartverein\Domain\Model\Sections $sectionToRemove The Sections to be removed
     * @return void
     */
    public function removeSection(\T3graf\Smartverein\Domain\Model\Sections $sectionToRemove)
    {
        $this->sections->detach($sectionToRemove);
    }

    /**
     * Returns the sections
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\T3graf\Smartverein\Domain\Model\Sections>
     */
    public function getSections()
    {
        return $this->sections;
    }

    /**
     * Sets the sections
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\T3graf\Smartverein\Domain\Model\Sections> $sections
     * @return void
     */
    public function setSections(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $sections)
    {
        $this->sections = $sections;
    }

    /**
     * Adds a VitaEntries
     *
     * @param \T3graf\Smartverein\Domain\Model\VitaEntries $vitum
     * @return void
     */
    public function addVitum(\T3graf\Smartverein\Domain\Model\VitaEntries $vitum)
    {
        $this->vita->attach($vitum);
    }

    /**
     * Removes a VitaEntries
     *
     * @param \T3graf\Smartverein\Domain\Model\VitaEntries $vitumToRemove The VitaEntries to be removed
     * @return void
     */
    public function removeVitum(\T3graf\Smartverein\Domain\Model\VitaEntries $vitumToRemove)
    {
        $this->vita->detach($vitumToRemove);
    }

    /**
     * Returns the vita
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\T3graf\Smartverein\Domain\Model\VitaEntries>
     */
    public function getVita()
    {
        return $this->vita;
    }

    /**
     * Sets the vita
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\T3graf\Smartverein\Domain\Model\VitaEntries> $vita
     * @return void
     */
    public function setVita(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $vita)
    {
        $this->vita = $vita;
    }

    /**
     * Adds a Changes
     *
     * @param \T3graf\Smartverein\Domain\Model\Changes $changelog
     * @return void
     */
    public function addChangelog(\T3graf\Smartverein\Domain\Model\Changes $changelog)
    {
        $this->changelog->attach($changelog);
    }

    /**
     * Removes a Changes
     *
     * @param \T3graf\Smartverein\Domain\Model\Changes $changelogToRemove The Changes to be removed
     * @return void
     */
    public function removeChangelog(\T3graf\Smartverein\Domain\Model\Changes $changelogToRemove)
    {
        $this->changelog->detach($changelogToRemove);
    }

    /**
     * Returns the changelog
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\T3graf\Smartverein\Domain\Model\Changes>
     */
    public function getChangelog()
    {
        return $this->changelog;
    }

    /**
     * Sets the changelog
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\T3graf\Smartverein\Domain\Model\Changes> $changelog
     * @return void
     */
    public function setChangelog(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $changelog)
    {
        $this->changelog = $changelog;
    }

    /**
     * Adds a MemberFees
     *
     * @param \T3graf\Smartverein\Domain\Model\MemberFees $memberFee
     * @return void
     */
    public function addMemberFee(\T3graf\Smartverein\Domain\Model\MemberFees $memberFee)
    {
        $this->memberFee->attach($memberFee);
    }

    /**
     * Removes a MemberFees
     *
     * @param \T3graf\Smartverein\Domain\Model\MemberFees $memberFeeToRemove The MemberFees to be removed
     * @return void
     */
    public function removeMemberFee(\T3graf\Smartverein\Domain\Model\MemberFees $memberFeeToRemove)
    {
        $this->memberFee->detach($memberFeeToRemove);
    }

    /**
     * Returns the memberFee
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\T3graf\Smartverein\Domain\Model\MemberFees>
     */
    public function getMemberFee()
    {
        return $this->memberFee;
    }

    /**
     * Sets the memberFee
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\T3graf\Smartverein\Domain\Model\MemberFees> $memberFee
     * @return void
     */
    public function setMemberFee(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $memberFee)
    {
        $this->memberFee = $memberFee;
    }
}
