<?php

declare(strict_types=1);

/*
 * This file is part of the "SmartVerein - TYPO3 Club Management" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2023 Development-Team <development@t3graf-media.de>, T3graf media-agentur UG
 */

namespace T3graf\Smartverein\Domain\Model;

/**
 * This file is part of the "smartClub - club-administration" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2023 Development-Team <development@t3graf-media.de>, T3graf media-agentur UG
 */

/**
 * SectionPositions
 */
class SectionPositions extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{
    /**
     * member
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\T3graf\Smartverein\Domain\Model\Members>
     * @TYPO3\CMS\Extbase\Annotation\ORM\Cascade("remove")
     */
    protected $member = null;

    /**
     * position
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\T3graf\Smartverein\Domain\Model\Functions>
     * @TYPO3\CMS\Extbase\Annotation\ORM\Cascade("remove")
     */
    protected $position = null;

    /**
     * __construct
     */
    public function __construct()
    {
        // Do not remove the next line: It would break the functionality
        $this->initializeObject();
    }

    /**
     * Initializes all ObjectStorage properties when model is reconstructed from DB (where __construct is not called)
     * Do not modify this method!
     * It will be rewritten on each save in the extension builder
     * You may modify the constructor of this class instead
     *
     * @return void
     */
    public function initializeObject()
    {
        $this->member = $this->member ?: new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $this->position = $this->position ?: new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
    }

    /**
     * Adds a Members
     *
     * @param \T3graf\Smartverein\Domain\Model\Members $member
     * @return void
     */
    public function addMember(\T3graf\Smartverein\Domain\Model\Members $member)
    {
        $this->member->attach($member);
    }

    /**
     * Removes a Members
     *
     * @param \T3graf\Smartverein\Domain\Model\Members $memberToRemove The Members to be removed
     * @return void
     */
    public function removeMember(\T3graf\Smartverein\Domain\Model\Members $memberToRemove)
    {
        $this->member->detach($memberToRemove);
    }

    /**
     * Returns the member
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\T3graf\Smartverein\Domain\Model\Members>
     */
    public function getMember()
    {
        return $this->member;
    }

    /**
     * Sets the member
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\T3graf\Smartverein\Domain\Model\Members> $member
     * @return void
     */
    public function setMember(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $member)
    {
        $this->member = $member;
    }

    /**
     * Adds a Functions
     *
     * @param \T3graf\Smartverein\Domain\Model\Functions $position
     * @return void
     */
    public function addPosition(\T3graf\Smartverein\Domain\Model\Functions $position)
    {
        $this->position->attach($position);
    }

    /**
     * Removes a Functions
     *
     * @param \T3graf\Smartverein\Domain\Model\Functions $positionToRemove The Functions to be removed
     * @return void
     */
    public function removePosition(\T3graf\Smartverein\Domain\Model\Functions $positionToRemove)
    {
        $this->position->detach($positionToRemove);
    }

    /**
     * Returns the position
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\T3graf\Smartverein\Domain\Model\Functions>
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Sets the position
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\T3graf\Smartverein\Domain\Model\Functions> $position
     * @return void
     */
    public function setPosition(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $position)
    {
        $this->position = $position;
    }
}
