<?php

/*
 * This file is part of the "SmartVerein - TYPO3 Club Management" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2023 Development-Team <development@t3graf-media.de>, T3graf media-agentur UG
 */

namespace T3graf\Smartverein\Hooks;

use T3graf\Smartverein\Utility\FeUserUtility;
use TYPO3\CMS\Core\DataHandling\DataHandler;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class FeUserDataHandlerHook
{
    /**
     * @return DataHandler
     */
    private function getDataHandler()
    {
        return GeneralUtility::makeInstance(DataHandler::class);
    }

    public function processDatamap_afterAllOperations(\TYPO3\CMS\Core\DataHandling\DataHandler &$pObj)
    {
        if (!array_key_exists('tx_smartverein_domain_model_members', $pObj->datamap)) {
            return;
        }

        if (str_starts_with(key($pObj->datamap['tx_smartverein_domain_model_members']), 'NEW')) {
            foreach ($pObj->datamap['tx_smartverein_domain_model_members'] as $uid => $propertyMap) {
                $feUserId = $propertyMap['fe_user'];
                if (!$feUserId) {
                    //debug($uid,'create1');
                    FeUserUtility::createFeUser($pObj->substNEWwithIDs[$uid]);
                }
            }
        } else {
            foreach ($pObj->datamap['tx_smartverein_domain_model_members'] as $uid => $propertyMap) {
                $feUserId = $propertyMap['fe_user'];
                if (!$feUserId) {
                    //debug($uid,'create2');
                    //FeUserUtility::createFeUser($uid);
                } else {
                    FeUserUtility::updateFeUserData($uid);
                }
            }
        }
    }
}
