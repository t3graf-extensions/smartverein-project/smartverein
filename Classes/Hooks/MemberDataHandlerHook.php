<?php

/*
 * This file is part of the "SmartVerein - TYPO3 Club Management" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2023 Development-Team <development@t3graf-media.de>, T3graf media-agentur UG
 */

namespace T3graf\Smartverein\Hooks;

use T3graf\Smartverein\Utility\ChangelogUtility;
use T3graf\Smartverein\Utility\MemberUtility;
use T3graf\Smartverein\Utility\SmartvereinUtility;
use TYPO3\CMS\Core\DataHandling\DataHandler;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class MemberDataHandlerHook
{
    /**
     * @return DataHandler
     */
    private function getDataHandler()
    {
        return GeneralUtility::makeInstance(DataHandler::class);
    }

    public function processDatamap_preProcessFieldArray(&$fieldArray, $table, $id, \TYPO3\CMS\Core\DataHandling\DataHandler &$pObj)
    {
        if ($table !== 'tx_smartverein_domain_model_members' || $table !== 'tx_smartverein_domain_model_vitaentries') {
            return;
        }

        // Do nothing
    }

    public function processDatamap_postProcessFieldArray($status, $table, $id, &$fieldArray, \TYPO3\CMS\Core\DataHandling\DataHandler &$pObj)
    {
        if ($table !== 'tx_smartverein_domain_model_members') {
            return;
        }
    }

    /**
     * @throws \Doctrine\DBAL\DBALException
     */
    public function processDatamap_afterAllOperations(\TYPO3\CMS\Core\DataHandling\DataHandler &$pObj)
    {
        if (!array_key_exists('tx_smartverein_domain_model_members', $pObj->datamap)) {
            return;
        }
        $settings = SmartvereinUtility::getTSSettings('smartverein');

        if (str_starts_with(key($pObj->datamap['tx_smartverein_domain_model_members']), 'NEW') && $settings['members']['autoMembershipNumber']) {
            foreach ($pObj->datamap['tx_smartverein_domain_model_members'] as $uid => $propertyMap) {
                if ($propertyMap['member_id'] === '') {
                    MemberUtility::setMemberID($pObj->substNEWwithIDs[$uid]);
                    MemberUtility::setMemberFolderName($pObj->substNEWwithIDs[$uid]);
                }
            }
        }

        $entries = $pObj->getHistoryRecords();
        if (!str_starts_with(key($pObj->datamap['tx_smartverein_domain_model_members']), 'NEW')) {
            foreach ($pObj->datamap['tx_smartverein_domain_model_members'] as $uid => $propertyMap) {
                ChangelogUtility::createChangelogEntries($uid, $entries, $propertyMap);
                MemberUtility::setMemberFolderName($uid);
            }
        } else {
            foreach ($pObj->datamap['tx_smartverein_domain_model_members'] as $uid => $propertyMap) {
                $newUid = $pObj->substNEWwithIDs[$uid];
                ChangelogUtility::newMemberChangelogEntry($newUid, $entries, $propertyMap);
                MemberUtility::setMemberFolderName($newUid);
            }
        }
        //die();
    }
}
