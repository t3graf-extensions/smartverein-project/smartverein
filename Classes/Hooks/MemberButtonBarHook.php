<?php

/*
 * This file is part of the "SmartVerein - TYPO3 Club Management" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2023 Development-Team <development@t3graf-media.de>, T3graf media-agentur UG
 */

namespace T3graf\Smartverein\Hooks;

use T3graf\Smartverein\Utility\MemberUtility;
use TYPO3\CMS\Backend\Routing\UriBuilder;
use TYPO3\CMS\Backend\Template\Components\ButtonBar;
use TYPO3\CMS\Core\Imaging\Icon;
use TYPO3\CMS\Core\Imaging\IconFactory;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class MemberButtonBarHook
{
    /**
     * Get buttons
     *
     * @param array $params
     * @param ButtonBar $buttonBar
     *
     * @return array
     */
    public function getButtons(array $params, ButtonBar $buttonBar)
    {
        $buttons = $params['buttons'];
        //check if edit record -> Show button only on edit
        if (!is_null(GeneralUtility::_GET('edit'))) {
            //check if book is set -> Show only on book edit
            $editArray = GeneralUtility::_GET('edit');
            //debug($editArray);die();
            if (in_array('edit', $editArray['tx_smartverein_domain_model_members'])) {
                //register button
                $iconFactory = GeneralUtility::makeInstance(IconFactory::class);
                $button = $buttonBar->makeLinkButton();
                $button->setIcon($iconFactory->getIcon(
                    'actions-envelope',
                    Icon::SIZE_SMALL
                ));
                $button->setTitle(\TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate('labels.button.writeMemberEmail', 'smartverein'));

                $button->setShowLabelText(true);
                $uid = array_shift(array_keys($editArray['tx_smartverein_domain_model_members']));
                //create link/route and set member uid for member controller function
                /*
               // $uriBuilder = GeneralUtility::makeInstance(UriBuilder::class);
               // $uri = $uriBuilder->buildUriFromRoute(
                    'mowareupdate',
                    ['member' => $uid]
                );
                $button->setHref($uri);
                */
                //debug($uid);
                //debug(MemberUtility::getMemberEmail($uid));die();
                $button->setHref('mailto:' . MemberUtility::getMemberEmail($uid));
                //register button
                $buttons[ButtonBar::BUTTON_POSITION_LEFT][5][] = $button;
            }
        }
        return $buttons;
    }
}
