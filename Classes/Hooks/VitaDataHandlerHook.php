<?php

/*
 * This file is part of the "SmartVerein - TYPO3 Club Management" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2023 Development-Team <development@t3graf-media.de>, T3graf media-agentur UG
 */

namespace T3graf\Smartverein\Hooks;

use T3graf\Smartverein\Utility\MemberUtility;
use T3graf\Smartverein\Utility\SmartvereinUtility;
use TYPO3\CMS\Core\DataHandling\DataHandler;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class VitaDataHandlerHook
{
    /**
     * @return DataHandler
     */
    private function getDataHandler()
    {
        return GeneralUtility::makeInstance(DataHandler::class);
    }
/*
    public function processDatamap_preProcessFieldArray(&$fieldArray, $table, $id, \TYPO3\CMS\Core\DataHandling\DataHandler &$pObj)
    {
        if ($table !== 'tx_smartverein_domain_model_vitaentries') {
            return;
        }
        foreach ($pObj->datamap['tx_smartverein_domain_model_vitaentries'] as $uid => $propertyMap) {
            if (str_starts_with($uid, 'NEW')) {
                $propertyMap['entered_by'] = SmartvereinUtility::getBeUsername();
            }

        }
    }

*/
    /**
     * @throws \Doctrine\DBAL\DBALException
     */
    public function processDatamap_afterAllOperations(\TYPO3\CMS\Core\DataHandling\DataHandler &$pObj)
    {
        if (!array_key_exists('tx_smartverein_domain_model_vitaentries', $pObj->datamap)) {
            return;
        }

        if (str_starts_with(key($pObj->datamap['tx_smartverein_domain_model_vitaentries']), 'NEW')) {
            foreach ($pObj->datamap['tx_smartverein_domain_model_vitaentries'] as $uid => $propertyMap) {
                if (str_starts_with($uid, 'NEW')) {
                    $newUid = $pObj->substNEWwithIDs[$uid];
                    // Mitglied wird neu angelegt mit vita einträgen
                    $propertyMap['entered_by'] = SmartvereinUtility::getBeUsername();
                }
                // Mitglied ist neu und vita eintrag auch
                memberUtility::setVitaEntryAuthor($pObj->substNEWwithIDs[$uid]);
            }
        } else {
            foreach ($pObj->datamap['tx_smartverein_domain_model_vitaentries'] as $uid => $propertyMap) {
                if (str_starts_with($uid, 'NEW')) {
                    $uid = $pObj->substNEWwithIDs[$uid];
                }
                // Mitglied existiert und vita wird erweiter
                memberUtility::setVitaEntryAuthor($uid);
            }
        }
        //die();
    }
}
