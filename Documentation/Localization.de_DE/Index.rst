.. every .rst file should include Includes.txt
.. use correct path!

.. include:: Includes.txt

.. Every manual should have a start label for cross-referencing to
.. start page. Do not remove this!

.. _start:

=============================================================
SmartVerein - TYPO3 Vereinsverwaltung
=============================================================
:Extension-Key:
    smartverein

:Package-Name:
    t3graf/smartverein

:Version:
   |release|

:Sprachen:
   en, de

:Autor:
   T3graf media-agentur

:E-Mail:
   development@t3graf-media.de

:Lizenz:
   Dieses Dokument ist veröffentlicht unter der
   `CC BY-NC-SA 4.0 <https://creativecommons.org/licenses/by-nc-sa/4.0/>`__ (Creative Commons)
   Lizenz
:Erstellt:
    |today|

:Schlüsselworte:
    member, management, organization, association, federation, club, t3graf

----

Diese Erweiterung integriert ein vielseitiges Vereins- und Verbandsverwaltungssystem in TYPO3. Es ist modular aufgebaut, sodass Sie es je nach Bedarf erweitern können. Es basiert auf Extbase & Fluid und nutzt die neuesten Technologien von TYPO3 CMS.

----

.. container:: row m-0 p-0

   .. container:: col-12 col-md-6 pl-0 pr-3 py-3 m-0

      .. container:: card px-0 h-100

         .. rst-class:: card-header h3

            .. rubric:: :ref:`Schnellstart <quickStart>`

         .. container:: card-body

            Eine schnelle Einführung in die Verwendung dieser Erweiterung.

   .. container:: col-12 col-md-6 pl-0 pr-3 py-3 m-0

      .. container:: card px-0 h-100

         .. rst-class:: card-header h3

            .. rubric:: :ref:`Einleitung <introduction>`

         .. container:: card-body

            Einführung in die Erweiterung smartverein, allgemeine Informationen.

   .. container:: col-12 col-md-6 pl-0 pr-3 py-3 m-0

      .. container:: card px-0 h-100

         .. rst-class:: card-header h3

            .. rubric:: :ref:`Editors manual <userManual>`

         .. container:: card-body

            Erfahren Sie, wie Sie die SmartVerein Module nutzen, die Plugins konfigurieren und wie man SmartVerein Datensätze erstellt.

   .. container:: col-12 col-md-6 pl-0 pr-3 py-3 m-0

      .. container:: card px-0 h-100

         .. rst-class:: card-header h3

            .. rubric:: :ref:`Tutorials <tutorials>`

         .. container:: card-body

            Tutorials und Snippets für viele häufige Anwendungsfälle.

   .. container:: col-12 col-md-6 pl-0 pr-3 py-3 m-0

      .. container:: card px-0 h-100

         .. rst-class:: card-header h3

            .. rubric:: :ref:`Administration <admin-manual>`

         .. container:: card-body

            Installieren oder aktualisieren Sie EXT:smartverein.

   .. container:: col-12 col-md-6 pl-0 pr-3 py-3 m-0

      .. container:: card px-0 h-100

         .. rst-class:: card-header h3

            .. rubric:: :ref:`Referenzen <reference>`

         .. container:: card-body

            Ausführliche Referenz zu bestimmten Aspekten dieser Erweiterung:
            TypoScript, TSconfig, ViewHelpers usw

   .. container:: col-12 col-md-6 pl-0 pr-3 py-3 m-0

      .. container:: card px-0 h-100

         .. rst-class:: card-header h3

            .. rubric:: :ref:`Support <support>`

         .. container:: card-body

            There are various ways to get support for EXT:smartverein!

   .. container:: col-12 col-md-6 pl-0 pr-3 py-3 m-0

      .. container:: card px-0 h-100

         .. rst-class:: card-header h3

            .. rubric:: :ref:`Addons <addons>`

         .. container:: card-body

            Verschiedene Erweiterungen, erweitern EXT:smartverein um zusätzliche Funktionen.
.. Table of Contents

.. toctree::
   :maxdepth: 2
   :titlesonly:
   :hidden:

   Introduction/Index
   QuickStart/Index
   UsersManual/Index
   Tutorials/Index
   Administration/Index
   Reference/Index
   Addons/Index

.. Meta Menu

.. toctree::
   :hidden:

   Sitemap
   genindex
