.. include:: /Includes.rst.txt

.. _events:

=========================
SmartVerein Veranstaltung
=========================

Ein separates Backend-Modul zur Pflege von Veranstaltungen im Backend.

Preisgestaltung
-------

- 150,00 € (zzgl. MwSt.) für 1 Website
- 450,00 € (zzgl. MwSt.) für unbegrenzte Websites im Zusammenhang mit der Agentur selbst oder ihren Kunden

**Enthalten**: Bugfix und Feature-Releases, Composer-Unterstützung

Kontakt: Bitte schreiben Sie eine E-Mail an extension@t3graf-media.de mit der von Ihnen benötigten Version und Ihrer Rechnungsadresse + Umsatzsteuer-Identifikationsnummer.

Funktionen
----------

- Ergänzt das Kalender Modul
- Buchungssystem für Veranstaltungen
- Einstellung von Preisen und Rabatten
- Einrichtung von Mindest- und Maximal-Teilnehmern
- Abrechnung mit Rechnungs Modul möglich

Record rendering
^^^^^^^^^^^^^^^^
The rendering of the articles can be configured with various options

- Disable the page tree for the whole module and define a default page. This is the fasted way for editors to work with news records.
- Define the fields which are shown

.. include:: /Images/Addons/NewsAdministration/records.rst.txt

Powerful filters
^^^^^^^^^^^^^^^^
Working with lots of records is getting easy when using filter to find the records fast.

- Full textsearch
- Filter by time range of, categories and all other most used attributes

.. include:: /Images/Addons/NewsAdministration/filter.rst.txt

Page listing
^^^^^^^^^^^^
Sometimes got a hard time to find the page on which news, categories or tags are saved?
This module will help you find those fast and easy.

.. include:: /Images/Addons/NewsAdministration/pid-listing.rst.txt
