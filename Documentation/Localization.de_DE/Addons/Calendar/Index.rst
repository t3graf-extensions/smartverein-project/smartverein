.. include:: /Includes.rst.txt



.. _calendar:

=======================
SmartVerein Kalender
=======================

Funktionen
----------

- Terminplanung
- Terminvereinbarung für Mitglieder
- Automatische Erinnerung der Mitglieder an bevorstehende Veranstaltungen
- Anmelde- und Abmeldefunktion für Termine

.. tip::
    Schauen Sie sich die „SmartVerein Module <https://quicko.software/en/modules/>“ an.
     Weitere Informationen zu allen SmartVerein Modulen finden Sie hier.

.. note::
      Fordern Sie noch heute ein unverbindliches Angebot an und stellen Sie Ihren Verein nachhaltig auf
      Fundament. „SmartVerein – TYPO3 Club Management“ ist Ihr zuverlässiger Partner.
    `Anfrage jetzt senden  <https://quicko.software/en/contact-us/>`__
