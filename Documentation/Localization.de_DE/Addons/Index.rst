.. include:: /Includes.rst.txt

.. _addons:

Addons
======

This sections describes extensions which extend EXT:smartverein with additional features.

.. toctree::
   :maxdepth: 3
   :titlesonly:
   :glob:

   Calendar/Index
   Events/Index
   Meetings/Index
   Tasks/Index
   Inventory/Index
   DownloadManager/Index
   Billing/Index

