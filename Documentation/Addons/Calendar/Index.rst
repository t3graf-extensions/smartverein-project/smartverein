.. include:: /Includes.rst.txt



.. _calendar:

=======================
SmartVerein Calendar
=======================

More information is coming soon
-------------------------------

- Appointment scheduling
- Booking appointments for members
- More to come

.. tip::
    Have a look at `SmartVerein Modules <https://quicko.software/en/modules/>`__
    for more information about all clubmanager modules.

.. note::
    Request a non-binding offer today and put your association on a sustainable
    footing. `SmartVerein - TYPO3 Club Management` is your reliable partner.
    `Submit request now <https://quicko.software/en/contact-us/>`__
