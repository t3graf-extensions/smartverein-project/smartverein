.. every .rst file should include Includes.txt
.. use correct path!

.. include:: Includes.txt

.. Every manual should have a start label for cross-referencing to
.. start page. Do not remove this!

.. _start:

=============================================================
SmartVerein - TYPO3 Club Management
=============================================================
:Extension key:
    smartverein

:Package name:
    t3graf/smartverein

:Version:
   |release|

:Language:
   en

:Authors:
   T3graf media-agentur

:Email:
   development@t3graf-media.de

:License:
   This extension documentation is published under the
   `CC BY-NC-SA 4.0 <https://creativecommons.org/licenses/by-nc-sa/4.0/>`__ (Creative Commons)
   license
:Rendered:
    |today|

:Keywords:
    member, management, organization, association, federation, club, t3graf

----

This extension integrates a versatile club and association management system in TYPO3. It is modular so you can expand it according your needs. It based on Extbase & Fluid and uses the latest technologies from TYPO3 CMS.

----

.. container:: row m-0 p-0

   .. container:: col-12 col-md-6 pl-0 pr-3 py-3 m-0

      .. container:: card px-0 h-100

         .. rst-class:: card-header h3

            .. rubric:: :ref:`Quick start <quickStart>`

         .. container:: card-body

            A quick introduction in how to use this extension.

   .. container:: col-12 col-md-6 pl-0 pr-3 py-3 m-0

      .. container:: card px-0 h-100

         .. rst-class:: card-header h3

            .. rubric:: :ref:`Introduction <introduction>`

         .. container:: card-body

            Introduction to the extension smartverein, general information.

   .. container:: col-12 col-md-6 pl-0 pr-3 py-3 m-0

      .. container:: card px-0 h-100

         .. rst-class:: card-header h3

            .. rubric:: :ref:`Editors manual <userManual>`

         .. container:: card-body

            Learn how to use the smartverein module, how to configure
            the plugins and how to create member records.

   .. container:: col-12 col-md-6 pl-0 pr-3 py-3 m-0

      .. container:: card px-0 h-100

         .. rst-class:: card-header h3

            .. rubric:: :ref:`Tutorials <tutorials>`

         .. container:: card-body

            Tutorials and snippets for many frequent use cases.

   .. container:: col-12 col-md-6 pl-0 pr-3 py-3 m-0

      .. container:: card px-0 h-100

         .. rst-class:: card-header h3

            .. rubric:: :ref:`Administration <admin-manual>`

         .. container:: card-body

            Install or upgrade EXT:smartclub.

   .. container:: col-12 col-md-6 pl-0 pr-3 py-3 m-0

      .. container:: card px-0 h-100

         .. rst-class:: card-header h3

            .. rubric:: :ref:`Reference <reference>`

         .. container:: card-body

            In-depth reference about certain aspects of this extension:
            TypoScript, TSconfig, ViewHelpers etc

   .. container:: col-12 col-md-6 pl-0 pr-3 py-3 m-0

      .. container:: card px-0 h-100

         .. rst-class:: card-header h3

            .. rubric:: :ref:`Support <support>`

         .. container:: card-body

            There are various ways to get support for EXT:smartverein!

   .. container:: col-12 col-md-6 pl-0 pr-3 py-3 m-0

      .. container:: card px-0 h-100

         .. rst-class:: card-header h3

            .. rubric:: :ref:`Addons <addons>`

         .. container:: card-body

            Various extensions improve EXT:smartverein with additional features.
.. Table of Contents

.. toctree::
   :maxdepth: 2
   :titlesonly:
   :hidden:

   Introduction/Index
   QuickStart/Index
   UsersManual/Index
   Tutorials/Index
   Administration/Index
   Reference/Index
   Addons/Index

.. Meta Menu

.. toctree::
   :hidden:

   Sitemap
   genindex
