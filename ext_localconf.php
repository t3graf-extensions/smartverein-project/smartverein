<?php

/*
 * This file is part of the "SmartVerein - TYPO3 Club Management" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2023 Development-Team <development@t3graf-media.de>, T3graf media-agentur UG
 */

use TYPO3\CMS\Core\Configuration\ExtensionConfiguration;
use TYPO3\CMS\Core\Information\Typo3Version;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;

defined('TYPO3') || die();

(static function () {
    $versionInformation = GeneralUtility::makeInstance(Typo3Version::class);
    // Only include page.tsconfig if TYPO3 version is below 12 so that it is not imported twice.
    if ($versionInformation->getMajorVersion() < 12) {
        ExtensionManagementUtility::addPageTSConfig(
            '@import "EXT:smartverein/Configuration/page.tsconfig"'
        );
    }
    // Add SmartVerein default folders tp Page TS
    ExtensionManagementUtility::addPageTSConfig(
        'default_upload_folders {
  # folder can be a combined identifier
  tx_smartverein_domain_model_members = 1:smartverein/' . \T3graf\Smartverein\Utility\SmartvereinUtility::getExtConfProperty('smartverein', 'folders.memberDocuments') . '
  # Or a folder relative to the default upload folder of the user
  #tx_smartverein_domain_model_members = news

  # You can set a folder for the whole table of for a specific field of that table
  tx_smartverein_domain_model_members.profile_image = 1:smartverein/' . \T3graf\Smartverein\Utility\SmartvereinUtility::getExtConfProperty('smartverein', 'folders.memberDocuments') . '/
  tx_smartverein_domain_model_members.membership_application = 1:smartverein/' . \T3graf\Smartverein\Utility\SmartvereinUtility::getExtConfProperty('smartverein', 'folders.memberDocuments') . '/
  tx_smartverein_domain_model_members.leaving_statement = 1:smartverein/' . \T3graf\Smartverein\Utility\SmartvereinUtility::getExtConfProperty('smartverein', 'folders.memberDocuments') . '/
  tx_smartverein_domain_model_members.declaration_of_consent = 1:smartverein/' . \T3graf\Smartverein\Utility\SmartvereinUtility::getExtConfProperty('smartverein', 'folders.memberDocuments') . '/
  #tx_smartverein_domain_model_members.vita = 1:smartverein/' . \T3graf\Smartverein\Utility\SmartvereinUtility::getExtConfProperty('smartverein', 'folders.memberDocuments') . '/
  tx_smartverein_domain_model_members.sepa_mandate = 1:smartverein/' . \T3graf\Smartverein\Utility\SmartvereinUtility::getExtConfProperty('smartverein', 'folders.memberDocuments') . '/

  # You can set a fallback for all tables
  defaultForAllTables = 1:smartverein/smartverein_upload

  # You can set a default year/month/day folder within the set default folder
  #tx_smartverein_domain_model_members.dateformat = 1
  #tx_smartverein_domain_model_members = 1:smartverein/' . \T3graf\Smartverein\Utility\SmartvereinUtility::getExtConfProperty('smartverein', 'folders.memberDocuments') . '/{Y}/{n}/{d}
}'
    );

    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
        'Smartverein',
        'Memberlist',
        [
            \T3graf\Smartverein\Controller\MembersController::class => 'list'
        ],
        // non-cacheable actions
        [
            \T3graf\Smartverein\Controller\MembersController::class => '',
            \T3graf\Smartverein\Controller\ContactsController::class => '',
            \T3graf\Smartverein\Controller\AddressbookController::class => '',
            \T3graf\Smartverein\Controller\SettingsController::class => '',
            \T3graf\Smartverein\Controller\MailQueueController::class => ''
        ]
    );

    // wizards
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
        'mod {
            wizards.newContentElement.wizardItems.plugins {
                elements {
                    memberlist {
                        iconIdentifier = smartverein-plugin-memberlist
                        title = LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_memberlist.name
                        description = LLL:EXT:smartverein/Resources/Private/Language/locallang_db.xlf:tx_smartverein_memberlist.description
                        tt_content_defValues {
                            CType = list
                            list_type = smartverein_memberlist
                        }
                    }
                }
                show = *
            }
       }'
    );

    // register hooks
    $GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['t3lib/class.t3lib_tcemain.php']['processDatamapClass']['smartverein_member_datahandler_hook'] = \T3graf\Smartverein\Hooks\MemberDataHandlerHook::class;
    $GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['t3lib/class.t3lib_tcemain.php']['processDatamapClass']['smartverein_feuser_datahandler_hook'] = \T3graf\Smartverein\Hooks\FeUserDataHandlerHook::class;
    $GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['t3lib/class.t3lib_tcemain.php']['processDatamapClass']['smartverein_vita_datahandler_hook'] = \T3graf\Smartverein\Hooks\VitaDataHandlerHook::class;
    $GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['t3lib/class.t3lib_userauthgroup.php']['getDefaultUploadFolder'][] =
        \T3graf\Smartverein\Hooks\DefaultUploadFolder::class . '->getDefaultUploadFolder';
    $GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['Backend\Template\Components\ButtonBar']['getButtonsHook']['smartverein'] = \T3graf\Smartverein\Hooks\MemberButtonBarHook::class . '->getButtons';

    $extConf = GeneralUtility::makeInstance(ExtensionConfiguration::class);
    $fe_users_storagePid = $extConf->get(
        'smartverein',
        'settings/memberStoragePid'
    );

    ExtensionManagementUtility::addPageTSConfig(
        'TCAdefaults.fe_users.pid = ' . $fe_users_storagePid
    );

    $settings = $extConf->get('smartverein');

    foreach ($settings['email'] as $key => $value) {
        ExtensionManagementUtility::addTypoScriptConstants(
            "smartverein.email.$key =" . $value
        );
    }

    foreach ($settings['settings'] as $key => $value) {
        ExtensionManagementUtility::addTypoScriptConstants(
            "smartverein.settings.$key =" . $value
        );
    }
})();
