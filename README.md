# TYPO3 Extension: ``SmartVerein - TYPO3 Club Management``

[![TYPO3 compatibility](https://img.shields.io/static/v1?label=TYPO3&message=v11%20%7C%20v12&color=f49800&&logo=typo3&logoColor=f49800)](https://typo3.org)
[![Packagist PHP Version Support (specify version)](https://img.shields.io/packagist/php-v/t3graf/smartverein/dev-main?color=purple&logo=php&logoColor=white)](https://packagist.org/packages/t3graf/smartverein)
[![Latest Stable Version](https://img.shields.io/packagist/v/t3graf/smartverein?label=stable&color=33a2d8&logo=packagist&logoColor=white)](https://packagist.org/packages/t3graf/smartverein)
[![Total Downloads](https://img.shields.io/packagist/dt/t3graf/smartverein?color=1081c1&logo=packagist&logoColor=white)](https://packagist.org/packages/t3graf/smartverein)
[![License](https://badgen.net/packagist/license/t3graf/smartverein)](https://gitlab.com/t3graf-extensions/smartverein-project/smartverein/-/blob/main/LICENSE.md)
[![Gitlab pipeline status](https://img.shields.io/gitlab/pipeline-status/t3graf-extensions/smartverein-project/smartverein?branch=main&label=CI%2FCD&logo=gitlab)](https://gitlab.com/t3graf-extensions/smartverein-project/smartverein)

> Basic extension of the smartClub project. This extension implements a versatile club and association management system based on Extbase & Fluid and uses the latest technologies from TYPO3 CMS. It is modular so you can expand it according your needs.

### Features

- Backend dashboard widgets
- Member management
- Address book for partners and sponsors of the club
- FE-User Member Dashboard



## Installation

### Installation using composer

The recommended way to install the extension is by using [Composer](https://getcomposer.org).

`composer require t3graf/smartverein`.

### Installation from TYPO3 Extension Repository (TER)

Download and install the extension with the extension manager module.

## Minimal setup

1) Include the static TypoScript of the extension.

## Which smartverein for which TYPO3 and PHP?

| smartverein | TYPO3     | PHP       | Support/Development                                             |
|-----------|-----------|-----------|-----------------------------------------------------------------|
| 2.x       | 12.4       | 8.1 - 8.2 | Features, Bugfixes, Security Updates  |
| 1.x       | 11.5 | 8.0 - 8.2 | Features, Bugfixes, Security Updates  |
<!--## 4. Administration

### Create content element

## 5. Configuration

### Extension settings

### Constants
-->
## Contribute

Please create an issue at [gitlab.com/t3graf-extensions/smartverein-project/smartverein/issues](https://gitlab.com/t3graf-extensions/smartverein-project/smartverein/issues).

**Please use Gitlab only for bug-reports or feature-requests.**

## Credits

This extension was created by Mike Tölle in 2021 for [T3graf media-agentur, Recklinghausen](https://www.t3graf-media.de).

Find examples, use cases and best practices for this extension in our [smartverein-project/smartverein blog series on t3graf.de](https://www.t3graf-media.de/blog/).

[Find more TYPO3 extensions we have developed](https://www.t3graf-media.de/) that help us deliver value in client projects. As part of the way we work, we focus on testing and best practices to ensure long-term performance, reliability, and results in all our code.


## Links

- **Demo:** [www.t3graf-media.de/typo3-extensions/smartverein](https://www.t3graf-media.de/typo3-extensions/smartverein)
- **Gitlab Repository:** [gitlab.com/t3graf-extensions/smartverein-project/smartverein](https://gitlab.com/t3graf-extensions/smartverein-project/smartverein)
- **TYPO3 Extension Repository:** [extensions.typo3.org/](https://extensions.typo3.org/)
- **Found an issue?:** [gitlab.com/t3graf-extensions/smartverein-project/smartverein/issues](https://gitlab.com/t3graf-extensions/smartverein-project/smartverein/issues)
