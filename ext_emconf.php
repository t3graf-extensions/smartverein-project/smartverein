<?php

/*
 * This file is part of the "SmartVerein - TYPO3 Club Management" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2023 Development-Team <development@t3graf-media.de>, T3graf media-agentur UG
 */

$EM_CONF[$_EXTKEY] = [
    'title' => 'SmartVerein - TYPO3 Club Management',
    'description' => 'This extension integrates a versatile club and association management system in TYPO3. It is modular so you can expand it according your needs. It based on Extbase & Fluid and uses the latest technologies from TYPO3 CMS.See the documentation for more information.',
    'category' => 'plugin',
    'author' => 'Development-Team',
    'author_email' => 'development@t3graf-media.de',
    'author_company' => 'T3graf media-agentur',
    'state' => 'alpha',
    'clearCacheOnLoad' => 0,
    'version' => '1.0.0',
    'constraints' => [
        'depends' => [
            'typo3' => '11.5.0-11.5.99',
            'static_info_tables' => '11.5.0-11.5.99',
            'scheduler' => '11.5.0-11.5.99',
        ],
        'conflicts' => [],
        'suggests' => [],
    ],
];
