<?php

/*
 * This file is part of the "SmartVerein - TYPO3 Club Management" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2023 Development-Team <development@t3graf-media.de>, T3graf media-agentur UG
 */

use TYPO3\CMS\Extbase\Utility\ExtensionUtility;

defined('TYPO3') || die();

(static function () {
    ExtensionUtility::registerModule(
        'Smartverein',
        'svTools',
        '',
        'after:web',
        [
        ],
        [
            'access' => 'user,group',
            'icon' => 'EXT:smartverein/Resources/Public/Icons/modulegroup_smartverein.svg',
            'labels' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_smartverein.xlf',

        ]
    );
    ExtensionUtility::registerModule(
        'Smartverein',
        'svTools',
        'members',
        'top',
        [
            \T3graf\Smartverein\Controller\MembersController::class => 'beListMembers, beNewMember, beEditMember, beDeleteMember, beUpdateMember, beCreateNewMember, beShowMemberDetails, beCreateMissingMemberIds, beDownloadVCards, beDownloadCsv',

        ],
        [
            'access' => 'user,group',
            'icon'   => 'EXT:smartverein/Resources/Public/Icons/module_members.svg',
            'labels' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_members.xlf',
            'path' => 'smartverein/members',
        ]
    );
    ExtensionUtility::registerModule(
        'Smartverein',
        'svTools',
        'addresses',
        'after:members',
        [
            \T3graf\Smartverein\Controller\AddressbookController::class => 'beListAddresses',

        ],
        [
            'access' => 'user,group',
            'icon'   => 'EXT:smartverein/Resources/Public/Icons/module_addressbook.svg',
            'labels' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_addresses.xlf',
            'path' => 'smartverein/addressbook',
        ]
    );
    ExtensionUtility::registerModule(
        'Smartverein',
        'svTools',
        'settings',
        'bottom',
        [
            \T3graf\Smartverein\Controller\SettingsController::class => 'index',

        ],
        [
            'access' => 'user,group',
            'icon'   => 'EXT:smartverein/Resources/Public/Icons/module_settings.svg',
            'labels' => 'LLL:EXT:smartverein/Resources/Private/Language/locallang_settings.xlf',
            'path' => 'smartverein/settings',
        ]
    );
})();
