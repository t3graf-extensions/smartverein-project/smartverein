<?php

declare(strict_types=1);

/*
 * This file is part of the "SmartVerein - TYPO3 Club Management" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2023 Development-Team <development@t3graf-media.de>, T3graf media-agentur UG
 */

namespace T3graf\Smartverein\Tests\Unit\Domain\Model;

use PHPUnit\Framework\MockObject\MockObject;
use TYPO3\TestingFramework\Core\AccessibleObjectInterface;
use TYPO3\TestingFramework\Core\Unit\UnitTestCase;

/**
 * Test case
 *
 */
class SectionPositionsTest extends UnitTestCase
{
    /**
     * @var \T3graf\Smartverein\Domain\Model\SectionPositions|MockObject|AccessibleObjectInterface
     */
    protected $subject;

    protected function setUp(): void
    {
        parent::setUp();

        $this->subject = $this->getAccessibleMock(
            \T3graf\Smartverein\Domain\Model\SectionPositions::class,
            ['dummy']
        );
    }

    protected function tearDown(): void
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function getMemberReturnsInitialValueForMembers(): void
    {
        $newObjectStorage = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        self::assertEquals(
            $newObjectStorage,
            $this->subject->getMember()
        );
    }

    /**
     * @test
     */
    public function setMemberForObjectStorageContainingMembersSetsMember(): void
    {
        $member = new \T3graf\Smartverein\Domain\Model\Members();
        $objectStorageHoldingExactlyOneMember = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $objectStorageHoldingExactlyOneMember->attach($member);
        $this->subject->setMember($objectStorageHoldingExactlyOneMember);

        self::assertEquals($objectStorageHoldingExactlyOneMember, $this->subject->_get('member'));
    }

    /**
     * @test
     */
    public function addMemberToObjectStorageHoldingMember(): void
    {
        $member = new \T3graf\Smartverein\Domain\Model\Members();
        $memberObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->onlyMethods(['attach'])
            ->disableOriginalConstructor()
            ->getMock();

        $memberObjectStorageMock->expects(self::once())->method('attach')->with(self::equalTo($member));
        $this->subject->_set('member', $memberObjectStorageMock);

        $this->subject->addMember($member);
    }

    /**
     * @test
     */
    public function removeMemberFromObjectStorageHoldingMember(): void
    {
        $member = new \T3graf\Smartverein\Domain\Model\Members();
        $memberObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->onlyMethods(['detach'])
            ->disableOriginalConstructor()
            ->getMock();

        $memberObjectStorageMock->expects(self::once())->method('detach')->with(self::equalTo($member));
        $this->subject->_set('member', $memberObjectStorageMock);

        $this->subject->removeMember($member);
    }

    /**
     * @test
     */
    public function getPositionReturnsInitialValueForFunctions(): void
    {
        $newObjectStorage = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        self::assertEquals(
            $newObjectStorage,
            $this->subject->getPosition()
        );
    }

    /**
     * @test
     */
    public function setPositionForObjectStorageContainingFunctionsSetsPosition(): void
    {
        $position = new \T3graf\Smartverein\Domain\Model\Functions();
        $objectStorageHoldingExactlyOnePosition = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $objectStorageHoldingExactlyOnePosition->attach($position);
        $this->subject->setPosition($objectStorageHoldingExactlyOnePosition);

        self::assertEquals($objectStorageHoldingExactlyOnePosition, $this->subject->_get('position'));
    }

    /**
     * @test
     */
    public function addPositionToObjectStorageHoldingPosition(): void
    {
        $position = new \T3graf\Smartverein\Domain\Model\Functions();
        $positionObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->onlyMethods(['attach'])
            ->disableOriginalConstructor()
            ->getMock();

        $positionObjectStorageMock->expects(self::once())->method('attach')->with(self::equalTo($position));
        $this->subject->_set('position', $positionObjectStorageMock);

        $this->subject->addPosition($position);
    }

    /**
     * @test
     */
    public function removePositionFromObjectStorageHoldingPosition(): void
    {
        $position = new \T3graf\Smartverein\Domain\Model\Functions();
        $positionObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->onlyMethods(['detach'])
            ->disableOriginalConstructor()
            ->getMock();

        $positionObjectStorageMock->expects(self::once())->method('detach')->with(self::equalTo($position));
        $this->subject->_set('position', $positionObjectStorageMock);

        $this->subject->removePosition($position);
    }
}
