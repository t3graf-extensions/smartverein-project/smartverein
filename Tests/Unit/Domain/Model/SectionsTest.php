<?php

declare(strict_types=1);

/*
 * This file is part of the "SmartVerein - TYPO3 Club Management" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2023 Development-Team <development@t3graf-media.de>, T3graf media-agentur UG
 */

namespace T3graf\Smartverein\Tests\Unit\Domain\Model;

use PHPUnit\Framework\MockObject\MockObject;
use TYPO3\TestingFramework\Core\AccessibleObjectInterface;
use TYPO3\TestingFramework\Core\Unit\UnitTestCase;

/**
 * Test case
 *
 */
class SectionsTest extends UnitTestCase
{
    /**
     * @var \T3graf\Smartverein\Domain\Model\Sections|MockObject|AccessibleObjectInterface
     */
    protected $subject;

    protected function setUp(): void
    {
        parent::setUp();

        $this->subject = $this->getAccessibleMock(
            \T3graf\Smartverein\Domain\Model\Sections::class,
            ['dummy']
        );
    }

    protected function tearDown(): void
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function getTitleReturnsInitialValueForString(): void
    {
        self::assertSame(
            '',
            $this->subject->getTitle()
        );
    }

    /**
     * @test
     */
    public function setTitleForStringSetsTitle(): void
    {
        $this->subject->setTitle('Conceived at T3CON10');

        self::assertEquals('Conceived at T3CON10', $this->subject->_get('title'));
    }

    /**
     * @test
     */
    public function getStaffReturnsInitialValueForSectionPositions(): void
    {
        $newObjectStorage = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        self::assertEquals(
            $newObjectStorage,
            $this->subject->getStaff()
        );
    }

    /**
     * @test
     */
    public function setStaffForObjectStorageContainingSectionPositionsSetsStaff(): void
    {
        $staff = new \T3graf\Smartverein\Domain\Model\SectionPositions();
        $objectStorageHoldingExactlyOneStaff = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $objectStorageHoldingExactlyOneStaff->attach($staff);
        $this->subject->setStaff($objectStorageHoldingExactlyOneStaff);

        self::assertEquals($objectStorageHoldingExactlyOneStaff, $this->subject->_get('staff'));
    }

    /**
     * @test
     */
    public function addStaffToObjectStorageHoldingStaff(): void
    {
        $staff = new \T3graf\Smartverein\Domain\Model\SectionPositions();
        $staffObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->onlyMethods(['attach'])
            ->disableOriginalConstructor()
            ->getMock();

        $staffObjectStorageMock->expects(self::once())->method('attach')->with(self::equalTo($staff));
        $this->subject->_set('staff', $staffObjectStorageMock);

        $this->subject->addStaff($staff);
    }

    /**
     * @test
     */
    public function removeStaffFromObjectStorageHoldingStaff(): void
    {
        $staff = new \T3graf\Smartverein\Domain\Model\SectionPositions();
        $staffObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->onlyMethods(['detach'])
            ->disableOriginalConstructor()
            ->getMock();

        $staffObjectStorageMock->expects(self::once())->method('detach')->with(self::equalTo($staff));
        $this->subject->_set('staff', $staffObjectStorageMock);

        $this->subject->removeStaff($staff);
    }
}
