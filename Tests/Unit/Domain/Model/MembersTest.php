<?php

declare(strict_types=1);

namespace T3graf\Smartverein\Tests\Unit\Domain\Model;

use PHPUnit\Framework\MockObject\MockObject;
use TYPO3\TestingFramework\Core\AccessibleObjectInterface;
use TYPO3\TestingFramework\Core\Unit\UnitTestCase;

/**
 * Test case
 *
 * @author Development-Team <development@t3graf-media.de>
 */
class MembersTest extends UnitTestCase
{
    /**
     * @var \T3graf\Smartverein\Domain\Model\Members|MockObject|AccessibleObjectInterface
     */
    protected $subject;

    protected function setUp(): void
    {
        parent::setUp();

        $this->subject = $this->getAccessibleMock(
            \T3graf\Smartverein\Domain\Model\Members::class,
            ['dummy']
        );
    }

    protected function tearDown(): void
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function getMemberIdReturnsInitialValueForString|null(): void
    {
    }

    /**
     * @test
     */
    public function setMemberIdForString|nullSetsMemberId(): void
    {
    }

    /**
     * @test
     */
    public function getFirstnameReturnsInitialValueForString|null(): void
    {
    }

    /**
     * @test
     */
    public function setFirstnameForString|nullSetsFirstname(): void
    {
    }

    /**
     * @test
     */
    public function getMidnameReturnsInitialValueForString(): void
    {
        self::assertSame(
            '',
            $this->subject->getMidname()
        );
    }

    /**
     * @test
     */
    public function setMidnameForStringSetsMidname(): void
    {
        $this->subject->setMidname('Conceived at T3CON10');

        self::assertEquals('Conceived at T3CON10', $this->subject->_get('midname'));
    }

    /**
     * @test
     */
    public function getLastnameReturnsInitialValueForString(): void
    {
        self::assertSame(
            '',
            $this->subject->getLastname()
        );
    }

    /**
     * @test
     */
    public function setLastnameForStringSetsLastname(): void
    {
        $this->subject->setLastname('Conceived at T3CON10');

        self::assertEquals('Conceived at T3CON10', $this->subject->_get('lastname'));
    }

    /**
     * @test
     */
    public function getStatusReturnsInitialValueForInt(): void
    {
        self::assertSame(
            0,
            $this->subject->getStatus()
        );
    }

    /**
     * @test
     */
    public function setStatusForIntSetsStatus(): void
    {
        $this->subject->setStatus(12);

        self::assertEquals(12, $this->subject->_get('status'));
    }

    /**
     * @test
     */
    public function getSalutationReturnsInitialValueForInt(): void
    {
        self::assertSame(
            0,
            $this->subject->getSalutation()
        );
    }

    /**
     * @test
     */
    public function setSalutationForIntSetsSalutation(): void
    {
        $this->subject->setSalutation(12);

        self::assertEquals(12, $this->subject->_get('salutation'));
    }

    /**
     * @test
     */
    public function getTitleReturnsInitialValueForString(): void
    {
        self::assertSame(
            '',
            $this->subject->getTitle()
        );
    }

    /**
     * @test
     */
    public function setTitleForStringSetsTitle(): void
    {
        $this->subject->setTitle('Conceived at T3CON10');

        self::assertEquals('Conceived at T3CON10', $this->subject->_get('title'));
    }

    /**
     * @test
     */
    public function getStreetReturnsInitialValueForString(): void
    {
        self::assertSame(
            '',
            $this->subject->getStreet()
        );
    }

    /**
     * @test
     */
    public function setStreetForStringSetsStreet(): void
    {
        $this->subject->setStreet('Conceived at T3CON10');

        self::assertEquals('Conceived at T3CON10', $this->subject->_get('street'));
    }

    /**
     * @test
     */
    public function getZipReturnsInitialValueForString(): void
    {
        self::assertSame(
            '',
            $this->subject->getZip()
        );
    }

    /**
     * @test
     */
    public function setZipForStringSetsZip(): void
    {
        $this->subject->setZip('Conceived at T3CON10');

        self::assertEquals('Conceived at T3CON10', $this->subject->_get('zip'));
    }

    /**
     * @test
     */
    public function getCityReturnsInitialValueForString(): void
    {
        self::assertSame(
            '',
            $this->subject->getCity()
        );
    }

    /**
     * @test
     */
    public function setCityForStringSetsCity(): void
    {
        $this->subject->setCity('Conceived at T3CON10');

        self::assertEquals('Conceived at T3CON10', $this->subject->_get('city'));
    }

    /**
     * @test
     */
    public function getStateReturnsInitialValueForString(): void
    {
        self::assertSame(
            '',
            $this->subject->getState()
        );
    }

    /**
     * @test
     */
    public function setStateForStringSetsState(): void
    {
        $this->subject->setState('Conceived at T3CON10');

        self::assertEquals('Conceived at T3CON10', $this->subject->_get('state'));
    }

    /**
     * @test
     */
    public function getCountryReturnsInitialValueForString(): void
    {
        self::assertSame(
            '',
            $this->subject->getCountry()
        );
    }

    /**
     * @test
     */
    public function setCountryForStringSetsCountry(): void
    {
        $this->subject->setCountry('Conceived at T3CON10');

        self::assertEquals('Conceived at T3CON10', $this->subject->_get('country'));
    }

    /**
     * @test
     */
    public function getAdditionToAddressReturnsInitialValueForString(): void
    {
        self::assertSame(
            '',
            $this->subject->getAdditionToAddress()
        );
    }

    /**
     * @test
     */
    public function setAdditionToAddressForStringSetsAdditionToAddress(): void
    {
        $this->subject->setAdditionToAddress('Conceived at T3CON10');

        self::assertEquals('Conceived at T3CON10', $this->subject->_get('additionToAddress'));
    }

    /**
     * @test
     */
    public function getLeavingWishReturnsInitialValueForBool(): void
    {
        self::assertFalse($this->subject->getLeavingWish());
    }

    /**
     * @test
     */
    public function setLeavingWishForBoolSetsLeavingWish(): void
    {
        $this->subject->setLeavingWish(true);

        self::assertEquals(true, $this->subject->_get('leavingWish'));
    }

    /**
     * @test
     */
    public function getDateofbirthReturnsInitialValueForDateTime(): void
    {
        self::assertEquals(
            null,
            $this->subject->getDateofbirth()
        );
    }

    /**
     * @test
     */
    public function setDateofbirthForDateTimeSetsDateofbirth(): void
    {
        $dateTimeFixture = new \DateTime();
        $this->subject->setDateofbirth($dateTimeFixture);

        self::assertEquals($dateTimeFixture, $this->subject->_get('dateofbirth'));
    }

    /**
     * @test
     */
    public function getProfileImageReturnsInitialValueForFileReference(): void
    {
        self::assertEquals(
            null,
            $this->subject->getProfileImage()
        );
    }

    /**
     * @test
     */
    public function setProfileImageForFileReferenceSetsProfileImage(): void
    {
        $fileReferenceFixture = new \TYPO3\CMS\Extbase\Domain\Model\FileReference();
        $this->subject->setProfileImage($fileReferenceFixture);

        self::assertEquals($fileReferenceFixture, $this->subject->_get('profileImage'));
    }

    /**
     * @test
     */
    public function getPreferedCommunicationReturnsInitialValueForInt(): void
    {
        self::assertSame(
            0,
            $this->subject->getPreferedCommunication()
        );
    }

    /**
     * @test
     */
    public function setPreferedCommunicationForIntSetsPreferedCommunication(): void
    {
        $this->subject->setPreferedCommunication(12);

        self::assertEquals(12, $this->subject->_get('preferedCommunication'));
    }

    /**
     * @test
     */
    public function getMethodOfPaymentReturnsInitialValueForInt(): void
    {
        self::assertSame(
            0,
            $this->subject->getMethodOfPayment()
        );
    }

    /**
     * @test
     */
    public function setMethodOfPaymentForIntSetsMethodOfPayment(): void
    {
        $this->subject->setMethodOfPayment(12);

        self::assertEquals(12, $this->subject->_get('methodOfPayment'));
    }

    /**
     * @test
     */
    public function getAccountOwnerReturnsInitialValueForString(): void
    {
        self::assertSame(
            '',
            $this->subject->getAccountOwner()
        );
    }

    /**
     * @test
     */
    public function setAccountOwnerForStringSetsAccountOwner(): void
    {
        $this->subject->setAccountOwner('Conceived at T3CON10');

        self::assertEquals('Conceived at T3CON10', $this->subject->_get('accountOwner'));
    }

    /**
     * @test
     */
    public function getIbanReturnsInitialValueForString(): void
    {
        self::assertSame(
            '',
            $this->subject->getIban()
        );
    }

    /**
     * @test
     */
    public function setIbanForStringSetsIban(): void
    {
        $this->subject->setIban('Conceived at T3CON10');

        self::assertEquals('Conceived at T3CON10', $this->subject->_get('iban'));
    }

    /**
     * @test
     */
    public function getBicReturnsInitialValueForString(): void
    {
        self::assertSame(
            '',
            $this->subject->getBic()
        );
    }

    /**
     * @test
     */
    public function setBicForStringSetsBic(): void
    {
        $this->subject->setBic('Conceived at T3CON10');

        self::assertEquals('Conceived at T3CON10', $this->subject->_get('bic'));
    }

    /**
     * @test
     */
    public function getCustomfield1ReturnsInitialValueForString(): void
    {
        self::assertSame(
            '',
            $this->subject->getCustomfield1()
        );
    }

    /**
     * @test
     */
    public function setCustomfield1ForStringSetsCustomfield1(): void
    {
        $this->subject->setCustomfield1('Conceived at T3CON10');

        self::assertEquals('Conceived at T3CON10', $this->subject->_get('customfield1'));
    }

    /**
     * @test
     */
    public function getCustomfield2ReturnsInitialValueForString(): void
    {
        self::assertSame(
            '',
            $this->subject->getCustomfield2()
        );
    }

    /**
     * @test
     */
    public function setCustomfield2ForStringSetsCustomfield2(): void
    {
        $this->subject->setCustomfield2('Conceived at T3CON10');

        self::assertEquals('Conceived at T3CON10', $this->subject->_get('customfield2'));
    }

    /**
     * @test
     */
    public function getCustomfield3ReturnsInitialValueForString(): void
    {
        self::assertSame(
            '',
            $this->subject->getCustomfield3()
        );
    }

    /**
     * @test
     */
    public function setCustomfield3ForStringSetsCustomfield3(): void
    {
        $this->subject->setCustomfield3('Conceived at T3CON10');

        self::assertEquals('Conceived at T3CON10', $this->subject->_get('customfield3'));
    }

    /**
     * @test
     */
    public function getCustomfield4ReturnsInitialValueForString(): void
    {
        self::assertSame(
            '',
            $this->subject->getCustomfield4()
        );
    }

    /**
     * @test
     */
    public function setCustomfield4ForStringSetsCustomfield4(): void
    {
        $this->subject->setCustomfield4('Conceived at T3CON10');

        self::assertEquals('Conceived at T3CON10', $this->subject->_get('customfield4'));
    }

    /**
     * @test
     */
    public function getCustomfield5ReturnsInitialValueForString(): void
    {
        self::assertSame(
            '',
            $this->subject->getCustomfield5()
        );
    }

    /**
     * @test
     */
    public function setCustomfield5ForStringSetsCustomfield5(): void
    {
        $this->subject->setCustomfield5('Conceived at T3CON10');

        self::assertEquals('Conceived at T3CON10', $this->subject->_get('customfield5'));
    }

    /**
     * @test
     */
    public function getCustomfield6ReturnsInitialValueForString(): void
    {
        self::assertSame(
            '',
            $this->subject->getCustomfield6()
        );
    }

    /**
     * @test
     */
    public function setCustomfield6ForStringSetsCustomfield6(): void
    {
        $this->subject->setCustomfield6('Conceived at T3CON10');

        self::assertEquals('Conceived at T3CON10', $this->subject->_get('customfield6'));
    }

    /**
     * @test
     */
    public function getCustomfield7ReturnsInitialValueForString(): void
    {
        self::assertSame(
            '',
            $this->subject->getCustomfield7()
        );
    }

    /**
     * @test
     */
    public function setCustomfield7ForStringSetsCustomfield7(): void
    {
        $this->subject->setCustomfield7('Conceived at T3CON10');

        self::assertEquals('Conceived at T3CON10', $this->subject->_get('customfield7'));
    }

    /**
     * @test
     */
    public function getCustomfield8ReturnsInitialValueForString(): void
    {
        self::assertSame(
            '',
            $this->subject->getCustomfield8()
        );
    }

    /**
     * @test
     */
    public function setCustomfield8ForStringSetsCustomfield8(): void
    {
        $this->subject->setCustomfield8('Conceived at T3CON10');

        self::assertEquals('Conceived at T3CON10', $this->subject->_get('customfield8'));
    }

    /**
     * @test
     */
    public function getCustomfield9ReturnsInitialValueForString(): void
    {
        self::assertSame(
            '',
            $this->subject->getCustomfield9()
        );
    }

    /**
     * @test
     */
    public function setCustomfield9ForStringSetsCustomfield9(): void
    {
        $this->subject->setCustomfield9('Conceived at T3CON10');

        self::assertEquals('Conceived at T3CON10', $this->subject->_get('customfield9'));
    }

    /**
     * @test
     */
    public function getCustomfield10ReturnsInitialValueForString(): void
    {
        self::assertSame(
            '',
            $this->subject->getCustomfield10()
        );
    }

    /**
     * @test
     */
    public function setCustomfield10ForStringSetsCustomfield10(): void
    {
        $this->subject->setCustomfield10('Conceived at T3CON10');

        self::assertEquals('Conceived at T3CON10', $this->subject->_get('customfield10'));
    }

    /**
     * @test
     */
    public function getEntryDateReturnsInitialValueForDateTime(): void
    {
        self::assertEquals(
            null,
            $this->subject->getEntryDate()
        );
    }

    /**
     * @test
     */
    public function setEntryDateForDateTimeSetsEntryDate(): void
    {
        $dateTimeFixture = new \DateTime();
        $this->subject->setEntryDate($dateTimeFixture);

        self::assertEquals($dateTimeFixture, $this->subject->_get('entryDate'));
    }

    /**
     * @test
     */
    public function getLeavingDateReturnsInitialValueForDateTime(): void
    {
        self::assertEquals(
            null,
            $this->subject->getLeavingDate()
        );
    }

    /**
     * @test
     */
    public function setLeavingDateForDateTimeSetsLeavingDate(): void
    {
        $dateTimeFixture = new \DateTime();
        $this->subject->setLeavingDate($dateTimeFixture);

        self::assertEquals($dateTimeFixture, $this->subject->_get('leavingDate'));
    }

    /**
     * @test
     */
    public function getMandateReferenceReturnsInitialValueForString(): void
    {
        self::assertSame(
            '',
            $this->subject->getMandateReference()
        );
    }

    /**
     * @test
     */
    public function setMandateReferenceForStringSetsMandateReference(): void
    {
        $this->subject->setMandateReference('Conceived at T3CON10');

        self::assertEquals('Conceived at T3CON10', $this->subject->_get('mandateReference'));
    }

    /**
     * @test
     */
    public function getMandateDateReturnsInitialValueForDateTime(): void
    {
        self::assertEquals(
            null,
            $this->subject->getMandateDate()
        );
    }

    /**
     * @test
     */
    public function setMandateDateForDateTimeSetsMandateDate(): void
    {
        $dateTimeFixture = new \DateTime();
        $this->subject->setMandateDate($dateTimeFixture);

        self::assertEquals($dateTimeFixture, $this->subject->_get('mandateDate'));
    }

    /**
     * @test
     */
    public function getSepaMandateReturnsInitialValueForFileReference(): void
    {
        self::assertEquals(
            null,
            $this->subject->getSepaMandate()
        );
    }

    /**
     * @test
     */
    public function setSepaMandateForFileReferenceSetsSepaMandate(): void
    {
        $fileReferenceFixture = new \TYPO3\CMS\Extbase\Domain\Model\FileReference();
        $this->subject->setSepaMandate($fileReferenceFixture);

        self::assertEquals($fileReferenceFixture, $this->subject->_get('sepaMandate'));
    }

    /**
     * @test
     */
    public function getMembershipApplicationReturnsInitialValueForFileReference(): void
    {
        self::assertEquals(
            null,
            $this->subject->getMembershipApplication()
        );
    }

    /**
     * @test
     */
    public function setMembershipApplicationForFileReferenceSetsMembershipApplication(): void
    {
        $fileReferenceFixture = new \TYPO3\CMS\Extbase\Domain\Model\FileReference();
        $this->subject->setMembershipApplication($fileReferenceFixture);

        self::assertEquals($fileReferenceFixture, $this->subject->_get('membershipApplication'));
    }

    /**
     * @test
     */
    public function getLeavingStatementReturnsInitialValueForFileReference(): void
    {
        self::assertEquals(
            null,
            $this->subject->getLeavingStatement()
        );
    }

    /**
     * @test
     */
    public function setLeavingStatementForFileReferenceSetsLeavingStatement(): void
    {
        $fileReferenceFixture = new \TYPO3\CMS\Extbase\Domain\Model\FileReference();
        $this->subject->setLeavingStatement($fileReferenceFixture);

        self::assertEquals($fileReferenceFixture, $this->subject->_get('leavingStatement'));
    }

    /**
     * @test
     */
    public function getDeclarationOfConsentReturnsInitialValueForFileReference(): void
    {
        self::assertEquals(
            null,
            $this->subject->getDeclarationOfConsent()
        );
    }

    /**
     * @test
     */
    public function setDeclarationOfConsentForFileReferenceSetsDeclarationOfConsent(): void
    {
        $fileReferenceFixture = new \TYPO3\CMS\Extbase\Domain\Model\FileReference();
        $this->subject->setDeclarationOfConsent($fileReferenceFixture);

        self::assertEquals($fileReferenceFixture, $this->subject->_get('declarationOfConsent'));
    }

    /**
     * @test
     */
    public function getPaymentIntervalReturnsInitialValueForInt(): void
    {
        self::assertSame(
            0,
            $this->subject->getPaymentInterval()
        );
    }

    /**
     * @test
     */
    public function setPaymentIntervalForIntSetsPaymentInterval(): void
    {
        $this->subject->setPaymentInterval(12);

        self::assertEquals(12, $this->subject->_get('paymentInterval'));
    }

    /**
     * @test
     */
    public function getEmailReturnsInitialValueForString(): void
    {
        self::assertSame(
            '',
            $this->subject->getEmail()
        );
    }

    /**
     * @test
     */
    public function setEmailForStringSetsEmail(): void
    {
        $this->subject->setEmail('Conceived at T3CON10');

        self::assertEquals('Conceived at T3CON10', $this->subject->_get('email'));
    }

    /**
     * @test
     */
    public function getPhoneReturnsInitialValueForString(): void
    {
        self::assertSame(
            '',
            $this->subject->getPhone()
        );
    }

    /**
     * @test
     */
    public function setPhoneForStringSetsPhone(): void
    {
        $this->subject->setPhone('Conceived at T3CON10');

        self::assertEquals('Conceived at T3CON10', $this->subject->_get('phone'));
    }

    /**
     * @test
     */
    public function getMobileReturnsInitialValueForString(): void
    {
        self::assertSame(
            '',
            $this->subject->getMobile()
        );
    }

    /**
     * @test
     */
    public function setMobileForStringSetsMobile(): void
    {
        $this->subject->setMobile('Conceived at T3CON10');

        self::assertEquals('Conceived at T3CON10', $this->subject->_get('mobile'));
    }

    /**
     * @test
     */
    public function getFaxReturnsInitialValueForString(): void
    {
        self::assertSame(
            '',
            $this->subject->getFax()
        );
    }

    /**
     * @test
     */
    public function setFaxForStringSetsFax(): void
    {
        $this->subject->setFax('Conceived at T3CON10');

        self::assertEquals('Conceived at T3CON10', $this->subject->_get('fax'));
    }

    /**
     * @test
     */
    public function getMemberTypeReturnsInitialValueForInt(): void
    {
        self::assertSame(
            0,
            $this->subject->getMemberType()
        );
    }

    /**
     * @test
     */
    public function setMemberTypeForIntSetsMemberType(): void
    {
        $this->subject->setMemberType(12);

        self::assertEquals(12, $this->subject->_get('memberType'));
    }

    /**
     * @test
     */
    public function getOrganisationReturnsInitialValueForString(): void
    {
        self::assertSame(
            '',
            $this->subject->getOrganisation()
        );
    }

    /**
     * @test
     */
    public function setOrganisationForStringSetsOrganisation(): void
    {
        $this->subject->setOrganisation('Conceived at T3CON10');

        self::assertEquals('Conceived at T3CON10', $this->subject->_get('organisation'));
    }

    /**
     * @test
     */
    public function getContactPersonReturnsInitialValueForString(): void
    {
        self::assertSame(
            '',
            $this->subject->getContactPerson()
        );
    }

    /**
     * @test
     */
    public function setContactPersonForStringSetsContactPerson(): void
    {
        $this->subject->setContactPerson('Conceived at T3CON10');

        self::assertEquals('Conceived at T3CON10', $this->subject->_get('contactPerson'));
    }

    /**
     * @test
     */
    public function getPositionReturnsInitialValueForString(): void
    {
        self::assertSame(
            '',
            $this->subject->getPosition()
        );
    }

    /**
     * @test
     */
    public function setPositionForStringSetsPosition(): void
    {
        $this->subject->setPosition('Conceived at T3CON10');

        self::assertEquals('Conceived at T3CON10', $this->subject->_get('position'));
    }

    /**
     * @test
     */
    public function getDepartmentReturnsInitialValueForString(): void
    {
        self::assertSame(
            '',
            $this->subject->getDepartment()
        );
    }

    /**
     * @test
     */
    public function setDepartmentForStringSetsDepartment(): void
    {
        $this->subject->setDepartment('Conceived at T3CON10');

        self::assertEquals('Conceived at T3CON10', $this->subject->_get('department'));
    }

    /**
     * @test
     */
    public function getWebsiteReturnsInitialValueForString(): void
    {
        self::assertSame(
            '',
            $this->subject->getWebsite()
        );
    }

    /**
     * @test
     */
    public function setWebsiteForStringSetsWebsite(): void
    {
        $this->subject->setWebsite('Conceived at T3CON10');

        self::assertEquals('Conceived at T3CON10', $this->subject->_get('website'));
    }

    /**
     * @test
     */
    public function getContactPersonSalutationReturnsInitialValueForInt(): void
    {
        self::assertSame(
            0,
            $this->subject->getContactPersonSalutation()
        );
    }

    /**
     * @test
     */
    public function setContactPersonSalutationForIntSetsContactPersonSalutation(): void
    {
        $this->subject->setContactPersonSalutation(12);

        self::assertEquals(12, $this->subject->_get('contactPersonSalutation'));
    }

    /**
     * @test
     */
    public function getFeUserReturnsInitialValueForFrontendUser(): void
    {
        $newObjectStorage = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        self::assertEquals(
            $newObjectStorage,
            $this->subject->getFeUser()
        );
    }

    /**
     * @test
     */
    public function setFeUserForObjectStorageContainingFrontendUserSetsFeUser(): void
    {
        $feUser = new \TYPO3\CMS\Extbase\Domain\Model\FrontendUser();
        $objectStorageHoldingExactlyOneFeUser = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $objectStorageHoldingExactlyOneFeUser->attach($feUser);
        $this->subject->setFeUser($objectStorageHoldingExactlyOneFeUser);

        self::assertEquals($objectStorageHoldingExactlyOneFeUser, $this->subject->_get('feUser'));
    }

    /**
     * @test
     */
    public function addFeUserToObjectStorageHoldingFeUser(): void
    {
        $feUser = new \TYPO3\CMS\Extbase\Domain\Model\FrontendUser();
        $feUserObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->onlyMethods(['attach'])
            ->disableOriginalConstructor()
            ->getMock();

        $feUserObjectStorageMock->expects(self::once())->method('attach')->with(self::equalTo($feUser));
        $this->subject->_set('feUser', $feUserObjectStorageMock);

        $this->subject->addFeUser($feUser);
    }

    /**
     * @test
     */
    public function removeFeUserFromObjectStorageHoldingFeUser(): void
    {
        $feUser = new \TYPO3\CMS\Extbase\Domain\Model\FrontendUser();
        $feUserObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->onlyMethods(['detach'])
            ->disableOriginalConstructor()
            ->getMock();

        $feUserObjectStorageMock->expects(self::once())->method('detach')->with(self::equalTo($feUser));
        $this->subject->_set('feUser', $feUserObjectStorageMock);

        $this->subject->removeFeUser($feUser);
    }

    /**
     * @test
     */
    public function getContactsReturnsInitialValueForContacts(): void
    {
        $newObjectStorage = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        self::assertEquals(
            $newObjectStorage,
            $this->subject->getContacts()
        );
    }

    /**
     * @test
     */
    public function setContactsForObjectStorageContainingContactsSetsContacts(): void
    {
        $contact = new \T3graf\Smartverein\Domain\Model\Contacts();
        $objectStorageHoldingExactlyOneContacts = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $objectStorageHoldingExactlyOneContacts->attach($contact);
        $this->subject->setContacts($objectStorageHoldingExactlyOneContacts);

        self::assertEquals($objectStorageHoldingExactlyOneContacts, $this->subject->_get('contacts'));
    }

    /**
     * @test
     */
    public function addContactToObjectStorageHoldingContacts(): void
    {
        $contact = new \T3graf\Smartverein\Domain\Model\Contacts();
        $contactsObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->onlyMethods(['attach'])
            ->disableOriginalConstructor()
            ->getMock();

        $contactsObjectStorageMock->expects(self::once())->method('attach')->with(self::equalTo($contact));
        $this->subject->_set('contacts', $contactsObjectStorageMock);

        $this->subject->addContact($contact);
    }

    /**
     * @test
     */
    public function removeContactFromObjectStorageHoldingContacts(): void
    {
        $contact = new \T3graf\Smartverein\Domain\Model\Contacts();
        $contactsObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->onlyMethods(['detach'])
            ->disableOriginalConstructor()
            ->getMock();

        $contactsObjectStorageMock->expects(self::once())->method('detach')->with(self::equalTo($contact));
        $this->subject->_set('contacts', $contactsObjectStorageMock);

        $this->subject->removeContact($contact);
    }

    /**
     * @test
     */
    public function getBelongstoReturnsInitialValueForMembers(): void
    {
        $newObjectStorage = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        self::assertEquals(
            $newObjectStorage,
            $this->subject->getBelongsto()
        );
    }

    /**
     * @test
     */
    public function setBelongstoForObjectStorageContainingMembersSetsBelongsto(): void
    {
        $belongsto = new \T3graf\Smartverein\Domain\Model\Members();
        $objectStorageHoldingExactlyOneBelongsto = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $objectStorageHoldingExactlyOneBelongsto->attach($belongsto);
        $this->subject->setBelongsto($objectStorageHoldingExactlyOneBelongsto);

        self::assertEquals($objectStorageHoldingExactlyOneBelongsto, $this->subject->_get('belongsto'));
    }

    /**
     * @test
     */
    public function addBelongstoToObjectStorageHoldingBelongsto(): void
    {
        $belongsto = new \T3graf\Smartverein\Domain\Model\Members();
        $belongstoObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->onlyMethods(['attach'])
            ->disableOriginalConstructor()
            ->getMock();

        $belongstoObjectStorageMock->expects(self::once())->method('attach')->with(self::equalTo($belongsto));
        $this->subject->_set('belongsto', $belongstoObjectStorageMock);

        $this->subject->addBelongsto($belongsto);
    }

    /**
     * @test
     */
    public function removeBelongstoFromObjectStorageHoldingBelongsto(): void
    {
        $belongsto = new \T3graf\Smartverein\Domain\Model\Members();
        $belongstoObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->onlyMethods(['detach'])
            ->disableOriginalConstructor()
            ->getMock();

        $belongstoObjectStorageMock->expects(self::once())->method('detach')->with(self::equalTo($belongsto));
        $this->subject->_set('belongsto', $belongstoObjectStorageMock);

        $this->subject->removeBelongsto($belongsto);
    }

    /**
     * @test
     */
    public function getFunctionsReturnsInitialValueForFunctions(): void
    {
        $newObjectStorage = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        self::assertEquals(
            $newObjectStorage,
            $this->subject->getFunctions()
        );
    }

    /**
     * @test
     */
    public function setFunctionsForObjectStorageContainingFunctionsSetsFunctions(): void
    {
        $function = new \T3graf\Smartverein\Domain\Model\Functions();
        $objectStorageHoldingExactlyOneFunctions = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $objectStorageHoldingExactlyOneFunctions->attach($function);
        $this->subject->setFunctions($objectStorageHoldingExactlyOneFunctions);

        self::assertEquals($objectStorageHoldingExactlyOneFunctions, $this->subject->_get('functions'));
    }

    /**
     * @test
     */
    public function addFunctionToObjectStorageHoldingFunctions(): void
    {
        $function = new \T3graf\Smartverein\Domain\Model\Functions();
        $functionsObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->onlyMethods(['attach'])
            ->disableOriginalConstructor()
            ->getMock();

        $functionsObjectStorageMock->expects(self::once())->method('attach')->with(self::equalTo($function));
        $this->subject->_set('functions', $functionsObjectStorageMock);

        $this->subject->addFunction($function);
    }

    /**
     * @test
     */
    public function removeFunctionFromObjectStorageHoldingFunctions(): void
    {
        $function = new \T3graf\Smartverein\Domain\Model\Functions();
        $functionsObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->onlyMethods(['detach'])
            ->disableOriginalConstructor()
            ->getMock();

        $functionsObjectStorageMock->expects(self::once())->method('detach')->with(self::equalTo($function));
        $this->subject->_set('functions', $functionsObjectStorageMock);

        $this->subject->removeFunction($function);
    }

    /**
     * @test
     */
    public function getPositionsReturnsInitialValueForPositions(): void
    {
        $newObjectStorage = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        self::assertEquals(
            $newObjectStorage,
            $this->subject->getPositions()
        );
    }

    /**
     * @test
     */
    public function setPositionsForObjectStorageContainingPositionsSetsPositions(): void
    {
        $position = new \T3graf\Smartverein\Domain\Model\Positions();
        $objectStorageHoldingExactlyOnePositions = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $objectStorageHoldingExactlyOnePositions->attach($position);
        $this->subject->setPositions($objectStorageHoldingExactlyOnePositions);

        self::assertEquals($objectStorageHoldingExactlyOnePositions, $this->subject->_get('positions'));
    }

    /**
     * @test
     */
    public function addPositionToObjectStorageHoldingPositions(): void
    {
        $position = new \T3graf\Smartverein\Domain\Model\Positions();
        $positionsObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->onlyMethods(['attach'])
            ->disableOriginalConstructor()
            ->getMock();

        $positionsObjectStorageMock->expects(self::once())->method('attach')->with(self::equalTo($position));
        $this->subject->_set('positions', $positionsObjectStorageMock);

        $this->subject->addPosition($position);
    }

    /**
     * @test
     */
    public function removePositionFromObjectStorageHoldingPositions(): void
    {
        $position = new \T3graf\Smartverein\Domain\Model\Positions();
        $positionsObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->onlyMethods(['detach'])
            ->disableOriginalConstructor()
            ->getMock();

        $positionsObjectStorageMock->expects(self::once())->method('detach')->with(self::equalTo($position));
        $this->subject->_set('positions', $positionsObjectStorageMock);

        $this->subject->removePosition($position);
    }

    /**
     * @test
     */
    public function getRoleReturnsInitialValueForRoles(): void
    {
        $newObjectStorage = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        self::assertEquals(
            $newObjectStorage,
            $this->subject->getRole()
        );
    }

    /**
     * @test
     */
    public function setRoleForObjectStorageContainingRolesSetsRole(): void
    {
        $role = new \T3graf\Smartverein\Domain\Model\Roles();
        $objectStorageHoldingExactlyOneRole = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $objectStorageHoldingExactlyOneRole->attach($role);
        $this->subject->setRole($objectStorageHoldingExactlyOneRole);

        self::assertEquals($objectStorageHoldingExactlyOneRole, $this->subject->_get('role'));
    }

    /**
     * @test
     */
    public function addRoleToObjectStorageHoldingRole(): void
    {
        $role = new \T3graf\Smartverein\Domain\Model\Roles();
        $roleObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->onlyMethods(['attach'])
            ->disableOriginalConstructor()
            ->getMock();

        $roleObjectStorageMock->expects(self::once())->method('attach')->with(self::equalTo($role));
        $this->subject->_set('role', $roleObjectStorageMock);

        $this->subject->addRole($role);
    }

    /**
     * @test
     */
    public function removeRoleFromObjectStorageHoldingRole(): void
    {
        $role = new \T3graf\Smartverein\Domain\Model\Roles();
        $roleObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->onlyMethods(['detach'])
            ->disableOriginalConstructor()
            ->getMock();

        $roleObjectStorageMock->expects(self::once())->method('detach')->with(self::equalTo($role));
        $this->subject->_set('role', $roleObjectStorageMock);

        $this->subject->removeRole($role);
    }

    /**
     * @test
     */
    public function getSectionsReturnsInitialValueForSections(): void
    {
        $newObjectStorage = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        self::assertEquals(
            $newObjectStorage,
            $this->subject->getSections()
        );
    }

    /**
     * @test
     */
    public function setSectionsForObjectStorageContainingSectionsSetsSections(): void
    {
        $section = new \T3graf\Smartverein\Domain\Model\Sections();
        $objectStorageHoldingExactlyOneSections = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $objectStorageHoldingExactlyOneSections->attach($section);
        $this->subject->setSections($objectStorageHoldingExactlyOneSections);

        self::assertEquals($objectStorageHoldingExactlyOneSections, $this->subject->_get('sections'));
    }

    /**
     * @test
     */
    public function addSectionToObjectStorageHoldingSections(): void
    {
        $section = new \T3graf\Smartverein\Domain\Model\Sections();
        $sectionsObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->onlyMethods(['attach'])
            ->disableOriginalConstructor()
            ->getMock();

        $sectionsObjectStorageMock->expects(self::once())->method('attach')->with(self::equalTo($section));
        $this->subject->_set('sections', $sectionsObjectStorageMock);

        $this->subject->addSection($section);
    }

    /**
     * @test
     */
    public function removeSectionFromObjectStorageHoldingSections(): void
    {
        $section = new \T3graf\Smartverein\Domain\Model\Sections();
        $sectionsObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->onlyMethods(['detach'])
            ->disableOriginalConstructor()
            ->getMock();

        $sectionsObjectStorageMock->expects(self::once())->method('detach')->with(self::equalTo($section));
        $this->subject->_set('sections', $sectionsObjectStorageMock);

        $this->subject->removeSection($section);
    }

    /**
     * @test
     */
    public function getVitaReturnsInitialValueForVitaEntries(): void
    {
        $newObjectStorage = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        self::assertEquals(
            $newObjectStorage,
            $this->subject->getVita()
        );
    }

    /**
     * @test
     */
    public function setVitaForObjectStorageContainingVitaEntriesSetsVita(): void
    {
        $vitum = new \T3graf\Smartverein\Domain\Model\VitaEntries();
        $objectStorageHoldingExactlyOneVita = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $objectStorageHoldingExactlyOneVita->attach($vitum);
        $this->subject->setVita($objectStorageHoldingExactlyOneVita);

        self::assertEquals($objectStorageHoldingExactlyOneVita, $this->subject->_get('vita'));
    }

    /**
     * @test
     */
    public function addVitumToObjectStorageHoldingVita(): void
    {
        $vitum = new \T3graf\Smartverein\Domain\Model\VitaEntries();
        $vitaObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->onlyMethods(['attach'])
            ->disableOriginalConstructor()
            ->getMock();

        $vitaObjectStorageMock->expects(self::once())->method('attach')->with(self::equalTo($vitum));
        $this->subject->_set('vita', $vitaObjectStorageMock);

        $this->subject->addVitum($vitum);
    }

    /**
     * @test
     */
    public function removeVitumFromObjectStorageHoldingVita(): void
    {
        $vitum = new \T3graf\Smartverein\Domain\Model\VitaEntries();
        $vitaObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->onlyMethods(['detach'])
            ->disableOriginalConstructor()
            ->getMock();

        $vitaObjectStorageMock->expects(self::once())->method('detach')->with(self::equalTo($vitum));
        $this->subject->_set('vita', $vitaObjectStorageMock);

        $this->subject->removeVitum($vitum);
    }

    /**
     * @test
     */
    public function getChangelogReturnsInitialValueForChanges(): void
    {
        $newObjectStorage = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        self::assertEquals(
            $newObjectStorage,
            $this->subject->getChangelog()
        );
    }

    /**
     * @test
     */
    public function setChangelogForObjectStorageContainingChangesSetsChangelog(): void
    {
        $changelog = new \T3graf\Smartverein\Domain\Model\Changes();
        $objectStorageHoldingExactlyOneChangelog = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $objectStorageHoldingExactlyOneChangelog->attach($changelog);
        $this->subject->setChangelog($objectStorageHoldingExactlyOneChangelog);

        self::assertEquals($objectStorageHoldingExactlyOneChangelog, $this->subject->_get('changelog'));
    }

    /**
     * @test
     */
    public function addChangelogToObjectStorageHoldingChangelog(): void
    {
        $changelog = new \T3graf\Smartverein\Domain\Model\Changes();
        $changelogObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->onlyMethods(['attach'])
            ->disableOriginalConstructor()
            ->getMock();

        $changelogObjectStorageMock->expects(self::once())->method('attach')->with(self::equalTo($changelog));
        $this->subject->_set('changelog', $changelogObjectStorageMock);

        $this->subject->addChangelog($changelog);
    }

    /**
     * @test
     */
    public function removeChangelogFromObjectStorageHoldingChangelog(): void
    {
        $changelog = new \T3graf\Smartverein\Domain\Model\Changes();
        $changelogObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->onlyMethods(['detach'])
            ->disableOriginalConstructor()
            ->getMock();

        $changelogObjectStorageMock->expects(self::once())->method('detach')->with(self::equalTo($changelog));
        $this->subject->_set('changelog', $changelogObjectStorageMock);

        $this->subject->removeChangelog($changelog);
    }

    /**
     * @test
     */
    public function getMemberFeeReturnsInitialValueForMemberFees(): void
    {
        $newObjectStorage = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        self::assertEquals(
            $newObjectStorage,
            $this->subject->getMemberFee()
        );
    }

    /**
     * @test
     */
    public function setMemberFeeForObjectStorageContainingMemberFeesSetsMemberFee(): void
    {
        $memberFee = new \T3graf\Smartverein\Domain\Model\MemberFees();
        $objectStorageHoldingExactlyOneMemberFee = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $objectStorageHoldingExactlyOneMemberFee->attach($memberFee);
        $this->subject->setMemberFee($objectStorageHoldingExactlyOneMemberFee);

        self::assertEquals($objectStorageHoldingExactlyOneMemberFee, $this->subject->_get('memberFee'));
    }

    /**
     * @test
     */
    public function addMemberFeeToObjectStorageHoldingMemberFee(): void
    {
        $memberFee = new \T3graf\Smartverein\Domain\Model\MemberFees();
        $memberFeeObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->onlyMethods(['attach'])
            ->disableOriginalConstructor()
            ->getMock();

        $memberFeeObjectStorageMock->expects(self::once())->method('attach')->with(self::equalTo($memberFee));
        $this->subject->_set('memberFee', $memberFeeObjectStorageMock);

        $this->subject->addMemberFee($memberFee);
    }

    /**
     * @test
     */
    public function removeMemberFeeFromObjectStorageHoldingMemberFee(): void
    {
        $memberFee = new \T3graf\Smartverein\Domain\Model\MemberFees();
        $memberFeeObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->onlyMethods(['detach'])
            ->disableOriginalConstructor()
            ->getMock();

        $memberFeeObjectStorageMock->expects(self::once())->method('detach')->with(self::equalTo($memberFee));
        $this->subject->_set('memberFee', $memberFeeObjectStorageMock);

        $this->subject->removeMemberFee($memberFee);
    }
}
