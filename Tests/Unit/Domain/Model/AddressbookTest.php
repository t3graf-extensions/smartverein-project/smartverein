<?php

declare(strict_types=1);

/*
 * This file is part of the "SmartVerein - TYPO3 Club Management" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2023 Development-Team <development@t3graf-media.de>, T3graf media-agentur UG
 */

namespace T3graf\Smartverein\Tests\Unit\Domain\Model;

use PHPUnit\Framework\MockObject\MockObject;
use TYPO3\TestingFramework\Core\AccessibleObjectInterface;
use TYPO3\TestingFramework\Core\Unit\UnitTestCase;

/**
 * Test case
 *
 */
class AddressbookTest extends UnitTestCase
{
    /**
     * @var \T3graf\Smartverein\Domain\Model\Addressbook|MockObject|AccessibleObjectInterface
     */
    protected $subject;

    protected function setUp(): void
    {
        parent::setUp();

        $this->subject = $this->getAccessibleMock(
            \T3graf\Smartverein\Domain\Model\Addressbook::class,
            ['dummy']
        );
    }

    protected function tearDown(): void
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function getTypeReturnsInitialValueForInt(): void
    {
        self::assertSame(
            0,
            $this->subject->getType()
        );
    }

    /**
     * @test
     */
    public function setTypeForIntSetsType(): void
    {
        $this->subject->setType(12);

        self::assertEquals(12, $this->subject->_get('type'));
    }

    /**
     * @test
     */
    public function getCompanyNameReturnsInitialValueForString(): void
    {
        self::assertSame(
            '',
            $this->subject->getCompanyName()
        );
    }

    /**
     * @test
     */
    public function setCompanyNameForStringSetsCompanyName(): void
    {
        $this->subject->setCompanyName('Conceived at T3CON10');

        self::assertEquals('Conceived at T3CON10', $this->subject->_get('companyName'));
    }

    /**
     * @test
     */
    public function getContactPersonReturnsInitialValueForString(): void
    {
        self::assertSame(
            '',
            $this->subject->getContactPerson()
        );
    }

    /**
     * @test
     */
    public function setContactPersonForStringSetsContactPerson(): void
    {
        $this->subject->setContactPerson('Conceived at T3CON10');

        self::assertEquals('Conceived at T3CON10', $this->subject->_get('contactPerson'));
    }

    /**
     * @test
     */
    public function getStreetReturnsInitialValueForString(): void
    {
        self::assertSame(
            '',
            $this->subject->getStreet()
        );
    }

    /**
     * @test
     */
    public function setStreetForStringSetsStreet(): void
    {
        $this->subject->setStreet('Conceived at T3CON10');

        self::assertEquals('Conceived at T3CON10', $this->subject->_get('street'));
    }

    /**
     * @test
     */
    public function getZipReturnsInitialValueForString(): void
    {
        self::assertSame(
            '',
            $this->subject->getZip()
        );
    }

    /**
     * @test
     */
    public function setZipForStringSetsZip(): void
    {
        $this->subject->setZip('Conceived at T3CON10');

        self::assertEquals('Conceived at T3CON10', $this->subject->_get('zip'));
    }

    /**
     * @test
     */
    public function getCityReturnsInitialValueForString(): void
    {
        self::assertSame(
            '',
            $this->subject->getCity()
        );
    }

    /**
     * @test
     */
    public function setCityForStringSetsCity(): void
    {
        $this->subject->setCity('Conceived at T3CON10');

        self::assertEquals('Conceived at T3CON10', $this->subject->_get('city'));
    }

    /**
     * @test
     */
    public function getStateReturnsInitialValueForString(): void
    {
        self::assertSame(
            '',
            $this->subject->getState()
        );
    }

    /**
     * @test
     */
    public function setStateForStringSetsState(): void
    {
        $this->subject->setState('Conceived at T3CON10');

        self::assertEquals('Conceived at T3CON10', $this->subject->_get('state'));
    }

    /**
     * @test
     */
    public function getCountryReturnsInitialValueForString(): void
    {
        self::assertSame(
            '',
            $this->subject->getCountry()
        );
    }

    /**
     * @test
     */
    public function setCountryForStringSetsCountry(): void
    {
        $this->subject->setCountry('Conceived at T3CON10');

        self::assertEquals('Conceived at T3CON10', $this->subject->_get('country'));
    }

    /**
     * @test
     */
    public function getImageReturnsInitialValueForFileReference(): void
    {
        self::assertEquals(
            null,
            $this->subject->getImage()
        );
    }

    /**
     * @test
     */
    public function setImageForFileReferenceSetsImage(): void
    {
        $fileReferenceFixture = new \TYPO3\CMS\Extbase\Domain\Model\FileReference();
        $this->subject->setImage($fileReferenceFixture);

        self::assertEquals($fileReferenceFixture, $this->subject->_get('image'));
    }

    /**
     * @test
     */
    public function getSalutationReturnsInitialValueForString(): void
    {
        self::assertSame(
            '',
            $this->subject->getSalutation()
        );
    }

    /**
     * @test
     */
    public function setSalutationForStringSetsSalutation(): void
    {
        $this->subject->setSalutation('Conceived at T3CON10');

        self::assertEquals('Conceived at T3CON10', $this->subject->_get('salutation'));
    }

    /**
     * @test
     */
    public function getDepartmendReturnsInitialValueForString(): void
    {
        self::assertSame(
            '',
            $this->subject->getDepartmend()
        );
    }

    /**
     * @test
     */
    public function setDepartmendForStringSetsDepartmend(): void
    {
        $this->subject->setDepartmend('Conceived at T3CON10');

        self::assertEquals('Conceived at T3CON10', $this->subject->_get('departmend'));
    }

    /**
     * @test
     */
    public function getPositionReturnsInitialValueForString(): void
    {
        self::assertSame(
            '',
            $this->subject->getPosition()
        );
    }

    /**
     * @test
     */
    public function setPositionForStringSetsPosition(): void
    {
        $this->subject->setPosition('Conceived at T3CON10');

        self::assertEquals('Conceived at T3CON10', $this->subject->_get('position'));
    }

    /**
     * @test
     */
    public function getNoteReturnsInitialValueForString(): void
    {
        self::assertSame(
            '',
            $this->subject->getNote()
        );
    }

    /**
     * @test
     */
    public function setNoteForStringSetsNote(): void
    {
        $this->subject->setNote('Conceived at T3CON10');

        self::assertEquals('Conceived at T3CON10', $this->subject->_get('note'));
    }

    /**
     * @test
     */
    public function getContactsReturnsInitialValueForContacts(): void
    {
        $newObjectStorage = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        self::assertEquals(
            $newObjectStorage,
            $this->subject->getContacts()
        );
    }

    /**
     * @test
     */
    public function setContactsForObjectStorageContainingContactsSetsContacts(): void
    {
        $contact = new \T3graf\Smartverein\Domain\Model\Contacts();
        $objectStorageHoldingExactlyOneContacts = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $objectStorageHoldingExactlyOneContacts->attach($contact);
        $this->subject->setContacts($objectStorageHoldingExactlyOneContacts);

        self::assertEquals($objectStorageHoldingExactlyOneContacts, $this->subject->_get('contacts'));
    }

    /**
     * @test
     */
    public function addContactToObjectStorageHoldingContacts(): void
    {
        $contact = new \T3graf\Smartverein\Domain\Model\Contacts();
        $contactsObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->onlyMethods(['attach'])
            ->disableOriginalConstructor()
            ->getMock();

        $contactsObjectStorageMock->expects(self::once())->method('attach')->with(self::equalTo($contact));
        $this->subject->_set('contacts', $contactsObjectStorageMock);

        $this->subject->addContact($contact);
    }

    /**
     * @test
     */
    public function removeContactFromObjectStorageHoldingContacts(): void
    {
        $contact = new \T3graf\Smartverein\Domain\Model\Contacts();
        $contactsObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->onlyMethods(['detach'])
            ->disableOriginalConstructor()
            ->getMock();

        $contactsObjectStorageMock->expects(self::once())->method('detach')->with(self::equalTo($contact));
        $this->subject->_set('contacts', $contactsObjectStorageMock);

        $this->subject->removeContact($contact);
    }
}
