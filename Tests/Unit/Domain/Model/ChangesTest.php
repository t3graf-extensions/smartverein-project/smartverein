<?php

declare(strict_types=1);

/*
 * This file is part of the "SmartVerein - TYPO3 Club Management" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2023 Development-Team <development@t3graf-media.de>, T3graf media-agentur UG
 */

namespace T3graf\Smartverein\Tests\Unit\Domain\Model;

use PHPUnit\Framework\MockObject\MockObject;
use TYPO3\TestingFramework\Core\AccessibleObjectInterface;
use TYPO3\TestingFramework\Core\Unit\UnitTestCase;

/**
 * Test case
 *
 */
class ChangesTest extends UnitTestCase
{
    /**
     * @var \T3graf\Smartverein\Domain\Model\Changes|MockObject|AccessibleObjectInterface
     */
    protected $subject;

    protected function setUp(): void
    {
        parent::setUp();

        $this->subject = $this->getAccessibleMock(
            \T3graf\Smartverein\Domain\Model\Changes::class,
            ['dummy']
        );
    }

    protected function tearDown(): void
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function getDateReturnsInitialValueForDateTime(): void
    {
        self::assertEquals(
            null,
            $this->subject->getDate()
        );
    }

    /**
     * @test
     */
    public function setDateForDateTimeSetsDate(): void
    {
        $dateTimeFixture = new \DateTime();
        $this->subject->setDate($dateTimeFixture);

        self::assertEquals($dateTimeFixture, $this->subject->_get('date'));
    }

    /**
     * @test
     */
    public function getChangedByReturnsInitialValueForString(): void
    {
        self::assertSame(
            '',
            $this->subject->getChangedBy()
        );
    }

    /**
     * @test
     */
    public function setChangedByForStringSetsChangedBy(): void
    {
        $this->subject->setChangedBy('Conceived at T3CON10');

        self::assertEquals('Conceived at T3CON10', $this->subject->_get('changedBy'));
    }

    /**
     * @test
     */
    public function getNoteReturnsInitialValueForString(): void
    {
        self::assertSame(
            '',
            $this->subject->getNote()
        );
    }

    /**
     * @test
     */
    public function setNoteForStringSetsNote(): void
    {
        $this->subject->setNote('Conceived at T3CON10');

        self::assertEquals('Conceived at T3CON10', $this->subject->_get('note'));
    }
}
