<?php

declare(strict_types=1);

/*
 * This file is part of the "SmartVerein - TYPO3 Club Management" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2023 Development-Team <development@t3graf-media.de>, T3graf media-agentur UG
 */

namespace T3graf\Smartverein\Tests\Unit\Domain\Model;

use PHPUnit\Framework\MockObject\MockObject;
use TYPO3\TestingFramework\Core\AccessibleObjectInterface;
use TYPO3\TestingFramework\Core\Unit\UnitTestCase;

/**
 * Test case
 *
 */
class MailQueueTest extends UnitTestCase
{
    /**
     * @var \T3graf\Smartverein\Domain\Model\MailQueue|MockObject|AccessibleObjectInterface
     */
    protected $subject;

    protected function setUp(): void
    {
        parent::setUp();

        $this->subject = $this->getAccessibleMock(
            \T3graf\Smartverein\Domain\Model\MailQueue::class,
            ['dummy']
        );
    }

    protected function tearDown(): void
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function getSubjectReturnsInitialValueForString(): void
    {
        self::assertSame(
            '',
            $this->subject->getSubject()
        );
    }

    /**
     * @test
     */
    public function setSubjectForStringSetsSubject(): void
    {
        $this->subject->setSubject('Conceived at T3CON10');

        self::assertEquals('Conceived at T3CON10', $this->subject->_get('subject'));
    }

    /**
     * @test
     */
    public function getBodyReturnsInitialValueForString(): void
    {
        self::assertSame(
            '',
            $this->subject->getBody()
        );
    }

    /**
     * @test
     */
    public function setBodyForStringSetsBody(): void
    {
        $this->subject->setBody('Conceived at T3CON10');

        self::assertEquals('Conceived at T3CON10', $this->subject->_get('body'));
    }

    /**
     * @test
     */
    public function getBodyHtmlReturnsInitialValueForString(): void
    {
        self::assertSame(
            '',
            $this->subject->getBodyHtml()
        );
    }

    /**
     * @test
     */
    public function setBodyHtmlForStringSetsBodyHtml(): void
    {
        $this->subject->setBodyHtml('Conceived at T3CON10');

        self::assertEquals('Conceived at T3CON10', $this->subject->_get('bodyHtml'));
    }

    /**
     * @test
     */
    public function getSenderReturnsInitialValueForString(): void
    {
        self::assertSame(
            '',
            $this->subject->getSender()
        );
    }

    /**
     * @test
     */
    public function setSenderForStringSetsSender(): void
    {
        $this->subject->setSender('Conceived at T3CON10');

        self::assertEquals('Conceived at T3CON10', $this->subject->_get('sender'));
    }

    /**
     * @test
     */
    public function getRecipientReturnsInitialValueForString(): void
    {
        self::assertSame(
            '',
            $this->subject->getRecipient()
        );
    }

    /**
     * @test
     */
    public function setRecipientForStringSetsRecipient(): void
    {
        $this->subject->setRecipient('Conceived at T3CON10');

        self::assertEquals('Conceived at T3CON10', $this->subject->_get('recipient'));
    }

    /**
     * @test
     */
    public function getSenderNameReturnsInitialValueForString(): void
    {
        self::assertSame(
            '',
            $this->subject->getSenderName()
        );
    }

    /**
     * @test
     */
    public function setSenderNameForStringSetsSenderName(): void
    {
        $this->subject->setSenderName('Conceived at T3CON10');

        self::assertEquals('Conceived at T3CON10', $this->subject->_get('senderName'));
    }

    /**
     * @test
     */
    public function getRecipientNameReturnsInitialValueForString(): void
    {
        self::assertSame(
            '',
            $this->subject->getRecipientName()
        );
    }

    /**
     * @test
     */
    public function setRecipientNameForStringSetsRecipientName(): void
    {
        $this->subject->setRecipientName('Conceived at T3CON10');

        self::assertEquals('Conceived at T3CON10', $this->subject->_get('recipientName'));
    }

    /**
     * @test
     */
    public function getAttachementsReturnsInitialValueForString(): void
    {
        self::assertSame(
            '',
            $this->subject->getAttachements()
        );
    }

    /**
     * @test
     */
    public function setAttachementsForStringSetsAttachements(): void
    {
        $this->subject->setAttachements('Conceived at T3CON10');

        self::assertEquals('Conceived at T3CON10', $this->subject->_get('attachements'));
    }

    /**
     * @test
     */
    public function getDateSentReturnsInitialValueForDateTime(): void
    {
        self::assertEquals(
            null,
            $this->subject->getDateSent()
        );
    }

    /**
     * @test
     */
    public function setDateSentForDateTimeSetsDateSent(): void
    {
        $dateTimeFixture = new \DateTime();
        $this->subject->setDateSent($dateTimeFixture);

        self::assertEquals($dateTimeFixture, $this->subject->_get('dateSent'));
    }

    /**
     * @test
     */
    public function getTypeReturnsInitialValueForString(): void
    {
        self::assertSame(
            '',
            $this->subject->getType()
        );
    }

    /**
     * @test
     */
    public function setTypeForStringSetsType(): void
    {
        $this->subject->setType('Conceived at T3CON10');

        self::assertEquals('Conceived at T3CON10', $this->subject->_get('type'));
    }

    /**
     * @test
     */
    public function getStatusReturnsInitialValueForInt(): void
    {
        self::assertSame(
            0,
            $this->subject->getStatus()
        );
    }

    /**
     * @test
     */
    public function setStatusForIntSetsStatus(): void
    {
        $this->subject->setStatus(12);

        self::assertEquals(12, $this->subject->_get('status'));
    }

    /**
     * @test
     */
    public function getPriorityReturnsInitialValueForInt(): void
    {
        self::assertSame(
            0,
            $this->subject->getPriority()
        );
    }

    /**
     * @test
     */
    public function setPriorityForIntSetsPriority(): void
    {
        $this->subject->setPriority(12);

        self::assertEquals(12, $this->subject->_get('priority'));
    }

    /**
     * @test
     */
    public function getRetriesReturnsInitialValueForInt(): void
    {
        self::assertSame(
            0,
            $this->subject->getRetries()
        );
    }

    /**
     * @test
     */
    public function setRetriesForIntSetsRetries(): void
    {
        $this->subject->setRetries(12);

        self::assertEquals(12, $this->subject->_get('retries'));
    }

    /**
     * @test
     */
    public function getProcessedTimeReturnsInitialValueForDateTime(): void
    {
        self::assertEquals(
            null,
            $this->subject->getProcessedTime()
        );
    }

    /**
     * @test
     */
    public function setProcessedTimeForDateTimeSetsProcessedTime(): void
    {
        $dateTimeFixture = new \DateTime();
        $this->subject->setProcessedTime($dateTimeFixture);

        self::assertEquals($dateTimeFixture, $this->subject->_get('processedTime'));
    }

    /**
     * @test
     */
    public function getErrorTimeReturnsInitialValueForDateTime(): void
    {
        self::assertEquals(
            null,
            $this->subject->getErrorTime()
        );
    }

    /**
     * @test
     */
    public function setErrorTimeForDateTimeSetsErrorTime(): void
    {
        $dateTimeFixture = new \DateTime();
        $this->subject->setErrorTime($dateTimeFixture);

        self::assertEquals($dateTimeFixture, $this->subject->_get('errorTime'));
    }

    /**
     * @test
     */
    public function getErrorMessageReturnsInitialValueForString(): void
    {
        self::assertSame(
            '',
            $this->subject->getErrorMessage()
        );
    }

    /**
     * @test
     */
    public function setErrorMessageForStringSetsErrorMessage(): void
    {
        $this->subject->setErrorMessage('Conceived at T3CON10');

        self::assertEquals('Conceived at T3CON10', $this->subject->_get('errorMessage'));
    }
}
