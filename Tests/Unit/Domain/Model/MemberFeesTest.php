<?php

declare(strict_types=1);

/*
 * This file is part of the "SmartVerein - TYPO3 Club Management" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2023 Development-Team <development@t3graf-media.de>, T3graf media-agentur UG
 */

namespace T3graf\Smartverein\Tests\Unit\Domain\Model;

use PHPUnit\Framework\MockObject\MockObject;
use TYPO3\TestingFramework\Core\AccessibleObjectInterface;
use TYPO3\TestingFramework\Core\Unit\UnitTestCase;

/**
 * Test case
 *
 */
class MemberFeesTest extends UnitTestCase
{
    /**
     * @var \T3graf\Smartverein\Domain\Model\MemberFees|MockObject|AccessibleObjectInterface
     */
    protected $subject;

    protected function setUp(): void
    {
        parent::setUp();

        $this->subject = $this->getAccessibleMock(
            \T3graf\Smartverein\Domain\Model\MemberFees::class,
            ['dummy']
        );
    }

    protected function tearDown(): void
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function getTitleReturnsInitialValueForString(): void
    {
        self::assertSame(
            '',
            $this->subject->getTitle()
        );
    }

    /**
     * @test
     */
    public function setTitleForStringSetsTitle(): void
    {
        $this->subject->setTitle('Conceived at T3CON10');

        self::assertEquals('Conceived at T3CON10', $this->subject->_get('title'));
    }

    /**
     * @test
     */
    public function getFeeReturnsInitialValueForString(): void
    {
        self::assertSame(
            '',
            $this->subject->getFee()
        );
    }

    /**
     * @test
     */
    public function setFeeForStringSetsFee(): void
    {
        $this->subject->setFee('Conceived at T3CON10');

        self::assertEquals('Conceived at T3CON10', $this->subject->_get('fee'));
    }

    /**
     * @test
     */
    public function getAdmissionFeeReturnsInitialValueForString(): void
    {
        self::assertSame(
            '',
            $this->subject->getAdmissionFee()
        );
    }

    /**
     * @test
     */
    public function setAdmissionFeeForStringSetsAdmissionFee(): void
    {
        $this->subject->setAdmissionFee('Conceived at T3CON10');

        self::assertEquals('Conceived at T3CON10', $this->subject->_get('admissionFee'));
    }

    /**
     * @test
     */
    public function getMembershipFeePeriodReturnsInitialValueForInt(): void
    {
        self::assertSame(
            0,
            $this->subject->getMembershipFeePeriod()
        );
    }

    /**
     * @test
     */
    public function setMembershipFeePeriodForIntSetsMembershipFeePeriod(): void
    {
        $this->subject->setMembershipFeePeriod(12);

        self::assertEquals(12, $this->subject->_get('membershipFeePeriod'));
    }
}
