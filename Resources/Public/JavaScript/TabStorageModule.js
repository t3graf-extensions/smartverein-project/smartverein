require([
  'bootstrap'
], function(bootstrap) {
  var lastTab = localStorage.getItem('lastTab');
  if(lastTab){
    var lastTabTrigger = document.querySelector('#'+lastTab)
    var setLastTab = new bootstrap.Tab(lastTabTrigger)
    setLastTab.show()
  }

  var tabElements = document.querySelectorAll('a[data-bs-toggle="tab"]')

  tabElements.forEach ((tabEl) =>{
    tabEl.addEventListener('shown.bs.tab', function (event) {
      localStorage.setItem('lastTab', event.target.id);
    });
  });

});
