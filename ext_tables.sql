CREATE TABLE fe_users (
	smartverein_member int(11) unsigned DEFAULT '0' NULL,

	KEY smartverein_member (smartverein_member)
);

CREATE TABLE tx_smartverein_domain_model_members (
	member_id varchar(255) DEFAULT NULL,
	gender int(11) DEFAULT '0' NOT NULL,
	firstname varchar(255) DEFAULT NULL,
	midname varchar(255) NOT NULL DEFAULT '',
	lastname varchar(255) NOT NULL DEFAULT '',
	status int(11) DEFAULT '0' NOT NULL,
	salutation int(11) DEFAULT '0' NOT NULL,
	title varchar(255) NOT NULL DEFAULT '',
	street varchar(255) NOT NULL DEFAULT '',
	zip varchar(255) NOT NULL DEFAULT '',
	city varchar(255) NOT NULL DEFAULT '',
	state varchar(255) NOT NULL DEFAULT '',
	country varchar(255) NOT NULL DEFAULT '',
	addition_to_address varchar(255) NOT NULL DEFAULT '',
	leaving_wish smallint(1) unsigned NOT NULL DEFAULT '0',
	dateofbirth int(11) NOT NULL DEFAULT '0',
	profile_image int(11) unsigned NOT NULL DEFAULT '0',
	preferred_communication int(11) DEFAULT '0' NOT NULL,
	method_of_payment int(11) DEFAULT '0' NOT NULL,
	account_owner varchar(255) NOT NULL DEFAULT '',
	iban varchar(255) NOT NULL DEFAULT '',
	bic varchar(255) NOT NULL DEFAULT '',
	customfield1 varchar(255) NOT NULL DEFAULT '',
	customfield2 varchar(255) NOT NULL DEFAULT '',
	customfield3 varchar(255) NOT NULL DEFAULT '',
	customfield4 varchar(255) NOT NULL DEFAULT '',
	customfield5 varchar(255) NOT NULL DEFAULT '',
	customfield6 varchar(255) NOT NULL DEFAULT '',
	customfield7 varchar(255) NOT NULL DEFAULT '',
	customfield8 varchar(255) NOT NULL DEFAULT '',
	customfield9 varchar(255) NOT NULL DEFAULT '',
	customfield10 varchar(255) NOT NULL DEFAULT '',
	entry_date int(11) NOT NULL DEFAULT '0',
	leaving_date int(11) NOT NULL DEFAULT '0',
	mandate_reference varchar(255) NOT NULL DEFAULT '',
	mandate_date int(11) NOT NULL DEFAULT '0',
	sepa_mandate int(11) unsigned NOT NULL DEFAULT '0',
	membership_application int(11) unsigned NOT NULL DEFAULT '0',
	leaving_statement int(11) unsigned NOT NULL DEFAULT '0',
	declaration_of_consent int(11) unsigned NOT NULL DEFAULT '0',
	payment_interval int(11) DEFAULT '0' NOT NULL,
	email varchar(255) NOT NULL DEFAULT '',
	phone varchar(255) NOT NULL DEFAULT '',
	mobile varchar(255) NOT NULL DEFAULT '',
	fax varchar(255) NOT NULL DEFAULT '',
	member_type int(11) DEFAULT '0' NOT NULL,
	organisation varchar(255) NOT NULL DEFAULT '',
	contact_person varchar(255) NOT NULL DEFAULT '',
	position varchar(255) NOT NULL DEFAULT '',
	department varchar(255) NOT NULL DEFAULT '',
	website varchar(255) NOT NULL DEFAULT '',
	member_folder varchar(255) NOT NULL DEFAULT '',
	contact_person_salutation int(11) DEFAULT '0' NOT NULL,
	fe_user int(11) unsigned NOT NULL DEFAULT '0',
	contacts int(11) unsigned NOT NULL DEFAULT '0',
	belongsto text NOT NULL,
	functions text NOT NULL,
	positions text NOT NULL,
	role text NOT NULL,
	sections text NOT NULL,
	vita int(11) unsigned NOT NULL DEFAULT '0',
	changelog int(11) unsigned NOT NULL DEFAULT '0',
	member_fee text NOT NULL,
	categories int(11) unsigned DEFAULT '0' NOT NULL
);

CREATE TABLE tx_smartverein_domain_model_contacts (
	members int(11) unsigned DEFAULT '0' NOT NULL,
	addressbook int(11) unsigned DEFAULT '0' NOT NULL,
	type int(11) DEFAULT '0' NOT NULL,
	contact varchar(255) NOT NULL DEFAULT ''
);

CREATE TABLE tx_smartverein_domain_model_functions (
	title varchar(255) NOT NULL DEFAULT ''
);

CREATE TABLE tx_smartverein_domain_model_positions (
	title varchar(255) NOT NULL DEFAULT ''
);

CREATE TABLE tx_smartverein_domain_model_sections (
	title varchar(255) NOT NULL DEFAULT '',
	staff int(11) unsigned NOT NULL DEFAULT '0'
);

CREATE TABLE tx_smartverein_domain_model_roles (
	title varchar(255) NOT NULL DEFAULT ''
);

CREATE TABLE tx_smartverein_domain_model_vitaentries (
	members int(11) unsigned DEFAULT '0' NOT NULL,
	date int(11) NOT NULL DEFAULT '0',
	entered_by varchar(255) NOT NULL DEFAULT '',
	description varchar(255) NOT NULL DEFAULT '',
	attachment int(11) unsigned NOT NULL DEFAULT '0'
);

CREATE TABLE tx_smartverein_domain_model_changes (
	members int(11) unsigned DEFAULT '0' NOT NULL,
	date int(11) NOT NULL DEFAULT '0',
	changed_by varchar(255) NOT NULL DEFAULT '',
	note text NOT NULL DEFAULT ''
);

CREATE TABLE tx_smartverein_domain_model_addressbook (
	type int(11) DEFAULT '0' NOT NULL,
	company_name varchar(255) NOT NULL DEFAULT '',
	contact_person varchar(255) NOT NULL DEFAULT '',
	street varchar(255) NOT NULL DEFAULT '',
	zip varchar(255) NOT NULL DEFAULT '',
	city varchar(255) NOT NULL DEFAULT '',
	state varchar(255) NOT NULL DEFAULT '',
	country varchar(255) NOT NULL DEFAULT '',
	image int(11) unsigned NOT NULL DEFAULT '0',
	salutation varchar(255) NOT NULL DEFAULT '',
	department varchar(255) NOT NULL DEFAULT '',
	position varchar(255) NOT NULL DEFAULT '',
	note text,
	contacts int(11) unsigned NOT NULL DEFAULT '0'
);

CREATE TABLE tx_smartverein_domain_model_sectionpositions (
	sections int(11) unsigned DEFAULT '0' NOT NULL,
	member text NOT NULL,
	position text NOT NULL
);

CREATE TABLE tx_smartverein_domain_model_memberfees (
	title varchar(255) NOT NULL DEFAULT '',
	fee varchar(255) NOT NULL DEFAULT '',
	admission_fee varchar(255) NOT NULL DEFAULT '',
	membership_fee_period int(11) DEFAULT '0' NOT NULL,
	categories int(11) unsigned DEFAULT '0' NOT NULL
);

CREATE TABLE tx_smartverein_domain_model_settings (
	title varchar(255) NOT NULL DEFAULT ''
);

CREATE TABLE tx_smartverein_domain_model_mailqueue (
	subject varchar(255) NOT NULL DEFAULT '',
	body text NOT NULL DEFAULT '',
	body_html text NOT NULL DEFAULT '',
	sender varchar(255) NOT NULL DEFAULT '',
	recipient varchar(255) NOT NULL DEFAULT '',
	sender_name varchar(255) NOT NULL DEFAULT '',
	recipient_name varchar(255) NOT NULL DEFAULT '',
	attachements text NOT NULL DEFAULT '',
	date_sent int(11) NOT NULL DEFAULT '0',
	type varchar(255) NOT NULL DEFAULT '',
	status int(11) DEFAULT '0' NOT NULL,
	priority int(11) DEFAULT '0' NOT NULL,
	retries int(11) NOT NULL DEFAULT '0',
	processed_time int(11) NOT NULL DEFAULT '0',
	error_time int(11) NOT NULL DEFAULT '0',
	error_message text NOT NULL DEFAULT '',
	categories int(11) unsigned DEFAULT '0' NOT NULL
);
